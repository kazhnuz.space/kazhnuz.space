---
layout: layouts/home.njk
---

<div class="h-card"><p class="align-center"><strong> <span class="p-pronouns">iel</span> - <span class="p-pronouns">they/them</span> - queer - 31 - <span class="p-role">webdev</span> - <span class="p-role">kobold</span></strong></p>

<img src="/img/hhghtthrhtr-small.avif" class="u-photo d-none" />

<p class="p-note">Bonjour, je suis <span class="p-name">Kazhnuz</span> ! Je suis un dev bidouilleur <span class="p-gender-identity">genderqueer</span> bi qui aime bien bricoler des bétises informatiques, écrire, et créer en général des trucs.</p>

Bienvenu sur mon <a href="https://kazhnuz.space/" class="u-url" rel="me">site personnel</a> ! C'est ici que je mets mes projets et mes créations (sur mon <a href="https://blog.kazhnuz.space/" class="u-url">blog</a>), pour le fun tout en permettant celleux que ça intéresse de venir voir ! On va y retrouver des perso queer, un peu de punk, du marginal qui tente de se débrouiller dans le monde. Et pas mal d'esthétique glitch.

[![Fanstuff Garden](/img/buttons/sites/fanstuffgarden.gif)](https://fanstuff.garden) [![Dimensions Quarante-Douze](/img/buttons/sites/qdouze.gif)](https://quarante-douze.net) [![Woltar](/img/buttons/contrib/woltar.gif)](https://woltar.net) [![Planète-Sonic](/img/buttons/contrib/pso.gif)](https://planete-sonic.com)

J'espère que vous passerez un bon moment sur ce blog, à lire mes petites bétises ! :)

<p class="terminal bg-dark"><span class="login">kazhnuz.space@kobold</span>:<span class="path">~</span>$ cd <a href="/about">/about</a> {% for post in collections.all | sort(false, true, "data.eleventySubNavigation.order") -%} {% if post.data.eleventySubNavigation.parent == "À propos" -%} <a href="{{ post.url }}">{{ post.data.eleventySubNavigation.key }}</a> {% endif -%}{%- endfor -%} <span class="cursor">█</span></p> 

<div class="align-center"> 

![Pansexuel⋅le](/img/buttons/pride/pansexual-pansexuel_le.gif) ![Non-Binaire](/img/buttons/pride/non-binary-non-binaire.gif) ![Demisexuel⋅le](/img/buttons/pride/demisexual-demisexuel_le.gif)
  
</div>
</div>