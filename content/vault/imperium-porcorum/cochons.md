---
layout: layouts/subpages.njk
eleventySubNavigation:
  key: Les cochons
  parent: Imperium Porcorum
  order: 0
---

# Les cochons

Les cochons sont la feature principale de l'univers d'Imperium Porcorum. Il s’agit de cochons qui étaient tout à fait normaux à l’origine, et qui ont muté à cause d’un « programme magique ». À première vu, ils sont de simples cochons ordinaires, à l’exception de quelques détails étranges. Tout d’abord par le fait qu’ils parlent et se comportent plus ou moins comme des créatures intelligentes.

En plus de cela, ils ont diverses capacités qui leur sont uniques. Ce sont eux qui ont soutenu Rosie Zanie lors de l'invasion du monde, et sont donc l'espèce dominante d'Ukropie... même si la majorité des généraux sont en fait humains.

Les cochons parlent l'Ukropien, langue principale d'Urkopie, mais également leur propre langue, le cochonus : langue incompréhensible pour les non-cochons. La langue à une structure aussi simple qu'improbable : elle est composée juste des sons "kr + [voyelle]" composé de kr + différente voyelle et combinaison (Kra Kre Kri Kro Kru Kré Krè Kron Kroi Kry (ou Krigrec)). Elle n'a aucune vraie construction linguistique.

## Fonctionnement

Les cochons ne sont pas à véritablement parler des êtres vivants, mais un programme magique reproduisant en partie la biologie des cochons (en cela, ils ne peuvent se reproduire, voir Création). Le programme magique est ce qui les fait fonctionner, et a été créé par la première impératrice d'Ukropie. Elle a utilisé un long processus pour créer de ces "cochons" sur toute la planète. Ce programme s'occupe de former leur consistance physique et de faire fonctionner leur corps comme un corps de cochons, ainsi que de lier un esprit ayant une intelligence comme celles d'un humain et leur offrir un certain nombre de capacités magiques.

Tout les cochons ayant le programme actif voir apparaître sur leur flanc un symbole représentant leur classe.

Le programme des cochons est contenu dans des cristaux de données nommés les **grouinkstal**. Le grouinkstal contient toutes les données et informations sur le cochon, ainsi que sa mémoire. Lorsque l'un cochon voit sa forme physique recevoir trop de dommage, ils explosent en un nuage de fumée, ne laissant derrière eux que le cristal avec toute les données. Des grouinkstaux vierges peuvent être construits à l'aide de cristaux d'énergie Ukropiens.

Ce programme créer également un lien avec tous les autres cristaux, ce qui permet aux cochons de communiquer, mais également avec d'autres êtres humains spécifiques : un *général*, le *Princeps* du général et l'*Imperator* (du moins au plus important). Ces humains forment la hiérarchie auquel le cochon obéit pour mener ses combats (sauf en cas de "bugs").

## Création

Pour créer un cochon, il ne faut pas des parents cochons. C'est nul ça, ça prend trop de temps. Le mieux, c'est la **machinocochons**. Ces machines sont non seulement ce qui permet de *créer* un cochon, mais également de maintenir toutes ses informations, de les analyser d'en soigner, et de les stocker. C'est un peu la machine à tout faire des généraux.

La machinocochons peut soit également ramener un cochon à la vie à partir d'un grouinkstal s'il en est devenu un, voir de créer un tout nouveau cochon à partir d'un grouinkstal vierge. Elle ne peut pas pratiquer le clonage d'un cochon déjà expérimenter, cela pour des raisons d'efficacité : rien ne nuit plus à l'efficacité qu'une crise existentielle quant à sa propre individualité dans le monde. Pas pratique, pas bien. Ces machines sont disponibles dans différents endroits réservés aux généraux, mais les *Princeps* possèdent leurs propres machines.

Les cochons sont donc nés d'un « programme magique » et ne sont pas véritablement de vrais "êtres vivants", mais plus des sortes de structures magiques. Ils sont de ce fait incapable de se reproduire naturellement, et sont créés dans des « centres porcins », et plus précisément par une machine nommée la **machinocochons**. Ce système permet à partir d'un *grouinkstal* de générer un nouveau cochon à partir des données contenu dans le grouinkstal. Les grouinkstaux vierges permettent de créer un cochon.

## Les pouvoirs des cochons

Les cochons possèdent un petit nombre de pouvoirs et capacités. Ces capacités sont censées leur donner un avantage contre les ennemis et une capacité à combattre en cas de rébellion contre l'empire porcin.

Pour le combat, ils possèdent le mode « **furie** », qui les entourent d'une aura rouge, leur permettant d'être invulnérables pendant un temps limité. Cette technique consiste tout simplement en un relâchement progressif de mana du cochon sous forme d'énergie, ce qui lui permet de foncer dans une attaque dévastatrice. Cette technique est puissante, mais requiers beaucoup d'énergie. Cependant, elle a le mérite de rendre le temps de son fonctionnement le cochons invulnérable à certains dégâts mineurs. Les cochons peuvent également **conjurer des armes** pour se battre et les lancer. Ne possédant pas de main, ils les font apparaître spontanément et les lance devant eux. Où en cloche. Beaucoup de stratégie possible. Au corps-à-corps ce n'est pas toujours pratique. Ces deux techniques de combats peuvent être complété par d'autres spécialisées de certaines *classes* et *éléments*.

Pour aider au déplacement, ils possèdent aussi le « **cochonoptère** » qui leur permet de voler. Le cochonoptère matérialise des pales d'hélicoptères sur le dos du cochon, lui offrant donc la capacité de se mouvoir dans les airs. Cette technique est cependant limitée, et n'est pas extrêmement maniable. Peut-être que des jetpacks auraient été plus utiles ?

La **cochonpathie** est une sorte de réseaux télépathique entre les cochons. Tout cochon peut communiquer par l'esprit avec un autre dans le secteur, ou qu'il connait personnellement. Il saura alors son grade et son identité, ce qui est extrêmement utile pour établir des stratégies. Il peut être coupé dans une pièce grâce à des sceaux anti-esprit.

## Les "bugs" au programme

Le programme des cochons est un programme très complexe. Ce qui fait que comme tout programme.

- Les **cochons glitchs**. Ces cochons sont atteints d'un bug sérieux faisant planter tout le programme. Leur apparence peut être défaillante, leurs différents pouvoirs peuvent être affectés de plusieurs manières. Le bug ne disparait pas même après régénération du grouinkstal.

- Les **cochons rebelles** ont tous les pouvoirs et particularité des cochons, à l'exception d'une : ils ne font partie d'aucune chaine de commande. Cela fait qu'ils font un peu ce qu'ils veulent, et se regroupent en tant que groupes indépendants. Ils ont sinon tout le reste des capacités : intelligentes, liens mentaux. Ils ne peuvent être utilisé par des généraux, même après avoir récupéré leurs grouinkstaux.

- Les **cochons sauvages** sont des cochons qui ont tous les pouvoirs, mais pas d'intelligence "presque humaine". Ils trainent souvent dans la nature et combattant avec les généraux pour prouver qu'ils sont plus efficace. Ils sont considérés comme une bonne source de grouinkstaux, mais attention ! Même réadapté, le bug reste en partie présent, ce qui fait qu'ils ne sont pas forcément aussi puissant que les cochons obtenus par cristaux vierges.

- Les **cochons null**. Ceux-là sont extrêmement étranges. Ils n'ont qu'une forme physique. C'est presque comme si c'était des animaux normaux. Des cochons comme animaux ? Quelle idée saugrenue.
