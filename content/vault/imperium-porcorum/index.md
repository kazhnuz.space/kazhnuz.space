---
layout: layouts/switchpages.njk
eleventySubNavigation:
  key: Imperium Porcorum
  parent: Vieux trucs
  description: Le projet avec des cochons mutants, avant sa refonte
  order: 4
---

# Imperium Porcorum

Imperium Porcorum est un univers humoristique et parodique, où une armée de cochons surpuissants se sont attaqué à une planète, la planète Ukropie, et l'ont envahi, le tout plein de superpouvoirs absurdes.

## Présentation

> La planète Ukropie était une planète normale et pleine de vie, jusqu'à ce qu'arrive l'invasion la plus stupide qu'on pouvait imaginer. Une armée de millier de cochons, aux pouvoirs surpuissants, qui se sont abattu sur notre société sérieuse comme une peste rose, rongeant tout ce qu'on avait créé pour en faire un chaos.

En l'an 1111, la planète Urkopie a été envahie par la terrible (mais néanmoins brillante) scientifique Rosie Zanie, qui a mené des expériences génético-magique pour créer une armée de cochons (plus ou moins) intelligent et envahir le monde avec. C'est ainsi qu'elle est devenu impératrice de l'Imperium Porcorum. Rapidement, elle a démonté les institutions en cours, et toute sa vie, elle a cherché les secrets de la toute puissance... Cependant, on ne sut jamais ce qu'elle trouva, parce qu'à la fin de son règne, elle s'enferma dans la *Tour Zanie* et n'en ressorti jamais.

Depuis, le monde est divisé en 5 grandes régions, dont quatre dirigés par des Princeps cherchant chacun à prendre le pouvoir et devenir le nouvel empereur des cochons. Partout dans le monde, des généraux dirige une armée de cochons et luttent entre eux pour cela pour obtenir le pouvoir. Après qu'elle se soit retiré dans la *Tour Zanie*, l'empire n'a plus eu d'empereurs et est resté sous la direction d'un conseil, et de 4 *Princeps* luttant pour un jour devenir les empereurs...

AVEC DES ARMÉES DE COCHONS BIEN SÛR.

Cet univers est à la base plus un gros délire qu'autre chose, avec une inspiration jeux-video. Il était prévu d'être utilisé dans un tactical RPG avec aspect "monster capture" ou on capturerait des cochons, puis d'autres animaux de fermes, pour partir au combat.

## Liste des pages

<ul>
  {%- for post in collections.all | sort(false, true, "data.eleventySubNavigation.order") -%}
    {%- if post.data.eleventySubNavigation.parent == eleventySubNavigation.key -%}
      <li><a href="{{ post.url }}">{{ post.data.eleventySubNavigation.key }}</a></li>
    {%- endif -%}
  {%- endfor -%}
</ul>