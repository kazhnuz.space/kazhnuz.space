---
layout: layouts/subpages.njk
eleventySubNavigation:
  key: Variations de cochons
  parent: Imperium Porcorum
  order: 1
---

# Variations de cochons

Les cochons peuvent varier de plusieurs manières, s'adaptant à diverses situations grâce auxdites variations, où montant en puissance avec les différents grades. Nous allons voir ici les différentes améliorations possibles, et ce qu'elles permettent. Ici, nous ne parlons que des cochons non-buggués.

## Les éléments

Les cochons peuvent adopter l'un des huit éléments physiques en tant que pouvoir. Cet élément leur donne une affinité non seulement passive avec l'élément lui permettant de mieux l'appréhender, mais également des pouvoirs uniques liés à cet élément.

Les éléments sont donc les suivants :

| Élément | Effet utilitaire | Furie élémentaire |
|:-------:|:-------------|:------------|
| **EAU** | Peut respirer sous l'eau | Peut provoquer un ouragan |
| **GLACE** | Résiste au froid | Peut geler les ennemis dans un blizzard |
| **AIR** | Peut éviter ce qui est au sol | Peut voler |
| **FOUDRE** | Immunité à l'électricité | Peut provoquer des orages |
| **FEU** | Résiste au feu | Provoque des tornades de feu |
| **METAL** | Très lourd | Balance des météores ferreux |
| **TERRE** | Peut creuser | Un tremblement de terre |
| **NATURE** | Peut découvrir les secrets naturels | Provoque un soin sur tout ses alliés |

## Les classes

On ne le sait pas, mais en plus d'être biologiste, magicienne, experte en métaphysique et dans les données de l'espace temps, Rosie Zanie avait un autre domaine d'expertise : elle était Docteure en rôlisme. De ce fait, elle a compris rapidement que le meilleur moyen de spécialisé son armée, était d'avoir plusieurs classes, mais également de laisser le choix aux généraux de, s'ils voulaient, défoncer tout à base de leur quinturie (armée de cinq cochons) composé exclusivement de cochons-barbares.

Chaque classe possède deux *formes améliorées*, spécialisant un peu plus la classe.

Les classes sont les suivants :

| Classe de base | Principe | Évolution 1 | Évolution 2 |
|:--------------:|:---------|:------------|:------------|
| Guerrier | le combat bourrin | Barbare (+ tanky) | Paladin (+ DPS) |
| Magiciens | Attaques magiques | Sorcier (+ fourbe) | Archmage (+ puissant) |
| Clerc | Magies spirituelles, un peu tank | Moine (+ tanky) | Druide (+ soin) |
| Rodeur | Fourberies, coups fourrés. Fragile. | Voleurs (+ fourbe) | Assassins (+ critiques) |
| Barde | ... musiques ? | Illusionniste (+ puissant) | Marchand (+ argent, polyvalent) |

## Les grades

Les cochons peuvent également avoir différents *grades* indiquant leur monté en puissance. Chaque grade offre de nouvelles capacités, qui permettent aux cochons de gagner en puissance au fur et à mesure. Les grades sont les suivants :

| Cochon | Pouvoir obtenu |
|:------:|:---------------|
| Cochon de base | N/A |
| Cochon de bronze | Peut obtenir une classe |
| Cochon d'argent | Peut être lié à un élément |
| Cochon d'or | Peut obtenir une classe avancée |
| Cochon élémentaire | Peut déclencher des furies élémentaires |

De plus, plus un cochon a une classe élevée, plus il a d'énergie magique et de puissance, ce qui rend toutes ses capacités plus efficaces.

## Les cochons légendaires

Il existe une dernière variation, hors de ce système : les cochons légendaires. Ces cochons sont apparus spontanément, et sont à la fois hors du système de grade (leur grade est "légendaire") et hors du système d'élément traditionnel. Ils sont extrêmement rares, et on dit qu'en trouver un est déjà un exploit. Plus puissant que les cochons normaux, ils peuvent faire des choses incroyables.

Au niveau des éléments, ils sont liés non pas à un élément physique, mais à l'un des *huit éléments métaphysiques* : le chaos, les ténèbres, le néant, l'espace, l'ordre, la lumière, l'énergie, le temps. Ces éléments leur donne des pouvoirs extraordinaire, puisqu'ils n'affectent pas simplement le monde physique, mais des parties même de la *réalité*.

Le rêve de tout général est d'en récupérer un.
