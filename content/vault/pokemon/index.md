---
layout: layouts/switchpages.njk
eleventySubNavigation:
  parent: Vieux trucs
  key: Vault Pokémon
  description: Tout mes vieux projets liés à Pokémon
  order: 1
---

# Vault Pokémon

Pleins de vieux petits trucs Pokémon que j'ai fait y'a un bail. C'est un peu en vrac et pas forcément super trié, surtout si ça intéresse des gens.

## Pages dédiées

J'ai mis à certains projets des pages dédiées pour que ce soit plus simple à lire :

<ul>
  {%- for post in collections.all | sort(false, true, "data.eleventySubNavigation.order") -%}
    {%- if post.data.eleventySubNavigation.parent == eleventySubNavigation.key -%}
      <li><a href="{{ post.url }}">{{ post.data.eleventySubNavigation.key }}</a> - {{ post.data.eleventySubNavigation.description }}</li>
    {%- endif -%}
  {%- endfor -%}
</ul>

## Sprites

Quelques sprites Pokémon que j'ai créé : Leaf en style DS et l'enterré vivant en style GBA/DS

Le premier est fait grâce aux sprites d'Aurore de Diamant/Perle à celui de la fille de Blanc/Noir et à celui de Leaf de RougeFeu VertFeuille, bien sûr.

Le second est une fusion. Il s'agit d'une fusion de taupiqueur (diglett), d'un cycliste et d'un nageur pour donner... L'enterré vivant de l'ensemble de creepypasta sur le syndrome de Lavanville (Lavender Town)

Il existe déjà un sprite de l'enterré vivant fait par QueenKami, son sprite étant largement plus effrayant que le mien qui est assez loupé XD. Mais j'avais envie d'essayer d'en faire une version plus "moderne" (même si les vieux jeux sont largement plus pratique pour le creepy. Mais avec les nouveaux, on peut facilement faire une ambiance "déprimante" si on s'y prend bien).

<div class="align-center">

![Un sprite d'une jeune fille vu de dos, avec un chapeau blanc, des cheveux chatains et une tenue bleu](/img/articles/vault/sprites/leaf.png) ![Un sprite d'une sorte de zombie blanc qui sort du sol](/img/articles/vault/sprites/buried-alive.png)

</div>