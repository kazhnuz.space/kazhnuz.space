---
layout: layouts/subpages.njk
eleventySubNavigation:
  parent: Vault Pokémon
  key: Pokémon The Missing Number
  description: Un vieux fangame pokémon
  order: 1
---

# Pokémon The Missing Number

Hop, je continue dans le déterrage de vieux trucs que j'ai faits pour de vieux projet, ici ce qui a été une carte de région pour mon très vieux projet : Pokémon - The Missing Number (J'ai réutilisé le nom pour mon site Pokémon rétro).

Ces cartes utilisent les tuiles de carte de Pokémon Or/Argent/Cristal

## Présentation du jeu

> Le monde est corrompu. Vous êtes sorti du néant. Vous avez l'impression d'avoir eut une histoire. Des histoires ? Vous allez devoir traverser cette région pour comprendre les origines de votre existence et pourquoi vous êtes là.

Ce jeu avait pour but d'être fortement basé sur les univers virtuels, la philosophie, l'anti-nihilisme et la métaphysique. Vous y jouez un être artificiel qui a été créé par le système d'une île artificiel virtuelle (fonctionnant grâce aux pouvoirs des Pokémon "glitch" pouvant créer des bulles de réalités dans une dimension parallèle nommée la "Mystery Zone"). En parallèle de votre histoire, vous découvrez l'histoire d'une "aventure Pokémon" traditionnel qui s'est mal passée, en reconstruisant le passage du "vrai héros" qui était prévu pour sauver ce monde de la corruption. Peut-être que vous pourrez même le sauver, avec l'aide de son ancien rival ?

Pendant tout ce temps, une voix mystérieuse tente de vous faire abandonner votre quête…

## Carte de la région

Bienvenue dans la région d'*Utopia*

Il y a eut deux versions de la carte de la région (ayant le même littoral, mais des agencements des routes et des villes différents), et cette version est la dernière en date.

La région était basée sur les concepts philosophiques.

<div class="align-center">

![Une petite carte Pokémon, elle est composée deux trois partie, suivant l'est ou l'ouest d'une chaine de montagne, et d'une île séparée](/img/articles/vault/missing-number/utopia.jpg)

</div>

Avec le recul je sens la grosse inspiration de Sinnoh XD

## Euclide

La première ville que vous rencontrez durant votre périple, après être sorti du néant. Elle est basée sur la carte d'Oliville d'une vieille version des jeux Or/Argent/Cristal.

Son nom fait référence au mathématicien Euclide.

<div class="align-center">

![Une ville de bord de mer](/img/articles/vault/missing-number/euclide.jpg)

</div>

## Bourg-Candide

Hop, en mapping, la première ville de Missing Number... Enfin, ce qui aurait été la première ville si j'avais voulu faire le jeu linéairement : C'est en fait une des dernières villes qu'on visite (hors de l'île). Dans ce jeu on ne joue pas un des "héros" du jeu, mais quelqu'un qui n'a rien à voir avec toute l'histoire et doit faire avec.

Son nom est une référence à *Candide* de Voltaire.

<div class="align-center">

![Une petite ville forestière. Cinq bâtiments, dont un grand sont situé dans la place principale, ainsi qu'un petit arbre à baie. Un poind d'eau sépare le cinquième bâtiment, et un objet](/img/articles/vault/missing-number/bourg-candide.jpg)

</div>

## Essentia

Ce serait la seconde ville si on atteignait pas cette zone vers la fin du jeu. Pas grand-chose à dire sur cette ville.

Son nom fait référence à *l'essence*, c'est-à-dire la notion d'une chose "en soi", de son concept détaché de son existence.

<div class="align-center">

![Une petite ville avec une rivière qui la coupe en deux](/img/articles/vault/missing-number/essentia.jpg)

</div>

## Village de l'Impasse

Cette ville est très importante : Elle contient la bibliothèque régionale (donc pleins d'archive qui seront utile), et une grotte où se déroule des événements participant au climax du jeu.

Son nom fait référence à la fois au fait qu'il est dans une impasse, et à l'impasse philosophique (aporie)

<div class="align-center">

![Un village de montagne avec une grande tour](/img/articles/vault/missing-number/impasse.jpg)

</div>

## Route 1 et 2

Deux routes inachevées.

<div class="align-center">

![Une route de Pokémon en pixel art](/img/articles/vault/missing-number/route-1.jpg)

![Une route de Pokémon en pixel art](/img/articles/vault/missing-number/route-2.jpg)

</div>
