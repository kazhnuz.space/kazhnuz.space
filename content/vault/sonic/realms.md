---
layout: layouts/subpages.njk
eleventySubNavigation:
  key: Sonic Realms
  parent: Vault Sonic
  description: Une suite à 2006, Rivals, Rush & Generations
  order: 4
---

# Sonic Realms

Jeux faisant suite et liens à Sonic Rush/Rivals/2006 & Generations. Sonic, Blaze et Shadow (gameplay boost) et Tails, Silver et Sticks (gameplay plateforme) devraient s'unir pour faire face à Eggman Nega, qui tente de récupérer l'énergie d'une créature nommée le *Devoreur*. Le jeu suivrais deux histoire en parallèle : celle de Silver et Shadow (avec ensuite Sticks trouvée dans une poubelle), et celle de Tails, Sonic et Blaze.

Comme dans Rush, les personnages ne feraient pas les niveaux dans le même ordre (voir ne feraient que 7 des 9 mondes, histoire d'avoir certains exclusif à chaque fois ?). Chaque niveau aurait également 4 act, deux "speed" et deux "plateforme", et le fait que chaque équipe soit déséquilibre en terme de vitesse/plateforme ferait qu'un act ne serait pas utilisé par une équipe.

## Synopsis de Sonic, Tails et Blaze

Sonic et Tails apprennent qu'Eggman à encore fait des siennes. Il aurait attaqué et volé un artéfact important dans un musée, la *Dent Antique*. Ils foncent dans la forêt, où il a été vu récemment, et l'attaque. Celui-ci ne comprend pas pourquoi il est attaqué, et dénie s'être intéressé, disant qu'il est juste en train de "créer une nouvelle magique de domination et d'oppression mondiale de manière tout à fait légale". Le combat se produit cependant quand même, et à la fin est arrêté par Blaze, disant qu'elle savait qu'elle les trouverait ici.

Explication qu'Eggman Nega recherche les dent du dévoreur, une créateur datant d'avant même l'origine des mondes (inspiration lovecraftienne ofc). Pour les nouveaux joueur, rééexplication qu'Eggman Nega est le **double d'Eggman** venant du monde de Blaze.

Au fur et à mesure de l'aventure, les personnages découvrirait ce qu'est l'objectif d'Eggman Nega, et qu'il a créé un réseau de fidèle dans chaque monde à l'aide de mensonges sur qui il est. Chaque monde est l'occasion de faire quelques moment d'humour sur ce qu'il représente, mais aussi de montrer que Sonic à voyagé dans de nombreux endroits. Cependant, Nega à une longueur d'avance : dans chaque monde il semble avoir récupérer la dent.

Les special stage du groupe amène les Sol Emerald. Sonic y gagnera pour la première fois la capacité de les utiliser.

Note : Eggman, furieux, poursuit aussi un peu se groupe pour tenter de récupérer les dents et doubler Nega. Il sera vaincu plusieurs fois par les héros.

## Scénario de Shadow, Silver et Rouge

Première cutscene : Silver arrive et cherche Shadow et Rouge. Le trouvant au bout de quelques minutes de recherche, il explique que c'est terrible, Eggman Nega à mis la main sur des informations concernant une créature extrèmement dangereuse. Silver indique directement Eggman Nega comme "le descendant d'Eggman".

Silver, Shadow et Rouge, à travers les mondes, découvre petit à petit qu'en plus d'Eggman Nega, le Devoreur à un culte étrange, composée d'être encapé qui veulent le ramener à la vie et ce qu'est le dévoreur. Le dévoreur est un être qui se réincarne constamment et dévore les mondes. À chaque fois qu'il est détruit, un nouveau peut réapparaitre. (Il est insinué que Solaris et le Time Eater sont les deux devoreur précédant). Les dent sont les restent d'une incarnation du dévoreur encore plus ancienne que Solaris.

Petit à petit, ce groupe découvre que c'est le culte du Devoreur qui a fourni la première dent (celle de leur monde). Ils vivent dans le Phantom Realm. Le Phantom Realm sera juste décrit comme un monde qui a été dévoré par le devoreur, et où les gens se sont séparé en deux.

- Les premiers membres de la civilisation ont cherché à se venger en envoyant des bouts de leur monde contre *tout les alliés du Devoreur* (insinuation que c'est là que vienne les deux Phantom Ruby)

- D'autre se mirent à le respecter, estimant que seul en s'alliant à lui la survie était possible.

Les special stage du groupe amène les Chaos Emerald. Shadow et Silver utiliseront une super form.

## Ensemble

Les deux groupes se rejoignent deux fois.

- Sur Terre, les groupes mettent ensemble ce qu'ils ont appris. Mise en avant qu'Eggman Nega ment constamment sur son origine. À un moment, Shadow dit à Sonic de ne pas avoir comme stratégie "on lui fonce dedans et on lui casse la figure". Le groupe apprend qu'Eggman Nega se fait passer pour "Nega Robotnik, petit cousin de Ivo Robotnik", qui aimait beaucoup sa soeur Maria et est triste de ce qu'est devenu son cousin, et prêt à tout pour aider le monde entretemps. Shadow décide que "on lui fonce dedans et on lui casse la figure".
- Sur le monde effacé, les groupes combattent ensemble Eggman Nega dans un dernier assaut.

## Les 9 mondes
- Mobius :: Niveau basé sur le premier de Rush. Boss : Eggman normal (qui se fait rapidement jarter du scénario, et a été attaqué)

- Terre :: Niveau basé sur City Escape

- Futur (avec ambigüité volontaire sur quel monde c'est en fait) :: Niveau futuriste utopique.

- Sol Dimension :: Niveau basée sur Pirate Island 

- MaginaryWorld :: Niveau onirique, réutilisant des gravité multiple à la Lost World ? Côté un peu "temple"

- Phantom Realm :: Monde composée de grotte obscures, proche du Null Space. Monde d'origine du Phantom Ruby, mais également de la secte du dévoreur.

- Music World :: Monde musical. Inspiration de Desert Ruin Act 3 et de Music Plant, très non sequitur. Faut y mettre Shadow. IL FAUT.

- Millenium Nights :: Monde basé sur Sonic and the Secret Rings.

- Monde effacé :: Un monde où plus rien n'existe, que quelques ruines de bâtiment, du feu. Ref à Crisis City, en fait. Les personnages diront que ce monde leur est "étrangement famillier", mais ne réussiront à le reconnaître. Le fond n'est que blanc, avec la sorte de "flou" utilisé pour indiquer les rêves. Boss final ici.

Deux mondes bonus pourraient apparaitre :
- Special Zone :: Pour rejouer aux Special Stage. C'est ici qu'on débloque le final stage.
- Chao Dimension :: Un Jardin des Chao... à l'échelle d'un monde ?

## Récupération des Emeraudes

La première émeraude à récupérer est forcément scénaristique, et le special stage avec. Les autres ne le sont pas. Finir tout les special stage permet avec n'importe quelle équipe de séléctionner la final story, dans la Special Zone.

Le chef du culte ramène à la vie le Devoreur. Il faut l'affronter. Le devoreur fait quelques références à tout les ennemis que Sonic à affronter. Il déclare que son ombre est toujours présente sur les mondes.

Qu'il finira toujours par tout dévorer.