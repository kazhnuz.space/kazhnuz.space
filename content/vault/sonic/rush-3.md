---
layout: layouts/subpages.njk
eleventySubNavigation:
  key: Sonic Rush 3
  parent: Vault Sonic
  description: Une suite de Sonic Rush
  order: 2
---

# Sonic Rush 3

Un vieux projet de "Sonic Rush 3". Cette catégorie contient aussi en sous-page un autre projet que j'ai eu du genre, "Sonic Realms". Aucun des deux projets n'ira plus loin qu'une phase de brainstorming, mais peut-être qu'un jour, qui sait.

## Concept

Toute la petite bande de Sonic (Sonic, Tails, Knuckles, Amy) se retrouvent dans le monde de Blaze suite à un prologue expliquant l’histoire, et doivent parcourir 7 niveaux afin de vaincre le père adoptif de Blaze qui est devenu berzek suite à l’utilisation des Sol Emerald. On en apprend plus sur le monde de Blaze, son histoire, et sa famille adoptive.

Dans chacun des niveaux, on devrait affronter un membre important de la hiérarchie du monde de Blaze, et une fois qu’on les a tous affrontés, on pourrait alors aller à l’assaut du père de Blaze, parce qu’on a le support suffisant pour ça (avec Sonic qui veut y aller tout de suite, et Blaze qui lui dit qu’on peut pas affronter l’empereur comme ça)

On aurait des batailles récurrantes avec le frère adoptif de Blaze, qui n’a aucun pouvoir mais qui se bat avec des armes blanches. Il aurait un côté un poil charmeur et sarcastique, bien plus à l’aise en société que sa petite soeur. Cependant, on pourrait voir dans les dialogues qu’il tient à elle, mais qu’il a un devoir envers sa famille et l’Empire (oh mon dieu, un Camus dans Sonic. Logiquement faudrait du coup qu’il meurt, d’après la loi des Camus).

## Idées en vrac

- Sonic & Blaze jouable dans chaque niveau, avec peut-être les autres personnages en bonus ou dans des niveaux spécifiques ?
- Utiliser des espèces australiennes pour tout les anthros (notamment la famille de Blaze), afin de créer un aspect exotique à ce monde, et du coup de convenir avec les koalas ?
- Faire apparaître Silver dans un dernier arc, qui deviendrait le lien entre Sonic 2006, les Sonic Rivals et les Sonic Rush ? Voir même peut-être une sorte de Mephiles, coincé dans le « néant », et qui voudrait revenir à l’existence (cela permettrait de placer un speech méta sur l’existence et l’inexistence, nice)
- Les Hidden Islands restent, ainsi que les phases en véhicules. On découvrent la partie Nord de l’Archipel, plus peuplé et plus centralisé autour de l’île principale

## Backstory de Blaze

Blaze est apparue dans son monde, surgie de nulle part, avec aucun souvenir. Elle était apparue du néant, entourée par des flammes. L’Empereur du monde de Sol, impressionné par cette apparition, adopta la jeune fille, qui devint la princesse du monde de Sol, au côté de son frère aîné adoptif. N’étant pas destinée à devenir impératrice, elle gagna cependant un devoir, des plus important : protéger les Sol Emerald, trésor du monde de Sol, et donc de protéger son monde tout entier. L’un est destiné à devenir un dirigeant, l’autre une combattante.

## Introduction du jeu

On commence avec Sonic et sa bande, dans un niveau se passant dans son monde (mon côté troll me dit de mettre South Island, mais ce serait plus logique d’être dans un niveau de ville). On retrouve Blaze, qui semble vouloir éviter Sonic. On la suit, peut-être un peu de fight avec elle, et elle finit par nous dire qu’elle est là pour essayer de chercher [[je sais pas encore]] un moyen de protéger son monde d’une nouvelle menace.

En effet, un nouveau conseiller est arrivé à la cour, et à au fil du temps fait apprécier à l’Empereur qu’il devrait avoir la main mise sur les Sol Emerald. En effet, le monde de Blaze est très anomique, l’Empire existe, il dirige, mais la piraterie est puissante, et pas mal de territoires ne sont en rien sous le contrôle de l’empire. Pour vaincre ses ennemis, il lui fait les Emerald.

Blaze refusa, malgré toute les tentatives de son frère pour éviter le conflit.

Cependant, malgré toute sa puissance, elle ne pu vaincre une armée entière face à elle (mais par contre elle pu faire une bonne montagne de dégât), et se fit prendre les Sol Emerald, et se retrouva dans l’obligation de se replier pour pouvoir ensuite les récupérer.

La réaction de Sonic face à ça ? « Okay, on y va ! » Résultat, toute la bande se retrouve dans le monde de Blaze, et c’est partie pour une grande aventure.

## Déroulement du jeu

Sept niveaux dans le monde de Blaze

- Six avec des seigneurs du monde de Blaze, tous basé sur une espèce australienne différente.
- Un dernier niveau où on affronte le père de Blaze. Le père et le frère de Blaze sont des Tylacine.
- Pleins de petites Hidden Island avec des secrets différents.
- Un mode mission centré sur les personnages secondaires : Une série de deux~trois missions avec un problème à résoudre, deux personnages, et un des héros. On joue les personnages secondaires et pas ceux principaux.