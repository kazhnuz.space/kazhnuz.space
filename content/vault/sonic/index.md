---
layout: layouts/switchpages.njk
eleventySubNavigation:
  parent: Vieux trucs
  key: Vault Sonic
  description: Tout mes vieux projets liés à Sonic
  order: 0
---

# Vault Sonic

Étant fan de Sonic depuis très longtemps, j'ai eut pas mal de projet Sonic. Le but de ces petites pages est de recensé mes vieux projets et idées Sonic, et de les rendres disponibles ici. Comme d'habitude dans le principe de cet endroit, presque tout est inachevé, et contiendrait beaucoup de choses à revoir, et est proposé tel-quelle, sans aucune garanti que ce soit bon :)

(déjà que je garantis pas cela d'habitude mdr)

## Pages dédiées

J'ai mis à certains projets des pages dédiées pour que ce soit plus simple à lire :

<ul>
  {%- for post in collections.all | sort(false, true, "data.eleventySubNavigation.order") -%}
    {%- if post.data.eleventySubNavigation.parent == eleventySubNavigation.key -%}
      <li><a href="{{ post.url }}">{{ post.data.eleventySubNavigation.key }}</a> - {{ post.data.eleventySubNavigation.description }}</li>
    {%- endif -%}
  {%- endfor -%}
</ul>

## Sprites

Des sprites qui peuvent être utile, j'ai fait le checkpoint et les ressort des jeux modernes dans le style des jeux Megadrive ^^

Fait à partir de "bases", c'est à dire d'une screenshoot de Sonic Episode 4 pour le ressort et du checkpoint de Sonic 1 pour le checkpoint moderne.

<div class="align-center">

![Des checkpoints et ressort Sonic](/img/articles/vault/sprites/springs.png)

</div>
