---
layout: layouts/subpages.njk
eleventySubNavigation:
  key: SMA - Histoire et thematiques
  parent: Sonic Monado Adventure
  order: 0
---

# Histoire et thematiques

L'histoire de MOnado Adventure se passe dans la base de Monado Prime, une biosphere artificielle en circuit fermé habitée conçue en partenariat entre les Fédérations Unis, G.U.N. et l'entreprise Rimlight. Cette base est construite sur l'île rocheuse de Fortitude Rock, une île quasi-désert située dans les zones polaires. Présentant de nombreux espaces de vies et biomes artificiels, la base est considérée comme l'example à suivre pour les futurs conquêtes spatiales. La base est autonome sauf en lumière (elle a besoin de la lumière du soleil pour faire pousser les plantes), c'est à dire qu'elle est capable de produire elle même tout ce dont elle a besoin en terme d'eau, nourriture et air.

Sonic, Tails et Amy (équipe de début de jeu) arrivent sur l'île suite à un appel du *Directeur de Monado Prime*, qui est inquiêt de l'arrivée de robot d'Eggman dans certains biomes de l'île. Il a peur que le docteur tente de s'emparer de la base et d'en faire un outil de conquête à la MeteorTech. Il a alors demandé de l'aide aux trois mobiens pour qu'ils se débarrassent de la présence du savant fou dans la biosphère.

Cependant, la base recèle bien des mystères, et Sonic & compagnies comprendront bien vite qu'on ne peut comprendre la menace présente que subit la biosphère, sans découvrir tout son passé...

## Rôles dans l'histoire

L'un des principes de Sonic Monado Adventure est que comme dans Sonic Adventure, chaque personnage à un point de vue différent sur l'intrigue, et a un objectif et un rôle narratif différents. Le rôle de chaque personnage sera

- **Sonic** doit empêcher le plan d'Eggman de porter ses fruits, et évitera les intrigues secondaires. Son histoire portera sur les dégats que fait Eggman, et la capacité de Sonic à l'empêcher.

- **Tails** doit sauver les habitants de la ville. Sonic histoire portera sur la différence entre un héros et un sauveteur, et lui fera gagner en autonomie en le montrant *complémentaire* avec Sonic.

- **Amy** va se pencher sur certains des soutiens qu'Eggman possède dans l'île, et sur la situation du jeune Ankou. Son histoire portera sur le combat contre les injustices existantes, et sur la manipulation.

- **Knuckles** va ignorer totalement la base (la considérant comme juste un nouveau jouet des humains sur leur planète), mais tentera de purifier l'énergie chaotique de l'île. L'histoire se penchera sur le lore des Erebiens.

- **Shadow** va faire des recherches sur l'incident de Monado Prime, et chercher le traitre ayant fourni les "clefs" de l'ex-centrale chaotique à Eggman. Son histoire portera sur le fanatismes et la manière dont on peut projeter nos croyances dans la réalité.

- **Cream** va tenter d'aller retrouver Gemerl, qui a disparu lors de l'attaque d'Eggman. Son histoire montre que cette petite lapine est précieuse et courageuse.

## Inspirations/Références

Ici je compile quelques unes des inspirations que je voulais mettre en avant et réutiliser pour construire mon histoire. Ce sont des lignes directrices qui pouvaient m'aider à miser sur certains aspects de l'histoire.

### Légendes et histoire

- Tout le concept d'Hybris/Phronésis sont inspiré des valeurs grecques. L’Empire Erebiens (dont le coeur du pouvoir est l'Hybris Altar) chute à cause de son intempérance et de leur orgueil, comme les héros grecs. Le passage de la ville de Phronésis à l'autel d'Hybris représentent l'abondon de la prudence/sagesse dans la quête du grandiose (qui est ici moqué). Le jeu est entièrement fondé sur la paranoïa qui prend les personnes en situation de pouvoir.

- Les quatres épreuves de Morrigans sont basé sur les quatre vertues telle que présentées par Platon.

- L'histoire de Monado Prime est inspiré de l'île d'Ys : Gradlon était le roi d'Ys (d'où Ys Gradlon), et Dahut celle qui a laissé le diable entrer dans l’île, la condamnant à sombrer sous les flots. Une légende raconte que le jour ou Paris sombrera sous les flots, l’île d’Ys remontera, ce qui représente un peu les propos du jeu, puisque les technologies Erebienne sont ramenée par Eggman pour faire sombrer symboliquement les capitales du monde.

- Le nom des bases monado est basé sur les monade de Leibniz, mais en vrai c'est plus pour la classe.

### Sciences

- La base de Monado Prime est inspirée des projets de biospheres comme [Biosphère 2](https://fr.wikipedia.org/wiki/Biosph%C3%A8re_II), [BIOS-3](https://en.wikipedia.org/wiki/BIOS-3) ou [MELiSSA](https://en.wikipedia.org/wiki/MELiSSA).

- L’idée de l’environnement interdit après une explosion de centrale, avec contamination est assez aisé à deviner, je suppose :p

# Personnages non-jouables

Etant un jeu de style Adventure, le projet contient un certain nombre de personnage secondaire lié à l'intrigue.

## Personnages de Monado Prime

### Taranis

- **Espèce :** Bouc

- **Age :** 14 ans.

- **Description Physique :** Taranis est un jeune bouc blanc, dôté de gants et chaussure style "cuir"/"aventurier". Il porte toujours une sorte de bateau d'acier, où il peut faire transiter de l'électricité.

- **Histoire et description psychologique :** Taranis est l'un des petits-fils de Morrigan de la base. Vivant dans une cabane située dans les bois de la pythosphère, c'est un adolescent colérique, plein de rage contre les injustices qui existent dans la base. Il estime que la direction de Monado Prime à trahi des habitants de la base en ne leur offrant pas la vie qui aurait été promis. Il estime aussi que Dahut, mère de Ankou, est responsable de tout ces soucis.

Il travaille secrètement pour l'Eggman Empire, ayant été amené par Eggman suite à diverse action positive que celui-ci à fait pour quelques habitants, tel qu'attaquer des créanciers, où protéger secrètement une de leur cachette dans la phytosphère. Il n'est pas vraiment au courant des plans *exacts* du docteur, mais veut lui payer sa dette, et n'a pas grand chose à faire des conséquences des actions du docteur pour le reste des Fédérations Unis où pour la ville principale. De plus, la promesse de pouvoir enfin quitter Fortitude Rock le motive.

Taranis à de ce fait une vision assez sombre du monde, furieux des injustices qui existent dans la base. Pour lui, le projet Millenium est une immense arnaque, qui les a déplacé et coincé dans une base sur un caillou perdu, ou il se sent enfermé. La difficulté à en partir pour la majorité des habitants ne fait qu'empirer la situation. Il ne croit ni en les gens ayant supervisé Monado Prime, mais en veut aussi aux saboteurs. Cependant, il fait partie des rares personnes ne pensant pas qu'Ankou est un poison *en lui-même*. Il est juste pour lui le fils d'une traîtresse qui est à l'origine de la mort de sa Grand-Mère, et au dueil de son père.

Il a énormément de mal à faire confiance aux autres, mais à une certaine attirance qu'il essaie de combattre pour les idéaux de Sonic. Est-il bien possible de sauver tout le monde, existe-t-il une solution pour aider les habitants sans suivre Eggman ? Il aimerait y croire, mais n'y arrive pas.

## Ankou

- **Espèce :** Mouton

- **Age :** 13 ans.

- **Description Physique :** Un petit mouton noir un peu chétif, le jeune Ankou se remarque surtout par son grand médaillon, et la grande cape qui l'entoure entièrement, comme pour le protéger. Il a de toute petite corne qui dépassent à peine du sommet de son crane.

- **Histoire et description psychologique :** Craintif, peu loquace et encore moins accessible, Ankou est un jeune orphelin qui se cache une grande partie du temps dans les zones inutilisée où "technique" de la base. Il connait tout les passages secrets de Monado Prime et les utilise pour s'éloigner des autres. Il a d'ailleurs dans un mur de la base l'endroit où il vit, une sorte de zone de passage de tuyant avec dedans un hamac volé et de quelques magazines récupéré à droite à gauche.

Ankou est comme il est à cause des brimades et du harcèlement qu'il a subit de ses camarades depuis qu'il est tout jeune. Il est le fils de Dahut, l'ingénieure de la Gosth Central, et si peu de gens connaissent exactement ce qu'elle était, ils le voit comme le fils de la cause de l'incident d'il y a 12 ans. Certains, moins au fait, croient qu'il est la cause du "poison", un mystérieux phénomène ayant conduit à la fermeture de plusieurs endroits. Surnommé carrément "le Poison", il a donc constamment été rejeté.

Très défaitiste et avec une grande tendance à se déprécier (voir à se haïr), Ankou estime qu'il ne mérite pas spécialement d'être "sauvé". Il a constamment l'impression que vu son ascendance, il devrait plus "laisser les autres tranquille" et vivre dans son coin. Il a du mal à accepter les preuves d'amitié. Il n'essaie à cause de cela même pas de se défendre, et se laisse tristement faire quand on l'attaque. Ankou est non-combattant, et n'aime pas du tout l'utilisation de la force.

Cependant, il n'est pas non plus entièrement inoffensif. Il collabore en effet un peu avec la direction, participant à attiser les tensions avec sa connaissance des passages secrets. Il ne collabore pas pour obtenir une salvation, mais parce qu'il estime que sa situation est inextricable. Le petit mouton noir à abandonné depuis longtemps.

Anrad est l'une des rares personnes à faire preuve de gentillesse avec lui, et paradoxalement il trouve Taranis moins pire que les autres, parce que même s'il sait parfaitement que le jeune Bouc le hait, le bouc n'irait jamais s'attaquer à plus faible, et ne le considère pas comme la source du poison. Cependant, Taranis n'est pas tendre non plus, et rejette constamment Ankou quand il tente de l'approcher.

## Anrad

- **Espèce :** Chèvre

- **Age :** 16 ans.

- **Description Physique :**  Une chèvre plutôt de grande taille, avec des cornes imposante, et une grande tenue de barde. (Grand chapeau, vêtements type "voyageur ancien").

- **Histoire et description psychologique :** Anrad est une jeune barde, qui fait souvent tâche dans la station. Beaucoup de gens la trouve bizarre, voir effrayante, ce malgré un caractère plutôt jovial et accessible. C'est que, dénuée de toute honte de ce qu'elle est, d'envie de correspondre aux normes sociales ou de discretion, la chèvre n'hésite pas du tout à aller voir les passant pour leur raconter les histoires du peuple Érebiens - qu'elle tiens de sa grand-mère, où à faire des grands discours sur la place public.

Les forces de l'ordre elles-mêmes souvent hésitent un peu à la déloger, de peur qu'elle aurait quelques pouvoirs mystiques caché, surtout que malgré qu'elle fasse un peu flipper, elle est plutôt appréciée des gens. Si les rumeurs disent que ses pouvoirs sont formidable, elle ne semble pas se battre. Elle a également une formidable tendance à troller, étant très forte pour remarquer les paradoxes chez les autres comme chez elle-même.

Après, ses actions ne sont pas que de l'excentrisme. Bien plus sage et réfléchie que son frère et que le jeune Ankou, elle estime qu'il faut éviter que les anciennes légendes Érebienne tombent dans l'oubli. Elle a refusée d’aider Eggman dans sa quête, mais d'un autre côté n'a rien révélé du rôle de son frère dans les plans d'Eggman aux autorités. Elle forme une sorte de troisième camps entre Eggman et la direction de Monado Prime. Elle suivra les héros plusieurs fois, intéressée par leurs objectif, et voulant sauver son frère des griffes d'Eggman.

Elle s'en veut secrètement de son échec à avoir gardé son frère dans la lumière. Elle n'agit pas directement parce qu'elle ne s'estime pas capable de résoudre une situation aussi inextricable.

## Gabriel Seraph (directeur de Monado Prime)

- **Espèce :** Humain

- **Age :** 52 ans.

- **Description Physique :** Gabriel Seraph est un grand homme, qui peut parraitre aux premiers abords imposant. Il porte constamment un costume avec son insigne de commandement de Monado One. Il porte de petite lunette rectangulaire, et ses cheveux grisonnant sont rabattu en arrière.

- **Histoire et description psychologique :** Gabriel Seraph est le nouveau chef de la base depuis l'incident d'il y a 12 ans, avant quoi il était le second d'Ys Gradlon dans la direction de la base. Si son élection a été unanime, il est souvent vu comme un couard imbécile et inepte, incapable d'arriver à la cheville de son prédécesseur. Il est mal vu à cause de principalement deux événements : son incapacité à dire ce qui s'était exactement passé lors de l'incident qui a tué Ys Gradlon ainsi que de nombreux ingénieurs, et le fait d'avoir accepté l'offre de HexaECO (compagnie rivale d’énergie de Rimlight) pour obtenir de l'énergie dans la base.

Celui-ci accorde cependant peu d'importance à son image, estimant que "tout l'honneur de ce cher conseil d'administration n'aurait qu'affamé la base". Il n'hésitera pas cependant à critiquer publiquement ses adversaires sur leurs affirmations, rappelant que les centrales chaotiques ne permettent pas la construction d'une nouvelle après incident.. Cela lui permettra de perdre une partie de ses adversaires, lui assurant peu d'ennemis dans son propre camp pendant les les premières année de son mandat.

Cependant, il recommence à être raillé depuis l'arrivée de badnics du Dr. Robotnik, pour son incapacité à agir. L'appel de Sonic, Tails et Amy sera la culmination de cela, et aujourd'hui son mandat est très mal vu par ses adversaires, qui appellent à sa démission.

Cependant, Gabriel Seraph est plus complexe que ce qu'on pourrait croire. Membre des Chaos Breaker, un groupe para-militaire venant essentiellement de GUN, qui sont particulièrement inquiet à cause des pouvoirs développés par les hybrides. Ils ont peur que cette différence de puissance fasse tomber leur peuple en position de faiblesse. Ces héritiers de Gradlon qui se sont d'autant plus renforcé depuis l'arrivée de Sonic dans ce monde. Son objectif est de provoquer de réactiver le projet Erinye afin de pouvoir "lutter contre la menace extraterrestre".

## Membres du projet Millenium

Le projet Millenium a impliqué de plusieurs personnages important (décédé aujourd'hui), qui auront une influence dans l'histoire soit à travers leur héritages, soit à travers d'autres aspects.

### Ys Gradlon

Ys Gradlon était un des grands généraux du G.U.N., et supervisait pour eux le projet afin de le comprendre et de le maîtriser.

Un homme déjà âgé de 76 ans lors de l'incident, mais droit et fier, il deviendra cependant de plus en plus tendu par les "risques" provoqué par les troubles dans les Fédérations Unis. Il sera celui qui lors d'incident, décidera de mettre en avant de plus en plus la partie militaire du projet.

Il disparaîtra dans l'incident de la Gosth Central, mais son héritage sera encore présent, étant à l'origine des Chaos Breaker. Ceux-ci resteront peut présent jusqu'à l'arrivée de nos héros (Sonic, etc.) sur Terre, provoquant un renouveau de leur idéologie.

### Morrigan

Une mobienne archeologue qui a participé au projet millenium en tant qu'experte de la troisième civilisation. Elle avait pour but de vérifier la dangerosité du Gosth Rift, et d'étudier la faisabilité d'exploiter la faille par une exégèse des textes anciens. Elle sera rapidement l'une des plus critiques sur le danger du projet.

Sa connaissance des phénomènes occultes lié au Gosth Rift lui a considéré une réputation presque mystique, la rendant crainte par les quelques mobiens ayant participé au projet. Elle est toujours crainte malgré sa mort il y a 12 ans lors de l'incident de la Gosth Central. Les circonstances de sa mort l'ont laissé comme fantôme, et elle guidera Knuckles dans sa quête.

Elle est la grand-mère de Taranis et Anrad. Sa mort est en grande partie ce qui fait que Taranis considère Ankou comme responsable des soucis dans sa vie.

### Dahut

Une brebis, mère d'Ankou et ancienne scientifique ayant travaillé sur le projet Millenium.

Docteure en énergie chaotique arrivée sur Terre pour pouvoir approfondir plus ses études qu'elle aurait pu sur Mobius, elle a étudié en grande partie les thèses de Gérald Robotnik. Optimiste sur le fait que les sciences aideraient le monde à devenir meilleur, elle a participé au projet dès qu'elle à sû que le conglomérat était intéressé par le Gosth Rift.

Sans doute l'une des plus optimiste et des forces principale du projet (par rapport à une Morrigan plus prudente)

Elle est celle qui a convaincu une partie des douze chef d'accepter le projet. Elle est morte lors de l'incident. À cause de cela, elle est considérée comme coupable de toute ce qui s'est passé durant ce projet, et son fils unique Ankou subit les contrecoups de cette réputation.