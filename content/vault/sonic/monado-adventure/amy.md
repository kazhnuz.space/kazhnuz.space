---
layout: layouts/subpages.njk
---

# Amy Rose

Tails est arrivé sur Monado Prime pour aider Sonic et Amy à défaire les nouveaux plans d'Eggman. Cependant, elle se séparera du groupe assez rapidement et commencera à découvrir un mystérieux enfant, Ankou, qui semble en savoir beaucoup sur la centrale. Elle remarquera que les forces de la cité ET Eggman s'intéresse à lui.

Elle décidera de l'aider, et découvrira que même Shadow s'intéresse à cet enfant mystérieux.

Amy ici sera présenter d'une manière plus proche des comics. Ayant confiance en elle, avec un crush pour Sonic présent mais peu appuyé. Son aggressivité sera tourné en plus une combativité (par exemple se moquant des soldats de GUN en disant que la situation à 5 contre 1 est inégale… pour eux), et n'hésitant pas à se moquer de leurs armes.

**Objectif** : Protéger le jeune Ankou, cible des forces de la cité ET d'Eggman.

**Thématique** : Les rapports de force et les oppressions, le deuil et la perte.

## Partie 1 - L'attaque d'Eggman

> [!NOTE]
> Le début de cette partie est en grande partie commune entre Amy, et Tails/Sonic

L'histoire commence sur Sonic, Tails et Amy qui arrive avec le Tornado sur l'île de Fortitude Rock. La première cutscene les montre volant sur le Tornado, avec Tails qui leur explique la situation : le Directeur de Monado Prime les a appelé à cause d'une apparition spontanée de badnics dans la base.

Une fois arrivée sur l'île, le personnage doit se diriger jusqu'à l'entrée de la base, et entrer, et le Maire les accueillera, leur indiquant la situation rapidement, et leur demandant de vaincre Eggman. Les personnages décideront de commencer par un tour en ville pour constater l'étendu de la situation.

Le joueur devrait se déplacer jusqu'à l'anneau de l'Act 1 de Hermetic Utopia.

- **Act 1 - Downtown Panic**

Une fois le niveau terminé, Tails annoncera qu'il a intercepté un signal provenant de l'une des spheres. Eggman ayant pris contrôle des portes, Tails proposera de se diriger vers le train aerien pour se diriger jusqu'à l'écosphere concernée.

> [!NOTE]
> C'est à partir de ce moment qu'on débloque Tails et Amy

- **Act 2 - Crystal Railways**

Le groupe est interrompu par Chaos Gamma, l'un des principaux gros bras d'Eggman. Amy se sépare du groupe, décidant de s'occuper du robot pour que les deux autres puissent avancer.


- **BOSS - Chaos Gamma 1**

Amy affronte et vainc Chaos Gamma. Celui-ci s'envole, et semble détecter une "cible d'Eggman". Amy, entendant ça, décide de partir à sa poursuite.

Elle le suis jusqu'à l'hydrosphere.



- **Act 3 - XXXX**

- **Act 4 - XXXX**

- **BOSS - VS Epsilon**

Amy sauve Ankou.

Flash. Un truc s'est passé dans la ville.

Arrivée avec les autres. La vide est vide de vie. Eggman accueille de Sonic avec un hologramme, annonçant son plan. Il a construit le Egg Banisher, qui est capable d'envoyer tout les habitants d'une zone dans une dimension parallèle. Il déclare ensuite qu'il compte lancer dans quelques heure le Egg Banisher pour lancer un assaut sur les plus grandes. Toutes les douzes heure, une nouvelle ville dont tout les habitants seraient bannis de la surface du globe !

Il déclare espérer que Sonic fera une tentative désespérée de sauver le monde, et qu'ils voulait également lui faire découvrir mieux le gout de la défaite, et ses nombreuses et subtil saveur.

Mais qu'à leur prochaines rencontre, il ne le sauvegardera pas.

Tails fait remarquer que le robot d'Eggman est au dessus de la ville, et qu'ils peuvent l'attaquer avec le Tornado. Les deux héros foncent vers le Tornado, tandis qu'Amy décide qu'elle va rester en arrière pour aller protéger les habitants restant de l'armée d'Eggman.

## Partie 2 - Ankou et Taranis

Dans cette partie, Amy découvre

Se termine par l'arrivée de Shadow, qui lui aussi s'intéresse à Ankou. Quand Amy s'approche avec son marteau, Shadow se moque un peu du fait qu'elle saute tout de suite sur le combat sans réfléchir à même ce qu'il veut.

Amy et Shadow se disputent un peu, chacun agacés de l'autre.

<>

Ils arrivent à la même conclusion : il faut faire tomber le chef de cette bases.

## Partie 3 - Revolution


- **BOSS - Chaos Gamma 2**
