---
layout: layouts/subpages.njk
---

# Shadow the Hedgehog

Comme souvent quand il y a une histoire avec Shadow, l'un des points les plus à travailler est sa personnalité et son personnage. Shadow ici sera assez simple, basé sur un certain nombre d'aspects:

- La personnalité de Shadow est un mélange entre sa personnalité SA2 et celle Modern : Il est un solitaire misanthrope un peu arrogant et sassy qui n'aime pas trop la compagnie. Il a sa "politesse" de la version japonaise, cependant, le côté un peu "soft speaking".

- Il écoute le vœu de Maria, et sauve surtout le monde pour le vœu de Maria. Il est donc un héros, mais un héros qui le fait pour une raison plus "personnelle" qu'un Sonic altruiste par personnalité. Il peut aussi avoir des méthodes plus brutales et va moins contenir sa puissancce, contrairement à Sonic encore.

- Shadow n'est pas membre de GUN. Il agit indépendamment, selon ses propres moyens. Il voit le GUN comme étant une organisation puissante, utile mais aussi possiblement dangereuse. Et non, il n'a pas "pardonné" à GUN entièrement, il reconnait juste que GUN est possiblement moins dangereux qu'il y a 50 ans.

- La Team Dark sont les potes de Shadow, dans une camaraderie un peu étrange, lié à leur caractères marqués et aux trauma de Shadow (qui a du mal à s'attacher). Pas ses supérieurs, ni une équipe officielle.

Cependant, le but de cette histoire n'est pas de mettre en opposition Sonic et Shadow, ce sont plus des points de repère pour faire des parallèle, et l'utilité des deux personnages : Sonic face à un antagoniste-victime, et Shadow face à un antagoniste-bourreau.

**Objectif** : Découvrir et arrêter les actions du traitre de Monado Prime.

**Thématique** : Intrigue et complot politique, la haine et la vacuité.

## Partie 1 - Dark Vigilente

Act 1 -

Act 2


## Partie 3 - Revolution


**Act 13 - Chaos Gadgets**

**Act 14 - Erinye Satellite**

Shadow traverse le satellite pour aller le désactiver. Quand il arrive au bout. Il arrive devant le directeur de la base.

**BOSS - Blood Hawk** - Commandé par Gabriel Seraph

Shadow détruit le robot, et détruit la commande ensuite du satellite. Seraph est furieux, et lui déblatère. Shadow se moque de lui, lui fait remarquer la vacuité de sa haine. Qu'il se cherche un ennemi qu'il attribue de tout les maux pour s'améliorer lui-même. Plutôt que d'admettre qu'il n'a pas autant de différence qu'il croit avec, il chercher à tirer tout ceux différents vers le bas.

Shadow se TP avec. Les crédit de Shadow montre des agents de GUN (avec Rouge) embarqué le directeur de la base, tandis que Shadow regarde puis se TP. Rouge le regarde se tp. Cut. Rouge arrive à côté de Shadow, les deux discutent.

Rouge explicite que les membres de cette vision ne seront pas vaincu juste parce que le chef est vaincu. Shadow dit qu'il sait. Elle dit qu'il y en a même un certain nombre infiltré dans GUN, et qu'elle n'est pas forcément à l'aise.

Shadow dit qu'alors ils combattront. Qu'il n'a pas peur d'eux.

Rouge dit qu'elle songeait au début tenter de faire croire à Omega que c'était des badnics, mais qu'on lui a dit que cela posait des "soucis d'ethique". Shadow dit qu'effectivement, tromper Omega comme ça est peut-être une mauvaise chose. Rouge dit en rigolant qu'il est horrible, Shadow lui répond qu'elle a commencé.
