---
layout: layouts/subpages.njk
eleventySubNavigation:
  key: SMA - Sonic the Hedgehog
  parent: Sonic Monado Adventure
  order: 10
---

# SMA - Sonic the Hedgehog

Le hérisson le plus rapide du monde est de retour ! Orgueilleux, fier et inarrêtable, Sonic est toujours l’amoureux de la liberté et de la course qu’il a toujours été. Étonnamment plus malin qu’il le laisse paraître, le hérisson a été appellé par le dirigeant de la zone avec Tails et Amy afin d’aider face à une invasion de Badniks.

Toujours heureux de pouvoir aider les gens, il n’a pas hésité et le voilà à présent pour conbattre Egman.

**Objectif** : Détruire le Egg Banisher

**Thématique** : Le fait qu'il se soit fait avoir par Eggman (deux fois), et qu'il soit incapable d'empêcher Eggman de faire des dégats.

> **Note: **
> Sonic peut entrer dans des Special Stage (un par zone) pour trouver les Chaos Emerald, dans des stages inspiré de ceux de Sonic CD (niveau style mode 7), avec une caméra fixe (pas de rotation) avec des inspirations du gameplay d’Unleashed.

## Partie 1 - L'attaque d'Eggman

> **Note: **
> Cette partie est en grande partie commune à Tails et Sonic (et le tout début est également commun avec Amy).

L'histoire commence sur Sonic, Tails et Amy qui arrive avec le Tornado sur l'île de Fortitude Rock. La première cutscene les montre volant sur le Tornado, avec Tails qui leur explique la situation : le Directeur de Monado Prime les a appelé à cause d'une apparition spontanée de badnics dans la base.

Une fois arrivée sur l'île, le personnage doit se diriger jusqu'à l'entrée de la base, et entrer, et le Maire les accueillera, leur indiquant la situation rapidement, et leur demandant de vaincre Eggman. Les personnages décideront de commencer par un tour en ville pour constater l'étendu de la situation.

Le joueur devrait se déplacer jusqu'à l'anneau de l'Act 1 de Hermetic Utopia.

- **Act 1 - Downtown Panic**

Une fois le niveau terminé, Tails annoncera qu'il a intercepté un signal provenant de l'une des spheres. Eggman ayant pris contrôle des portes, Tails proposera de se diriger vers le train aerien pour se diriger jusqu'à l'écosphere concernée.

> **Note: **
> C'est à partir de ce moment qu'on débloque Tails et Amy

- **Act 2 - Crystal Railways**

Le groupe est interrompu par Chaos Gamma, l'un des principaux gros bras d'Eggman. Amy se sépare du groupe, décidant de s'occuper du robot pour que les deux autres puissent avancer. Sonic et Tails arrivent eux dans la *phytosphère*. Depuis cet endroit, Tails fait remarquer que le signal provient de la forêt artificielle.

Le joueur doit rejoindre l'anneau d'act de Lush Forest.

- **Act 3 - Lush Forest**

Dans la forêt, Sonic et Tails trouvent une dalle qui semblent amener vers un espace à l'intérieur des murs. Le signal y provient. Ils rentrent.

Transition immédiate vers le niveau suivant.

- **Act 4 - Sylvian Base**

A la fin du niveau, Sonic et Tails arrive à l'ordinateur centrale de la base, et l'allume. Ils arrivent à désactiver la fermeture forcée des portes, mais y trouvent un enregistrement.

Sur cet enregistrement : Eggman commence à dire que c'est pas très gentil d'entrer par effraction chez les gens, mais que ce n'est pas grave parce qu'une bonne base qui se fait attaquer est beaucoup plus efficace qu'un mail pour pouvoir contacter le hérisson bleu, et qu'il a un message pour lui. Il annonce qu'il est en train de tester sa nouvelle invention, le *Egg Banisher*, et qu'il va s'attaquer à la ville très bientôt.

Il déclare espérer que Sonic sera présent à ce rendez-vous, parce qu'il à beaucoup de chose à lui montrer. Sonic et Tails voient que le lancement n'est pas simplement imminent. Il a commencé.

Ils doivent se rendre jusqu'à la porte, où ils sont arrêter par un étrange individu, qui déclare que les habitants de cette zone mérite ce qu'il va leur arriver, et que les habitants des spheres se vengent enfin d'année d'humiliation.

- **BOSS - Vs Taranis 1** : Le jeune bouc se bat avec des techniques élécctriques.

Sonic réussi à vaincre son jeune adversaire, mais un grand flash se produit et est visible à travers la porte. Amy et Tails arrivent vers ce moment. Sonic saute et fonce vers la ville, furieux d'avoir été ralenti comme ça lorsqu'il allait sauver une ville.

Sonic entre à la ville. Vide de vie. Eggman l'accueille avec un hologramme, annonçant son plan. Il a construit le Egg Banisher, qui est capable d'envoyer tout les habitants d'une zone dans une dimension parallèle. Il déclare ensuite qu'il compte lancer dans quelques heure le Egg Banisher pour lancer un assaut sur les plus grandes. Toutes les douzes heure, une nouvelle ville dont tout les habitants seraient bannis de la surface du globe !

Il déclare espérer que Sonic fera une tentative désespérée de sauver le monde, et qu'ils voulait également lui faire découvrir mieux le gout de la défaite, et ses nombreuses et subtil saveur.

Mais qu'à leur prochaines rencontre, il ne le sauvegardera pas.

Tails fait remarquer que le robot d'Eggman est au dessus de la ville, et qu'ils peuvent l'attaquer avec le Tornado. Les deux héros foncent vers le Tornado, tandis qu'Amy décide qu'elle va rester en arrière pour aller protéger les habitants restant de l'armée d'Eggman.

- **Subgame - Tornado Assault**

A la fin de ce niveau, les héros se prennent un tir qui fait disparaitre Tails, et le Tornado chute. Black out.

Quand Sonic se réveille, un nouvel holograme d'Eggman lui dit que son ami est son prisonnier… et qu'il a intérêt de se dépecher s'il veut le sauver. Sonic part alors en quête pour aller vaincre le Docteur.

## Partie 2 - A la recherche d'Eggman

Sonic doit alors commencer une recherche à travers le base Monado. Il a accès à trois zones en particulier, les trois spheres restantes. À plusieurs endroit ou des personnages sont accessible, le joueur pourra apprendre qu'une *personne étrange* se trouve dans l'hydrosphere, dans une *mystérieuse salle secrête*, *accessible par la plage*.

Le joueur doit du coup rejoindre l'anneau de l'act de Radiant Coast

- **Act 5 - Radiant Coast**

Cela active l'accès au premier Trials (celui de la sagesse). Sonic peut donc y accéder.

- **Act 6 - Sophia Trials**

Rencontre avec une mystérieuse chèvre. Celle-ci lui raconte la légende du peuple erebiens, et du Gosth Rift. Elle explique que quand sa grand-mère était en vie, les humains ont répété ici une erreur similaire avec la Gosth Central, et que les actions du Docteur ne sont qu'une nouvelle répétition de cette tragédie. Sonic commence à lui demander ce qu'elle sait du Docteur, quand ils sont interrompu par un boss.

- **Boss - Vs Egg Charon**

Une fois le boss vaincu, Anrad se présente comme la soeur de Taranis. Quand Sonic le qualifie de "jerk", elle lui explique sa situation. Sonic se radoucit un peu, mais continue à être plutôt méfiant envers Anrad. Elle dit qu'elle peut l'aider à vaincre Eggman. *Quelqu'un* sait comment s'introduire dans la Gosth Central, un échidné Rouge. Elle lui explique qu'il travers les différentes spheres, et qu'elle lui conseille de vérifier dans la Thermosphere s'il s'y trouve.

Alors qu'il part, elle lui demande de ne pas être trop dur avec son frère, il a autant besoin d'aide que d'être stoppé.

- **Act 7 - Arid Hill**

Sonic rencontre et doit se battre contre le Egg Charon.

- **Boss - Vs Egg Charon**

Knuckles n'est pas présent ici, mais peut-être dans les plateau ?

- **Act 8 - Windy Cliff**

Sonic croise Knuckles. Knuckles lui explique l'instabilité de la faille chaotique et le danger. Il dit qu'il est en mission importante et lui demande ce qu'il veut. Sonic lui explique sa situation. Knuckles lui dit qu'il existe une entrée secrête vers la base d'Eggman, *dans la grotte glacée*.

Sonic fonce vers la grotte.

- **Act 9 - Cyrogenic Grotto**

## Partie 3 - Chute et remontée

Une fois la grotte passée, Sonic se retrouve dans la Gosth Central, et l'act Egg Dystopia se lance immédiatement.

- **Act 10 - Egg Dystopia**

Sonic se retrouve à nouveau face à Taranis. Il tente de le convaincre que sa voie est mauvaise, et qu'il doit arrêter toute cette histoire. Taranis explique qu'il n'a pas le choix, qu'ils sont tous bloqué sur cette ile. Que les traitres ont détruit toute chance de rêve.

Le combat se lance.

- **BOSS - Vs Taranis 2** (si j'aurais réussi à avoir du voice acting, les personnages auraient continué leur débat durant le combat)

Taranis vaincu, Sonic tente de lui expliquer son erreur, mais se fait arrêter brusquement : un piège s'active, et les deux mobiens disparaissent dans un flash.

Les deux personnages reprennent connaissance après un moment dans un espace mystérieux, le "forgotten void". Sonic comprend vite que c'est l'endroit où Eggman a banni les habitants, mais ne voit personne. Il se dit que ça doit être trop grand. Taranis est furieux de la trahison d'Eggman, Sonic se moque un peu de lui.

Cependant, voyant le trouble de Taranis, Sonic lui demandera de lui expliquer avec ses propres mots pourquoi il a fait ça, lui parlant du fait qu'il a croisé sa soeur. Taranis lui parlera un peu de la disparition de sa grand mère, et de la situation des habitants les plus pauvres, et de son envie de faire tomber tout ça. Qu'il n'a pas d'autre choix, qu'Eggman est le seul assez puissant pour lutter. Que eux n'ont pas la force à eux seul.

Sonic répond en lui expliquant qu'ils peuvent lutter et affronter la direction de la base sans rejoindre Eggman, et qu'il ne leur apportera que du malheurs en plus. Qu'il tente de conquérir chaque monde qu'il croise, et qu'il ne croit qu'en son propre pouvoir. Il explique la situation des gens dans la ville, lui demande combien de ses habitants étaient des gens brimés, qu'il dit vouloir protéger. Le jeune bouc ne dit rien. Sonic dit ensuite simplement que cela ne sert à rien de rester ici à débattre.

- **Act 11 - Forgotten Void**

Les deux personnages traversent le Forgotten Void jusqu'à se trouver vers une créatures (avec toute les histoires, le joueur pourra deviner qu'il s'agit d'Ys Gradlon, ayant survécu mais ayant été corrompu par l'énergie chaotique, mais ce ne sera pas dit explicitement).

- **BOSS - Vs ?????**

Sonic ressort dans un flash de lumière, avec Taranis, dans les ruines. Il amène le jeune bouc, il est tant de sortir d'ici. Celui-ci ne comprend pas le comportement de Sonic, pourquoi il l'aide. Celui-ci lui dit que ce n'est pas à lui de donner ou non son pardon pour les actions du jeune Bouc. C'est à lui de faire en sorte de se racheter pour ses crimes. Même si Sonic lui fera remarquer que techniquement, seul lui et ses amis sont vraiment au courant de ses actions : c'est à lui de voir ça avec sa conscience.

- **Act 12 - Hybris Altar**

Une fois sorti des ruines, Sonic reçoit un contact de Tails : celui-ci s'est libéré, et à réussi à libérer les habitants de la ville aussi (la chronologie sera vague).

Le hérisson a un grand sourire narquois, pensant à à quel point le plan d'Eggman se retourne contre lui, et dit qu'il est tant de partir donner une bonne leçon à Eggman, qui n'aurait pas du sous-estimer Tails.

Taranis ne dit rien, mais son sprite indique qu'il semble rassuré.

*(Sonic peut à nouveau prendre les Adventure Field)*

- **Act 13 - Gosth Gadgets**

Sonic et Taranis croisent Shadow, en pleine course. Shadow lui fait une remarque sarcastique sur le fait qu'il a visiblement encore réussi à mettre dans son camp un ancien ennemi. Sonic lui répond que c'est son deuxième travail après arrêter un tyran mégalomane au génie limité par son absence de connaissance des peignes à moustaches.

Shadow ne lui répond pas spécialement, et se contente de lui demandé s'il se dirige au Chaos Core. Quand Sonic lui répond que oui, celui-ci répond que c'est parfait, qu'il va pouvoir aller s'occuper du directeur plus tôt. Sonic lui demande pourquoi, Shadow lui répond simplement qu'il a utilisé Eggman et à ses propres plan. Juste avant de disparaitre, il dit à Sonic d'être rapide, l'arme d'Eggman va sans doute bientôt être rechargé. Sonic répond avec amusement que "rapide" est son second prénom, Shadow soupire d'agacement et se TP. Cependant, juste avant, il lui demandera de "remercier Amy de sa part".

Taranis demande - un peu plus timidement qu'avant - si Shadow est tout le temps comme ça. Sonic répond qu'il semble un peu de mauvaise humeur, mais qu'une fois qu'on le connaît on sait lire ce qu'il y a derrière. Et que certaines personnes sont un peu trop lesté par des rôles trop envahissant, leur laissant peut d'espace pour s'exprimer.

Taranis restera silencieux.

- **Act 14 - Chaos Core**

Sonic arrive face à Eggman. Il se vente un peu en lui disant "Game Over", Eggman lui répond qu'il va l'éliminer. Sonic fonce vers le robot, et le combat se lance.

- **Boss - Egg Banisher**

Fin du scénario de Sonic : Le Egg Banisher s'effondre et Eggman disparait avec dans la faille. Le monde a été sauvé encore une fois ! Sonic se dirige vers Taranis. Le jeune bouc s'excuse à Sonic, disant qu'il a été bête de croire en Eggman, et qu'il n'a fait qu'aider les plans non seulement du Docteur, mais de puissant de la base aussi.

Sonic lui répond que d'une certaine manière, il le comprend. Qu'il ne comprend pas grand chose à toute les dynamiques de ce monde, de comment les puissants, et qu'il peut imaginer que c'est simple de se faire avoir par un beau discours. Mais il dit à Taranis qu'il a la force pour lutter, mais surtout qu'il n'est pas seul et que c'est en groupe qu'ils lutteront. Il lui dit également qu'il peut compter sur son aide… même si Shadow à sans doute fait une partie du travail, désormais (et il ajoutera qu'il suppose qu'Amy aussi, puisque Shadow la remercie, ne se doutant absolument pas que c'est par accident qu'il a raison).

Il lui explique que c'est pareil pour lui, il a eu besoin des autres pour terminer cette aventure.

Un petit blanc se forme. Les deux remontent lentement l'ascenseur, avant que Sonic dise que c'est trop lent pour lui, et saute pour courir sur le mur, sur la musique de *It Doesn't Matter*.

Fin de l'histoire de Sonic.
