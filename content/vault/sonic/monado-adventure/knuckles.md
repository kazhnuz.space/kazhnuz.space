---
layout: layouts/subpages.njk
---

# Knuckles the Echidna

**Objectif** : Résoudre la corruption chaotique entrainée par l'utilisation de la faille chaotique.

**Thématique** : L'hybris est une chose qui se répète, et qui produira les mêmes conséquences.

## Arrivée dans l'ile

## Partie 1 - L'incident

- **Act 1 - Crystal Railways**

L'échidné se dirige vers la

- **Act 2 - Soprosyne Trials + Vs Chaos Delta**

Après avoir détruit le E-100, Knuckles découvre la première partie de la légende de la civilisation Erebienne.

- **Act 3 - Cryogenesis Grotto**

Knuckles trouvera l'entrée indiquée par Morrigan et entrera dans la centrale devenu base d'Eggman. Il vois les quantités d'énergie chaotique. Il aura d'abord un aspect un peu énervé, critiquant la manière dont les civilisations ont toujours joué avec une énergie sacrée qu'iels ne maitrisaient pas et qu'iels ne devraient pas toucher.

Morrigan sera en désaccord, notamment que dans les différentes civilisations, il y a eu des utilisations très différentes. Des utilisations prudentes, imprudentes. Morrigan dira que cette faille était utilisée depuis des milliers d'années avant la chute des Erebiens.

Knuckles admettra la validité de la remarque, mais répliquera que le soucis est qu'il suffit qu'un l'exploite mal pour que ça tourne mal, et Morrigan lui répond que c'est pour ça qu'ils doivent agir.

- **Act 4 - Egg Dystopia**


- **Act 5 - Spectral Archive ?**

## Partie 2 - Purification

> [!NOTE]
> Les niveaux de ce passage peuvent être fait dans le désordre, avec une unique contrainte : l'act 2 de chaque zone n'est débloqué qu'après avoir fait l'act 1 qui lui correspond. Les événements de l'act 1 sont lié au lieu, les événements de l'act 2 à l'ordre.
> Les robots d'Eggman sont également vaincu dans l'ordre d'apparition. Knuckles affronte dans l'ordre Epsilon, Zeta et Beta. En mode "trial", les robots utiliseront tous le même décors, recolorant la salle en gris au lieu de la couleur de l'épreuve.

- **Act T-1 - Windy Cliff**

Rencontre Sonic

- **Act T-2 - Andreia Trials**

- **Act P-1 - Verdant Plain**

- **Act P-2 - Diké Trials**

- **Act ?-1 - Tropical Ocean**

- **Act ?-2 - Sophia Trials**

## Partie 3 - Le rituel

- **Act 12 - Ancient Scriptures**

- **Act 13 - Hybris Altar**

- **Act 14 - Forgotten Realm**

- **BOSS FINAL - Vs Ys Gradlon**
