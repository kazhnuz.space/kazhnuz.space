---
layout: layouts/subpages.njk
eleventySubNavigation:
  key: SMA - Miles "Tails" Prower
  parent: Sonic Monado Adventure
  order: 11
---

# Miles "Tails" Prower

Tails est arrivé sur Monado Prime pour aider Sonic et Amy à défaire les nouveaux plans d'Eggman. Cependant, lors d'une attaque sur Eggman, il se retrouvera capturé par ce dernier. Le petit renard vivra alors sa propre aventure, sauvant les habitants de la ville tandis que Sonic affronte le dictateur.

L'idée de ce scénario est de se détacher de l'idée que Tails devrait être indépendant de Sonic pour être la même personne, pour entrer plus dans celle de montrer qu'ils ont déjà des talents différents, et que chacun apporte à l'autre.

**Objectif** : Sauver les habitants de la ville.

**Thématique** : Sa débrouillardise et sa complémentarité avec Sonic. Sauvetage vs "combattre les méchants".

## Partie 1 - L'attaque d'Eggman

> [!NOTE]
> Cette partie est en grande partie commune à Tails et Sonic (et le tout début est également commun avec Amy).

L'histoire commence sur Sonic, Tails et Amy qui arrive avec le Tornado sur l'île de Fortitude Rock. La première cutscene les montre volant sur le Tornado, avec Tails qui leur explique la situation : le Directeur de Monado Prime les a appelé à cause d'une apparition spontanée de badnics dans la base.

Une fois arrivée sur l'île, le personnage doit se diriger jusqu'à l'entrée de la base, et entrer, et le Maire les accueillera, leur indiquant la situation rapidement, et leur demandant de vaincre Eggman. Les personnages décideront de commencer par un tour en ville pour constater l'étendu de la situation.

Le joueur devrait se déplacer jusqu'à l'anneau de l'Act 1 de Hermetic Utopia.

- **Act 1 - Downtown Panic**

Une fois le niveau terminé, Tails annoncera qu'il a intercepté un signal provenant de l'une des spheres. Eggman ayant pris contrôle des portes, Tails proposera de se diriger vers le train aerien pour se diriger jusqu'à l'écosphere concernée.

> [!NOTE]
> C'est à partir de ce moment qu'on débloque Tails et Amy

- **Act 2 - Crystal Railways**

Le groupe est interrompu par Chaos Gamma, l'un des principaux gros bras d'Eggman. Amy se sépare du groupe, décidant de s'occuper du robot pour que les deux autres puissent avancer. Sonic et Tails arrivent eux dans la *phytosphère*. Depuis cet endroit, Tails fait remarquer que le signal provient de la forêt artificielle.

Le joueur doit rejoindre l'anneau d'act de Lush Forest.

- **Act 3 - Lush Forest**

Dans la forêt, Sonic et Tails trouvent une dalle qui semblent amener vers un espace à l'intérieur des murs. Le signal y provient. Ils rentrent.

Transition immédiate vers le niveau suivant.

- **Act 4 - Sylvian Base**

A la fin du niveau, Sonic et Tails arrive à l'ordinateur centrale de la base, et l'allume. Ils arrivent à désactiver la fermeture forcée des portes, mais y trouvent un enregistrement.

Sur cet enregistrement : Eggman commence à dire que c'est pas très gentil d'entrer par effraction chez les gens, mais que ce n'est pas grave parce qu'une bonne base qui se fait attaquer est beaucoup plus efficace qu'un mail pour pouvoir contacter le hérisson bleu, et qu'il a un message pour lui. Il annonce qu'il est en train de tester sa nouvelle invention, le *Egg Banisher*, et qu'il va s'attaquer à la ville très bientôt.

Il déclare espérer que Sonic sera présent à ce rendez-vous, parce qu'il à beaucoup de chose à lui montrer. Sonic et Tails voient que le lancement n'est pas simplement imminent. Il a commencé.

Ils doivent se rendre jusqu'à la porte, où ils sont arrêter par un étrange individu, qui déclare que les habitants de cette zone mérite ce qu'il va leur arriver, et que les habitants des spheres se vengent enfin d'année d'humiliation.

- **BOSS - Vs Taranis 1** : Le jeune bouc se bat avec des techniques élécctriques. Dans l'histoire de Tails, celui-ci participe aussi au combat.

Sonic réussi à vaincre son jeune adversaire, mais un grand flash se produit et est visible à travers la porte. Amy et Tails arrivent vers ce moment. Sonic saute et fonce vers la ville.

Le groupe entre à la ville. Vide de vie. Eggman l'accueille avec un hologramme, annonçant son plan. Il a construit le Egg Banisher, qui est capable d'envoyer tout les habitants d'une zone dans une dimension parallèle. Il déclare ensuite qu'il compte lancer dans quelques heure le Egg Banisher pour lancer un assaut sur les plus grandes. Toutes les douzes heure, une nouvelle ville dont tout les habitants seraient bannis de la surface du globe !

Il déclare espérer que Sonic fera une tentative désespérée de sauver le monde, et qu'ils voulait également lui faire découvrir mieux le gout de la défaite, et ses nombreuses et subtil saveur.

Mais qu'à leur prochaine rencontre, il ne le sauvegardera pas.

Tails fait remarquer que le robot d'Eggman est au dessus de la ville, et qu'ils peuvent l'attaquer avec le Tornado. Les deux héros foncent vers le Tornado, tandis qu'Amy décide qu'elle va rester en arrière pour aller protéger les habitants restant de l'armée d'Eggman.

- **Subgame - Tornado Assault**

A la fin de ce niveau, les héros se prennent un tir qui fait disparaitre Tails. Black out. Tails se réveille dans une cellule, à l'intérieur d'une ville qui semble contrôlée par Eggman.

## Partie 2 - Jailbreaking

> [!NOTE]
> Une grande partie de se passage se fait en Adventure Field

Eggman l'accueille après 1~2 minutes ou le joueur est laissé à pouvoir agir avec un message se moquant de lui, puis le petit renard monologue un peu sur sa situation, et sur sa frustration de ce nouvel échec. Cependant, il pense au fait qu'il en est capable. Il sait que des gens ont confiance en lui, comme Sonic. On voit une mini-cutscene ou Tails tente une nouvelle technique de spin dash aerien pour tenter d'aller plus vite, et tombe, et Sonic le rattrape et lui dit : "Ce n'est pas grave, tente autant de fois dont tu auras besoin."

Après cette cutscene, Tails débloque le "Spin Dash Aerien" (sa capacité A+B).

L'évasion va se faire en utilisant les capacités de Tails. Il faudra s'envoler jusqu'en haut de la cellule, et détruire avec un coup de spin-dash aerien des grilles, afin d'avancer dans un conduit d'aeration. Si Tails tombe dans le vide ou des piques, fondu au noir et retour dans la cellule.

Une fois que c'est fait, il se retrouve dans l'Adventure Field. Il a la possibilité d'explorer tout Gosth Central et Erebian Ruins, mais sera absolument seul durant tout ce temps. Si on va vers la porte menant au reste de l'ile, Tails fera une remarque qu'il lui faudrait avoir accès au système informatique. D'abord, il faudrait qu'il trouve des appareils à Eggman pour pouvoir gérer ça.

- **Subgame - Treasure Hunt** : Tails est dans l'Adventure field et doit trouver trois appareils d'Eggman pour trouver. Des pièges sont actifs et l'Adventure Field fonctionne comme un mini-niveau.

Une fois que c'est fait, Tails estime qu'il va devoir se connecter au système d'Eggman. Pour ça, il va devoir entrer dans sa ville

- **Act 5 - Egg Dystopia** (le niveau est rendu ici plus facile grace au vol de Tails, qui devra surtout éviter les obstacles)

Cela lui permettra de découvrir qu'Eggman à pris plus ou moins contrôle du système informatisé de l'île. Il voit qu'il y a un système de numérisation humaine, proche de ceux du G.U.N., et décide qu'autant hacké ça de lui-même.

L'entrée se trouve dans les anciennes ruines Erebiennes.

- **Act 6 - Silicon Discord**

Tails réussi à arriver au terminal et commence à le hacker, et obtient des informations sur le projet Gosth Central, et sur comment Eggman en a fait une arme. Il est interrompu par une arme d'Eggman arrive.

- **BOSS - Vs TODO**

Tails sort, et la porte vers le Wrath Corridor est ouvert. Il peut enfin sortir.

Il commence à théoriser qu'il serait sans doute possible d'inverser la polarité du flux chaotique l'arme du Docteur (la ref est volontaire :D). Il remarque la présence de quatre failsafe qu'Eggman à détecté, en les indiquant comme "à contrôler", y envoyant quatre de ces armes. Il obtient les infos sur l'un en particulier, l'Andreia Trials, activable dans la thermosphere.

- **Act 7 - Wrath Corridor**

Quand Tails sort, il n'a toujours aucune communication avec Sonic ou Amy. Il n'y a visiblement plus aucune communication possible dans toute la base, d'après le renard à cause de la surcharge en énergie ambiante. Tails décide de plutôt se concentrer sur les habitants, qui sont en grand danger.

## Parte 3 - Libération.

Tails décide d'aller mettre son plan à execution. Pour ça il doit atteindre une des épreuves, l'Andreia Trials dont il a trouvé les infos plus tôt.

- **Act 8 : Andreia Trials**

Tails arrive à la fin de l'endroit et trouve un E-100 détruit.

Tails réussi à avoir la signature d'énergie, il peut donc aller faire l'effet inverse de l'arme d'Eggman, en appliquant son plan depuis l'Hybris Altar.

- **Act 9 : Hybris Altar**

Tails active sa machine et provoque un grand flash. La ville est restaurée (techniquement, ça se passe quand peut avant que Sonic se fasse bannir). Le renard remarque que l'énergie ambiante dans l'air semble moins fort d'après ses outils de mesure. Cela devrait protéger aussi le reste des habitants, qui n'étaient pas bannis, et les appareils de communication devraient fonctionner mieux.

Il voit les reste du E-100, dont la reconstruction est en cours.

Eggman bypass son système de communication pour l'engueuler, et lui dire qu'il va voir de quel bois il se chauffe. Il dit également que cela ne fait rien : en faisant ça, Tails à décharger le trop plein de la faille avec l'énergie nécessaire pour ramener les habitants.

Il faudra du temps avant qu'elle se recharge assez, et que coup de chance, Sonic arrive lui rendre visite et ne se doute pas du piège. Et que les petits habitants que le renard ont sauvé n'auront pas beaucoup de temps non plus, ses roboticizeurs s'activent et apportent des badnics.

- **BOSS - Egg Charon**

Une fois que Tails à fini le boss, il voit que Sonic ne répond pas. Le renard se morfond, mais se dit qu'il doit continuer à se battre. Il doit détruire les générateur. Pour cela, il doit sortir de la base, et estime avoir quatre générateur à détruire.

## Partie 4 - Résistence

Tails est dans la zone principale. Si Tails rejoint la phytosphere, il croisera Amy qui lui dira que celle-ci a été détruite, et ils discuteront de la situation. (le dialogue est différent avant et après que Tails apprend que Sonic a réussi à se sortir de la faille).

> [!NOTE]
> Les trois acts (11, 12 et 13) suivants peuvent être fait dans l'ordre au choix du joueur, les cutscenes s'adapteront suivant l'ordre)


- **Act 11 : Arid Hills**

Tails est content d'en avoir détruit un, une zone pacifiée.

- **Act 12 : Tropical Ocean**

Tails a une notification indiquant que Sonic est détecté. Les deux communiqueront, Tails lui apprend sa situation, et Sonic la sienne. Cela rendra Tails plus confiant pour la suite.

Sonic lui dira d'aller vérifier aussi s'il n'y a pas une dans les Gosth Gadgets, Eggman en a sans doute caché une dans un endroit aussi difficile d'accès, mais Sonic dit qu'il doit aller le battre avant qu'il décide de rebannir les habitants.

Tails acquiesce, mais dit qu'il fera celle là une fois qu'il aura terminé les trois mettant le plus en danger les habitants.

- **Act 13 : Cryogenesis Grotto**

Tails détruit le générateur, et se prépare à aller dans la base d'Eggman détruire le dernier.

- **Act 14 : Gosth Gadgets** : Vers le début du niveau, un grand bruit d'explosion et pleins de message d'erreur se feront, indiquant la destruction du Egg Banisher

Tails détruit le dernier générateur, Sonic à vaincu Eggman, ils ont gagné ! Cependant, en remontant, il voit un message du dictateur, visiblement pré-enregistré, menaçant la ville de destruction de son Super Badnics Amélioré, Chaos Alpha (qu'Eggman décrit comme son meilleurs *chasseur*, et sa fiche indiquera qu'il s'agit d'un robot parfait pour poursuivre les cibles d'Eggman). Tails fait un commentaire sur à quel point Eggman est un mauvais perdant, et fonce dans la ville.

Une fois dans la ville, le renard peut rejoindre Chaos Alpha et le vaincre. Devant lui, Tails se souviens que c'est le robot qui avait harcelé Amy durant l'attaque de Station Square, et se demandera qui est la cible que le robot vise, mais n'aura pas de réponse.

- **BOSS - Chaos Alpha** (Non, c'est pas une blague sur le fait que c'est techniquement un "Chaos Zero", mdr)

Tails bat le boss, et sauve la ville. Cependant, il se demande pourquoi l'envoie d'un chasseur dans une telle situation. Cependant, le jeune héro peut profiter pour l'instant de son répis, et sera rejoint par Sonic qui le félicite.

> **Image de fin** : Sonic qui frotte la tête d'un Tails content mais un peu embarassé des compliments, avec en fond un Taranis se tenant tranquille.
