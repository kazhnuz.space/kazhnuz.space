---
layout: layouts/switchpages.njk
eleventySubNavigation:
  key: Sonic Monado Adventure
  description: Un mixt Advance et Generations
  parent: Vault Sonic
  order: 0
---

# Sonic Monado Adventure

Ce document est une mise au propre du scénario et de quelques éléments de design général que j’avais prévu pour mon projet de fangame « Project Millenium » (que j'ai nommé ici Sonic Monado Adventure), qui était un fangame Sonic en 2D, inspiré cependant de l’ère Adventure (pensez Sonic FGX, ou Sonic Fusion). Ce document contiendra donc tout ce que j’avais fait niveau scénario, backstory, personnages, etc. Il présentera aussi de manière générale les niveaux qui existaient dans le jeux, soit 7 niveaux de 3 acts, et 2 de 4 acts.

Le projet devait se dérouler entièrement dans la base de Monado Prime, une biosphère artificielle. Les personnages présents étaient Sonic the Hedgehog, Miles « Tails » Prower, Knuckles the Echidna, Shadow the Hedgehog, Amy Rose et Cream the Rabbit.

Le but n'est pas forcément de révolutionner la licence, et le projet n'hésitait pas à être fortement référentiel (par exemple de nombreuses structures du jeu sont inspirée directement de Sonic Adventure ou Sonic Adventure 2), étant un petit plaisir que je me faisais.

## Liste des pages

<ul>
  {%- for post in collections.all | sort(false, true, "data.eleventySubNavigation.order") -%}
    {%- if post.data.eleventySubNavigation.parent == eleventySubNavigation.key -%}
      <li><a href="{{ post.url }}">{{ post.data.eleventySubNavigation.key }}</a></li>
    {%- endif -%}
  {%- endfor -%}
</ul>

## Présentation du projet

Sonic : Monado Adventure était un fangame Sonic the Hedgehog inspiré des Sonic Adventure, mais en deux dimension. Le but était de mélanger avec un Sonic Advance les particularités et les possibilités d'un Sonic Adventure en terme de narration (une histoire construite avec des enjeux, du développement de personnage, ordre des stages dictés par le scénario et non scénario dicté par l'ordre des stages) ainsi que l'aspect plus "RPG" de ces titres (nouvelles capacités à débloquer, HUB world) avec le style générale d'un Sonic Advance.

Le jeux reprenait quelques idées des jeux Rush (notamment de Rush Adventure), tel que les matériaux ou les tableaux de missions, et s’inspirait légèrement de quelques tentatives de recréer en 2D des jeux Adventure-like ou de jeux similaires.

## Gameplay

Sonic Monado Adventure aurait du se jouer globalement comme un Sonic Advance, mais avec un bouton d'action supplémentaire pour les actions contextuelles, à la Sonic Adventure 2. Les personnages auraient eut un gameplay similaire à ceux qu'ils avaient dans les Sonic Advances ou Adventures. Le jeu proposera 6 personnages jouables dans l'aventure principale : Sonic, Tails, Knuckles, Amy, Cream, Shadow. Le jeu aurait contenu également des sub-games tels qu'une phase "Sky Chase" (plus proche de celle d'Adventure que de Sonic 2), des jardin des chao et quelques éléments à collectionner et améliorer.

Chaque personnage à le même core gameplay, avec les mêmes statistiques, le spin-jump et le spin dash seuls trois données changeront :

- L'action secondaire (B) - Généralement une attaque
- L'action aérienne (A+A) - Généralement un mouvement aidant au plateforming
- L'action secondaire aérienne (A+B) - Généralement une attaque, mais tout les personnages n'en auront pas

Le but est d'éviter de devoir gérer trop de complexité, et de réutiliser une majeure partie du code pour chaque personnage. La majeure partie des gameplays seront un mixt entre celui de Sonic Advance et de Sonic Advance 2.

### Actions des personnages

| Personnage | A + A | B | A + B |
|:-----------|:-----:|:-:|:-----:|
| **Sonic** | Jump Dash / Homming Attach | Glissade | Bounce Attack |
| **Tails** | Vol (8 sec) | Coup de Queue | Aerial Spin Dash |
| **Knuckles** | Planer | Coups de poing | Drill Drive |
| **Amy** | Double Saut | Coup de Marteau | Coup de Marteau Aerien |
| **Shadow** | Chaos Wrap (tp dans une direction au choix du joueur) / Homming Attach | Sommersault | Stomp |
| **Cream** | Vol (6 sec) | Chao Attack | Chao Attack |

## Déroulement du jeu

Le jeu devait être divisé en 6 histoires, chacune prenant 14 des 20 zones dans un ordre parfois un peu différents suivants les personnages. Il est à noter que contrairement à un Sonic Adventure, les variations d'ordres sont minimes. Les boss peuvent être différents suivant les personnages, et ne sont croisés que dans certaines des zones, formant une sorte de mélange entre un Sonic Adventure.

Le jeu peux avoir deux type de transition entre les niveaux :

- Soit le joueur peu repasser par l'Adventure Field. Dans ces moments, pour simplifier l'accès au niveau suivant, les Omochao présent un peu partout dans l'île indiqueront ou trouver le niveau suivant. Le conseil de les utiliser sera présent sur le premier écran de chargement du jeu.

- Soit il sera directement transmis au niveau suivant lorsque le scénario le demande.

Lorsque les six scénario sont fini, et que les 7 Chaos Emerald ont été récupéré dans le scénario de Sonic, le joueur peut sélectionner la Final Story, qui lancera un dernier mini-scénario amenant à un boss final en Super Form.