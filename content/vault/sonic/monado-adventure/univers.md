---
layout: layouts/subpages.njk
eleventySubNavigation:
  key: SMA - Univers et lore
  parent: Sonic Monado Adventure
  order: 2
---

# SMA - Univers et lore

> [!NOTE]
> Tout ce lore n'avait pas pour objectif d'être constamment explicitement dans le jeu. Il servait à nourrir surtout mon écriture, quelques dialogues que je pourrais mettre aux PNJ, et si je le sentais, créer un codex avec toutes ces informations.

Comme Sonic Adventure et Sonic Adventure 2, le projet visait à introduire sa propre histoire tout en étant lié à des éléments déjà existant des jeux précédant, afin de créer de la continuité sans verser dans l’histoire qui n’existe que pour expliquer des trucs des jeux précédant. Cependant, contrairement à SA/SA2, Sonic : Monado Adventure utilise explicitement un monde pour décrire l'univers de Sonic, ici le two-world, afin de l'utiliser pour construire son intrigue et les différents éléments qui vont en faire un jeu.

Le but de cette page est donc de décrire un peu ici les éléments du monde de Sonic utilisés et remixés pour former l'univers de ce jeu.

## Le Two-World

> [!NOTE]
> De nombreux éléments de cette vision du two-world viennent d'une autre version antérieur de mon envie de faire un "Sonic Advance+Adventure" : Sonic Adventure 0. Ce projet avait comme principe d'être une "désadataptation" des premiers épisodes de Sonic X, tentant d'imaginer ce qu'aurait pu être un jeu qui aurait inspiré cette saison 1 pour dans Sonic X (même si en vrai vers la fin ça devenait assez différent). "L'incident de Christmas Island" était le scénario de ce jeu, et le nom de Christmas Island sert pour donner l'idée aux humains que Sonic viendrait de cette île, puisque c'est ici qu'il a été vu pour la première fois.

Sonic : Monado Adventure se base au niveau de son univers sur le concept des deux mondes (à la Sonic X/IDWSonic… et avec un brin de stargate dedans) : Deux "mondes" (ici des planètes d'un même système planétaire), et les personnages peuvent passer de l'un à l'autre. Ces deux mondes sont ceux de la Terre, une planète organisée et peuplée surtout par les humains, et une planète surnommée nommé le "monde des animaux" (Mobius), peuplé surtout par des animaux anthropomorphiques intelligents. Mobius est dans ce lore un peu un équivalent de Mars dans notre système planétaire, et les deux planètes sont assez proche.

Le passage d'un monde à l'autre est conduit par l'existence de *Super Warp Ring*, des immenses anneaux qui ont le pouvoir d'être des passage entre les différentes planètes du système solaire, restent d'une ancienne civilisation. Ils étaient inactif depuis des milliers d'année quand les *expéditions interdimensionnelles* d'il y a 50 ans, puis *l'incident de Christmas Island* sur Terre ont provoqué leur réactivation, et la connexion des deux mondes. Depuis, les deux mondes se connaissent, et doivent coéxister, non sans quelques… réserves, parfois.

### Les quatres grandes civilisations

Les quatres grandes civilisations sont les quatres peuples qui ont atteint la possibilité d'être des civilisations interplanétaires, et qui ont succèssivements réussi à construire des créations hors de leur planète d'origines. Les quatres proviennent de Mobius, mais les humains sont en phase de devenir la *cinquième grande civilisation*, même si leur technologies spatiales manquent encore de capacités

- La **première grande civilisation** (-10 000 ans) est encore peu étudiée, parce qu'une grande partie de leurs créations ont disparu du fait de l'érosions. Ils ont construit de nombreux bâtiments dans tout le système solaire, et sont surnommé les premiers. Les plus grands restes de leur civilisations sur les deux planètes habitées seront insinué comme étant les *temples de gaïa*. Ils sont à l'origine des Warp Rings. Ils sont surnommés les *Premiers* et autres surnoms vagues du genre.

- La **seconde grande civilisation** (-9000 ans) est une civilisation centralisée, qui a surtout vécu sur Mobius. Elle sera insinuée comme étant un équivalent des *Trolls* d'ArchieSonic, mais sans plus de détails.

- La **troisième grande civilisation** (-7500 ans) est la *civilisation d'Erebe*, à l'origine des premières constructions explotant les *veines chaotiques*. Cette civilisation est le coeur du scénario de Knuckles, et Monado Prime est construit sur une faille utilisé par cette civilisation.

- La **quatrième grande civilisation** (-5000 ans) est la civilisation qui est à l'origine d'Angel Island et de robots de guerres nommée les Gizoïd. Il était prévu d'en faire une civilisation qui a réduit en esclavage le peuple des échidnés (le Knuckles Clan tel qu'on le voit dans Adventure sont des descendant d'esclave s'étant révolté durant la chute de cette civilisation, et récupérant les CE pour affronter des descendants de la civilisation).

### Le recontact

Le monde des animaux a été en partie redécouvert durant les grands débuts de la conquête spatiale terrestre, il y a de cela ~75 ans. La découverte des warp rings inactifs, le lancement de futurs projets comme le projet ARK, et l'exploration de Mobius par les premiers humains (dont le jeune professeur Gerald Robotnik, qui étudiera notamment beaucoup la similarité sur Terres de ruines et de représentations sur Mobius présentant des échidnés).

Cependant, le recontact se brisera lors du durcissement de la ligne politique terrestres il y a 50 ans, du à la fois de troubles dans les Federations Unis naissantes, et du shutdown de l'ARK du à des suspiscion envers le professeur Gérald Robotnik et l'incident de l'activation d'un robot antique.

## La Terre

La "Terre" de Sonic Monado Adventure est une terre alternative à la notre, ou les pays sont principalement remplacé par leurs Erzats, et où la forme des continent est légèrement différent.

Cette Terre possède comme principale puissance politique les *Fédérations Unis*, et comme plus grande force militaire les *Guardian Units of Nations* (ou GUN).

### Les Fédérations Unis

Les Fédérations Unis sont la république parlementaire avec un régime présidentiel qui couvre une grande partie de la planète Terre. Plus puissante nation de la planète, elle a subit plusieurs crise depuis le retour du Docteur *Ivo Robotnik* sur Terre lors de l'incident de Christmas Island.

Elles peuvent être vu comme quelque chose entres les Nations-Unis et les États-Unis. Le but de cet état est de garantir un développement tout en respectant les libertés des pays qui en font partie (et qui conserve une certaine indépendance). L'île de Sunlit Island est sous contrôle des Fédérations Unis, avec comme représentant le maire de la ville de *Sunlit Metropolis*.

Leur technologie est majoritairement contemporaire, mais a réussi de grande avancée depuis 50 ans à l'aide des technologies chaotique, en grande partie découvertes par le professeur Gérald

Dans le jeu, les Fédérations Unis, n'ont pas une présence très forte dans le jeu, en tout cas pas directement.

### Les Gardian Units of Nations

Les Gardian Units of Nations (ou G.U.N. parfois orthographié en GUN) sont la principale force militaire des Fédérations Unis, et ont comme rôle de protéger à la fois les Fédérations Unis, et à la fois les pays alliés. Cette armée est dirigé par un Commandant.

L'armée est actuellement en grande partie dédiée à affronter le Dr. Ivo Robotnik, qui vise à conquérir le monde, mais n’arrive pas à le localiser et à lancer des attaques précises. La capacité à disparaître de ce savant étant déconcertante, et ses moyens encore inconnus. L'inefficacité de GUN à vaincre les menances originaire du monde des animaux est la source d'une méfiance chez certaines personnes.

## Le Projet Millenium

Le projet Millenium est un vaste projet démarré par les fédérations-unis il y a de cela 30 ans, se basant sur les nombreuses recherches effectuées durant le recontact, ayant pour but d'être un projet global pour mettre la civilisation des Fédérations Unis au niveau des quatres grandes civilisations. Il contenait notamment un volet énergitique (provoquant la fondation de Rimlight et des Chaos Central), d'un volet militaire (fondant les satellite militaire Erinye) et d'un projet spatiale (provoquant la fondation des bases Monado).

Ce projet devait faire des Federations Unis une civilisation spatiale, la "Cinquième Grande Civilsiation", et d'entrer dans un nouvel age d'or, mais s'est partielement ralenti avec l'incident de la Gosth Central qui provoquera la mort du Général XXXXX, il y a 12 ans. Le volet militaire et le volet spatial ont été mis de côté pour concentrer les ressources sur le projet énergétique, permettant à la civilisation terrestre d'avancer vers l'énergie propre.

## Les bases Monado

Le projet Monado est le projet qui a entrainé la fondation de la base de Monado Prime, et de possibles futures autre base de type Monado (qui ne furent jamais construites). Il s'agit d'un projet ambitieux créé par plusieurs grandes acteurs des Fédérations Unis, et qui a eu un effet considérable sur le développement technologique de la fédération. Le but de ces bases étaient d’illustrer la meilleure vie qu’ils auraient tous dans ce nouvel age d'or, futur que de préparer une future expansion spatiale.

Les bases types « Monado » sont des biosphères artificielles utilisant l'activité bactérienne pour créer un système écologique pro. L'idée est de créer un espace qui pourrait être hermétiquement fermé et vivre sans besoin d'apport extérieur. Des usines bactérienne s'occupent du plus gros de recyclage de l'air et des déchets biologiques, permettant de produire à la fois de l'oxygènes et des engrais vert pour les espaces de cultures.

Ces bases peuvent abriter plusieurs centaines de milliers de personnes, et sont capable de vivre en autarcie presque totale, les deux éléments les plus important à leur survies étant : du soleil (pour la survie des biomes artificiels) et une sorte d'énergie performante (le plus souvent des Chaos Central). Le but est d'à la fois construire une base de recherche, une expérimentation dédiée à la future colonisation spatiale,

### La construction de Monado Prime

Monado Prime est la première base de type monado construite et terminée. Elle a été ouverte officiellement il y a de cela 26 ans, après 7 ans de construction, sur l'île de Fortitude Rock. Ce site a été choisi au début du projet, il y a 35 ans par les scientifiques de Rimlight, fortement intéressé par la présence d'une faille chaotique extraordinaire, le Gosth Rift, mais également de ruines de la civilisation Erebienne, une antique civilisation qui avait exploité la faille et avait pu vire sur l'ile au climat ingrat.

Après de dure négociations, ils purent construire la base, ansi que la Gosth Centrale, la première véritable Centrale Chaotique. L'archéologue Morrigan et la chercheuse Dahut, toutes deux mobiennes, firent partie de ce projet.Grâce à celui-ci, une base Monado fut créé, la première, qui permis à toute une petite vie de se faire sur l'île.

Le projet continua sa vie normalement, s'améliorant et permettant de nombreuses découvertes, jusqu'à il y a 12 ans, un mystérieux incident arrivant dans la couche inférieur de la base, la Gosth Central. Personne ne sait exactement ce qui s'est passé, et l'incident est maintenant oublié de la plupars des nouvelles personnes travaillant dans la base, mais depuis, la base doit se fournir en énergie via une centrale affilliée construite par HexaECO. Un biome a également été interdit au public depuis cet incident.

### La base aujourd'hui

Aujourd'hui, la base est une cité semi-indépendante dirigée par un commité scientifique et un conseil de citoyen, qui tourne un peu plus au ralenti qu'à l'époque et n'a jamais atteint le nombre d'habitant révé par ses créateurs trop optimistes. Si elle a été amélioré et modernisée avec le temps - bien qu'elle était très en avance sur son temps - elle produit moins de découverte et d'évolution. Cependant, sa réputation reste d'être le futur des Fédérations Unis, et elle est un franc succès sur le plan de l'autarcie.

## Rimlight

La société Rimlight est l’un des éléments scénaristique important du jeu. Holding co-détenu par des actionnaires privés et par le GUN, la société recherche dans le domaine des énergies propres, et plus précisément dans le domaine des énergies chaotiques. Elle est la plus grande réussite du projet militaire.

Ils utilisent notamment pour cela les thèses de Gerald Robotnik sur ce type d’énergie, quand il était chercheur à l’université de Central City, thèses qui avaient conduit Gerald à faire des recherches sur les civilisations anciennes échidnés, pour mieux comprendre leur rapport à l’énergie chaotique (d’où les découvertes sur Chaos, la prophétie échidné, Gizoïd…) et le fait qu’il fut choisit comme chef de l’équipe du projet Shadow. Ils sont les producteurs des Chaos Drive, et des Chaos Central.

### Les Chaos Central

Les Chaos Central sont une source d’énergie de plus en plus présente sur toute la planète. Construite sur des zones nommées les Veines Chaotiques (Chaos Vein) et sont utilisées depuis environs vingt ans (pour le marché publique) à vingt-cinq ans (pour les Guardian Units of Nations). L’énergie produite par les Chaos Central revet deux forme : l’éléctricité, ou les Chaos Drives, des batteries chaotiques extrêmement durable utilisée notamment par les mécha des Guardian Units of Nations.

L’intérieur d’une centrale chaotique est cependant un endroit dangereux à cause de l’influence de l’énergie chaotique et de sa transformation en électricité ou en *Chaos Shard* pour les Chaos Drive, et est réservé à des connaisseurs de l’énergie chaotique. Le cœur d’une centrale chaotique, nommé le *Chaos Core*, est un endroit ou l’énergie circule suivant des patterns très précis.

C’est la deuxième source d’énergie la plus utilisée, après l’énergie thermique liquide produite par la société HexaECO, utilisée depuis à peu près la même période. Contrairement aux centrales HexaECO, les centrales chaotiques ont le defaut d'être extrèmement limité en terme d'emplacement, ayant besoin de Chaos Vein pour fonctionner.

### Les Chaos Vein

Les Chaos Vein (Veines Chaotiques) sont la source de l’énergie utilisée dans les Chaos Central du conglomérat Rimlight. Il s’agit de zones d’émission spontanée d’Énergie Chaotique qui existent à quelques endroits de la planète, relativement rares mais trouvable sur tout les continents. Même si ces failles sont rien face à des Chaos Emerald, il est possible de produire de une énergie propre et durable à l’aide de ces failles. Quand elles ne sont pas sur-exploité (comme ce qui est arrivé dans un incident avec l’une des premières centrales chaotiques), ces failles sont contrairement au Chaos Emerald particulièrement docile à utiliser.

Les Chaos Vein sont des failles vers des univers-bulles qui leur sont propre (des sortes de Special Zone à petite échelle, mais qui ne contiennent pas de Chaos Emerald) et qui est la source de cette énergie. C’est ce qui fait l’aspect durable et stable à la fois de cette énergie : seul très peu d’énergie s’échappe de la veine, mais la reserve qu’il y a derrière est largement au dessus de toute la consommation humaine planétaire depuis les débuts de la révolution industrielle, au bordure de l’immesurable.

## Le projet Erinyie

Le projet Erinye est la raison pourquoi GUN s’est intéressé à la veine chaotique, mais un produit "secondaire" faisant partie du projet militaire. Ce projet dirigé par Ys Gradlon consistant en une nouvelle expérimentation sur des lasers à énergie chaotique, basé sur les mêmes technologies qui avaient donné l’Eclipse Cannon. Mais le but était que l’énergie serait envoyée depuis la centrale vers les satellites, qui la concentrerait en tir d’énergie chaotique.

Le satellite principal retransmettrait l’énergie vers la batterie de satellites dispersés autour du globe, qui pourraient ensuite s’occuper de la concentrer et de faire le tir dans leur zone d’action. N’importe quel endroit du globe pourrait alors être touché par un tir d’énergie chaotique. Ce serait l’arme ultime.

## L'incident de Monado Prime

Si ce ne fut pas la "chute" du Projet Millenium, l'incident de Monado Prime est ce qui le transformera en un projet de recherche énergétique. Quelques semaines avant l'incident, les Federations Unis ont commencé à subir des troubles. La fédération planétaire, se retrouva face à des troubles indépendantistes important, et à des groupes terroristes qui refusaient le choix de former une nation globale. La paranoïa commença à monter, même envers les employés de GUN, dont une partie était pour juste une organisation qui regrouperait des états indépendants

Certains partisant de la ligne dure des Federations Unis commencèrent à voir des ennemis et des complices des indépendantistes et des terroristes partout. Ys Gradlon fut de ceux qui furent touchés. Il voulut utiliser le projet Gosth Central et Erinye pour se débarrasser de ses adversaires. Il fit venir de nombreux soldats pour sécuriser cette évolution.

Dahut, voyant l’évolution du projet, commença à prendre peur. Elle tenta de s’opposer à Gradlon, mais celui-ci la fit enfermer. Le travail fut maintenu, tandis que le Chaos Core fut encore travaillé par une plus petite équipe, une partie des membres étant dévoué à la stabilité de la veine. Dahut finit par s’évader, et forma une machine permettant de saturer l’énergie chaotique en la renvoyant en boucle infinie. Elle préférait saboter le projet, et mettre en jeu la vie d'une partie du personnel de la centrale que de laisser Gradlon faire ses plans.

Lors d'un test, elle provoqua donc un sabotage qui provoqua une surtension, détruisant la centrale (qui ne sera remis en état que des années plus tard par Eggman) et contamina une partie des ruines Erebienne. Dahut, Morrigan et Ys Gradlon disparurent dans l'incident. Le reste de la base (grace aux système de protection hermétique) ne fuit pas touché.