---
layout: layouts/subpages.njk
eleventySubNavigation:
  key: Concepts JDR Meteors Sonata
  parent: Univers de Space Opera
  order: 3
---

# Concepts JDR Meteors Sonata

Ici je mets en vrac quelques concepts que j'avais eut pour l'univers, et qui devaient avoir une certaines importances. Ces concepts en étaient qu'à la phase brainstorming, et certains ont été réutilisé pour former les *spécialités* (classes non magiques) d'Erratum/DanseRonce.

Le JDR devait utiliser le même système D100 que mon JDR Erratum.

## Liste des classes

Les classes de Meteors Sonata étaient basé sur l'idée d'avoir des classes basées sur quatre type d'action : DPS, Healer, Tank, Trickster (les derniers étant ceux qui font un peu de tout). Le but niveau "lore" était de présenter des éléments qui rappelaient certains aspects de divers univers de science-fictions (mis entre parenthèse et en italique)

Liste des classes prévues :
- **Fantassin Blindé** : Tank avec un peu de DPS, synergie avec Colonel *(référence à Doom, Halo et Metroid)*
- **Archéologue** : Trickster avec pleins de truc pour le lore/les technologies anciennes, les capacités lié à l'énergie *(référence à Daniel Jackson de Stargate)*
- **Médecin** : Soigneur, quelques bonus
- **Chercheur** : Capacité de produire des trucs chimique, analyse liée à la physique/astrophysique
- **Soldat** : DPS distance, multiarme, un peu tank, fort en troupe, synergie avec Colonel
- **Mécanicien/Ingénieur** : Peut créer des armes, avoir un "familier robot", réparation de machine.
- **Colonel** : Capacité de donner des ordres, donner des bonus, bannière, DPS
- **Suiveur du Néant** : DPS "magique", trickster, chips, un soin dangereux *(mélange de référence à Lovecraft et aux Ombres de Babylon 5)*
- **Chevalier de l'Energie** : DPS "magique", un peu de soin, influence *(référence aux Jedi de Star Wars)*
- **Adepte du Spectre** : Discret, Roublard
- **Contrebandier/Chasseur de Prime** : DPS distance, Argent *(référence à Han Solo)*
- **Civil/Politicien/Diplomate** : Soin, aide morale, bonus aux alliés *(référence aux civils dans Stargate, notamment Stargate Universe)*

Répartitions des classes suivantes des combinaisons Healer/DPS/Tank/Trickster
- Healer (pur) : Médecin
- DPS (pur) : Soldat
- Tank (pur) : Fantassin
- Trickster (pur) : Archéologue
- Healer+Tank : Colonel
- Healer+DPS : Chevalier de l'Énergie
- Healer+Trickster : Civil
- DPS+Tank : Chasseur de Prime (Tank Esquive)
- DPS+Trickster : Suiveur du Néant
- Trickster+Tank : Adepte du Spectre
- Inclassable : Chercheur (tout)
- Inclassable : Mécanicien/Ingénieur (tout)

Une volonté que j'avais était de potentiellement ajouter comme type de personnage la "Star" et séparer Contrebandier et Chasseur de Prime dans les classes.

## Customisation

Quelques idées en vrac pour la customisation que j'avais :

- Customisation des armes/outils (mise en avant du système de transformation via un aspect crafting) : Remplacement des armes par quelques "bases" et mise de tout les effets (dont ceux speciaux de certaines armes) par des modifications
- Outils spéciaux : Clef à Molette Multifonction (Ingénieur) et Excavateur Sonique (Archéologue) et d'autres ? (un instrument de musique pour la star, un analyseur pour le chercheur ?). Outil spécial pour le médecin aussi ? Peut-être en fusionner quelques-uns de cette liste
- Customisation des personnages (façon un peu cyberpunk) : Cyborgs, transformations biologiques, améliorations mentales/spirituelles/idéologiques
- Ingénieurs ont leur propre robots, chercheurs peuvent avoir leur propre aspects
- Crafting de vaisseaux ?

## Classes d'éveil

Le système d'Erratum possède une stat de "Métaphysique" que je comptais réutiliser, inspiré de la clairvoyance utilisée dans un JDR d'un de mes potes. J'avais envie de l'utiliser ici aussi pour ajouter une notion d'éveil : quand la stat de métaphysique passe à 100, l'esprit serait happé par le monde métaphysique et peut devenir un être "éveillé". Changement radical de gameplay, nouveau set de classe pour les personnages éveillés.

Parmi les idées que j'avais, il y avait de m'inspirer de Personna et des notions autour du Psyché et de l'identité (genre des archétypes de personnages, etc). Je voulais aussi m'inspirer de la philosophie, et évidemment à la fois des Anciens de Stargate et des fantômes de forces de Star Wars.

L'idée était de faire de ces classes des classes encore plus de support, puisque ce serait aussi une forme de re-roll.