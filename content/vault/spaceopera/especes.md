---
layout: layouts/subpages.njk
eleventySubNavigation:
  key: Espèces Space-Opera
  parent: Univers de Space Opera
  order: 0
---

# Espèces de Space Opera

Un set d'espèce utilisée pour ce JDR *Space Opera* que je préparais. S'il n'a pas été utilisé tel quel, j'en ai repris certaines dans divers univers.

## Les espèces solaires

Les espèces solaires sont les espèces originaires du système solaire, qu'elles soient naturelles (comme les humains) ou artificielles (comme les Versatiles ou les Zoomorphes).

Ces espèces sont soit organiques (Zoomorphes et Humains), soit technologiques, mais fondé sur des principes proche des cellules humaines.

### Les humains

Les humains sont l'espèce originaire de la Terre, du Système Solaire. C'est une espèce vivant dans des planètes tempérée (15 °C en moyenne), avec une chimie fondée sur le carbone et l'eau.

#### Les phosphoriens

Des humains qui ont été plusieurs siècles en contact avec le voyage interdimensionnel. Tous les humains ne sont pas Phosphorien.

### Les Zoomorphes

Une espèce créée artificiellement par les humains lors de la crise des robot-travailleurs.

Il s'agit d'une espèce intelligente, qui a été créé en 2232 parce que finalement plus simple et moins cher à reproduire qu'un robot sophistiqué tel qu'un versatile. Ils ressemblent à des animaux anthropomorphiques et sont composé de différentes espèces animales suivant leur objectif de création.

Ils sont restés une espèce vu comme "de serviteur" (considéré comme des animaux, puisqu'ils étaient basés sur les animaux) jusqu'à 2288 où ils obtinrent le droit d'être des citoyens légaux de la confédération solaire.

Cependant, ils sont souvent vus comme plus "animaux" qu'humain. La peur des "instinct zoomorphes" et du fait qu'ils seraient un "vecteur de transmissions des maladies animales aux humains", qui n'a aucune véritable origine scientifique, sera souvent source de discrimination.

### Les versatiles

Des robots capables de changer d’apparence à volonté, étant composé de nanorobot remplaçant les cellules. Ils ont été construits en 2212 lors de la grande phase de recherches technologique du début de la confédération solaire.

Ils ont été importants d’abord sur Céres, suite à la révolte robotique et zoomorphes de 2266.

Ils sont une espèce se méfiant beaucoup des humains et sont souvent vu comme des "machines sans âmes" par beaucoup. De nombreux versatiles construirent leur propre fédération galactique par la suite, à cause des tensions avec les humains.

## Les Centauriens

L'espèce qui s'alliera le plus vite aux humains, les Centauriens avait eu des contacts avec les humains bien avant leur rencontre officielle, donnant lieu à de nombreuses légendes (les "Martiens", les "petits hommes gris", "l'homme de Roswell").

Espèce humanoïde grise au cerveau disproportionnément grand, cette espèce vient à l'origine d'Alpha Centauri. Basé sur une chimie quelque peu différente de là notre, ils ont besoin de stabilisateur pour tenir à nos températures et pressions (ces stabilisateurs forment des champs protecteurs). Cependant, ils ont créé via bio-ingénierie une sous-espèce nommée les "Centauriens terrestres", qui peut tenir dans des conditions terrestres (mais à besoin d'un respirateur artificiel, trop d'oxygène leur étant toxique), à températures cependant en dessous de 40 °C.

Ils se sont allié très tôt avec les humains et ont beaucoup participé aux projets de la confédération solaire, tel que le projet Dandelion.

## Les Eirons

Les Eirons sont des créatures cristallines vaguement humanoïdes. Ils ont de longues crêtes de "plumes" cristallines retombant jusqu'à leur deux queues, qui en sont aussi pourvues. Leur tête ressemble vaguement à un mélange entre des oiseaux et des poissons, pourvue de sorte de branchies et d'un grand bec.

Plus grandes que des humains (approchant des 2 mètres et demis), elles ont six membres (tous doté de deux doigts à la fin), ainsi que deux appendices dorsaux considérés comme des queues. Ils se déplacent en nageant (ils sont originellement aquatique) ou utilisent leur six membres constamment pour se déplacer, n'ayant pas de station debout permanente.

## Rekts

Les Rekt sont une espèce minérale plus petite, atteignant difficilement le mètre. Pourvu comme les humains de quatre membres, ils ressemblent à des sortes de grands lézards de pierres.

Une théorie sur leur origine serait qu'ils seraient à l'origine des êtres artificiels créés pour survivre dans toutes les conditions, mais d'autres éléments racontent qu'ils seraient la plus ancienne des espèces du système de Roma.

Ils n'ont que très peu de limites sur les territoires qui leur sont vivables : ils peuvent supporter non seulement des températures très froides et très chaudes, mais n'ont pas vraiment de besoins respiratoires.

Ils sont souvent considérés comme peu malins.

## D'krein

Cousin des Rekt, les D’kraiens en sont une espèce de géante et ailée, mais avec une armure de pierre moins présente. Ils sont nommés parfois les « draconiques », mais ce nom a été donné également à d'autres espèces à travers les âges (notamment à l'époque de phosphorus, où une grande espèce recevra ce nom).

Ils font généralement entre 2 à 3 mètres et peuvent atteindre jusqu'à une dizaine de mètres. Ils ont une longévité incroyable, dépassant plusieurs siècles.

Il existe une sous espèce sur une planète spécifique, encore plus grands (ils peuvent atteindre les 5 mètres) et avec une longévité encore plus élevée.

## Urthoïdes

Les Urthoïdes sont une espèce insectoïde vivant sur des planètes avec de fort taux en arsenic. L’atmosphère terrestre leur est fatale. Doté de carapace solide et de pince coupante, ce sont de formidable combattant. Leur technologie est principalement fondée sur les biotechnologies. Population très nombreuse à la reproduction rapide, leur patrimoine génétique est modifié suivant leur rôle dans la société.

Cette espèce est une espèce connue pour avoir été particulièrement expansionniste, jusqu'à la chute de leur civilisation galactique, suite à une guerre civile.

Les Urthoïdes ont une individualité, mais pas véritablement de notion de "soi" comme étant opposé au groupe. Cela dit, leur individualité s'exprime dans l'une des plus grandes importances de leur civilisation : le jeu. Le jeu est chez eu la forme ultime de l'art, celle qui à partir de règle permet d'inventer des expériences. Chez les Urthoïdes, trouver des règles de jeu amusantes est encore plus haut que les arts graphiques ou auditifs. L'humour est aussi très important, mais l'humour urthoïdes est souvent difficile à comprendre chez les autres espèces. Ceux-ci en sont parfaitement conscient... et du coup le font encore plus.

L'amusement et l'humour sont donc deux des vertus les plus importantes chez les urthoïdes.

## Le Spectre

Le Spectre, "Parasite Ultime" est le nom donné à une créature extraterrestre composé de bactérie intelligente lié entre elles. Cette créature parasitique, ayant le pouvoir de prendre contrôle de nombreuses espèces en les infectant, peut prendre également l'apparence d'une forme de brume fantomatique.

Si "le spectre" est souvent utilisé au singulier puisqu'il existe un esprit de ruche entre les différentes bactéries le composant, en vérité cet esprit ne fonctionne que sur l'échelle d'une planète, ce qui fait que chaque planète atteinte par le spectre possède un spectre différent. Ils peuvent cependant communiquer entre eux grâce à diverses technologies, ce qui fait que le Spectre fait partie des civilisations à être une nation galactique malgré son fonctionnement radicalement différent.

Le spectre est capable de s'adapter à plusieurs chimies, plusieurs types de température. Il est vu comme le "parasite ultime". D'après les légendes, ce serait à l'origine une arme.

Le Spectre à une réputation très mauvaise, celle d'être sournois, incapable de toute invention et de voler la technologie à ses "hôtes". S'il est vrai que cette créature fonctionne par "parasitage" d'une certaine manière, d'une autre elle fonctionne par symbiose, puisque l'esprit de la personne parasitée est le plus souvent copié dans la ruche, ce qui fait qu'il en fait partie, d'une certaine manière.

Cela fait de chaque spectre une créature singulière, de par les esprits et la culture des peuples contaminé. Cependant, le plus souvent, ce sont des espèces "primitives" qui sont capturé, le spectre pouvant apporter l'intelligence de lui-même. De nombreuses planètes sont remplies de créatures "sauvages" parasitée par le spectre, formant le plus gros de leur empire.

D'un point de vue galactique, ce peuple est très peu guerrier et pratique plutôt l'espionnage. Les espions spectre sont des êtres parasités par le spectre, avec une technologie limitant en partie l'esprit de ruche, ne permettant de transmettre que quelques informations entre les différents individus parasités sur la planète.