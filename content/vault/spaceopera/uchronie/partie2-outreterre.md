---
layout: layouts/subpages.njk
eleventySubNavigation:
  key: 2 - L'outre-terre
  parent: Uchronie Solaire
  order: 1
---

# Les temps de l'outre-terre (2097-2177)

La période de "l'Outre Terre" est toute la période durant laquelle les états de la Terre auront une influence directe sur les humains habitant dans l'espace.

Cette période se terminera avec les grandes révolutions solaires.

## Les 25 optimistes (2097-2120)

La présence de 50 millions de terriens dans l'espace commença à augmenter les questions sur l'influence des états terrestres. C'est en 2097 que sera adoptée la grande réforme de l'OICSA, donnant plus de force aux états terrestres, démarrant ce qu'on nomme traditionnellement l'ère de "l'Outre-Terre".

Le principe de cette réforme sera d'autoriser la "sponsorisation" de chaque base et ville par un pays ou une organisation en particulier, qui deviendra in-fine une portion hors terre du territoire du ou des pays qui sponsorise - une même base pouvant être sponsorisé par plusieurs entités.

Les entreprises et associations furent également autorisé de sponsoriser des bases. En effet, de nombreuses bases furent construites par des groupements culturels ou politique, des religions, voire des entreprises privées. Et même si un garde-fou fut instauré avec la « Constitution partagée de l'OICSA », ayant comme objectif de pouvoir être accepté par tous les pays finançant l'OICSA, elle gagna rapidement un côté très minimaliste, ne garantissant que vaguement les libertés individuelles et les droits sociaux. Et si certaines expérimentations politiques y furent particulièrement intéressante (telle que des expérimentations d'utopie politique de partage) grâce à la présence de majoritairement des gens intéressés pour faire vivre ces utopies, de nombreux groupes eurent plus comme objectif de s'affranchir de toute forme de besoin de partager leurs richesses.

Le système connaîtra vite des dérives, et même s’il est faux de dire que l’OICSA n’a plus aucune influence, il en perdit énormément aux profits d’états ou d’entreprise privés dans chaque base. Cela provoquera de sérieux soucis de corruption et de conflits d’intérêt. Cependant, tout ne sera pas accepté, et une tentative de la mafia de sponsorisé directement et ouvertement une ville-bulle fut empêché en 2113.

Cependant, cela n'empêchera pas les villes spatiales de vivre un formidable accroissement de la population. En effet, on passe de 50 millions en 2097 à 115 millions en 2120, notamment grâce à la constructive massive. Ce fut également une période d'augmentation radicale des vaisseaux spatiaux. Les vaisseaux spatiaux 100% réutilisables devinrent la norme, et d'immenses cargos spatiaux commencèrent à être construit.

Tout était prêt pour "l'exploitation de l'espace". Et tous les ennuis qui en découleraient. C’est en ceci que c’est généralement considéré comme la fin du projet « nouvelle frontière ».

C’est ce nouveau palier franchi qui fera changer une grande partie des avis négatifs sur l’habitation solaire : D’une « lubie », cela devint un moyen d’avoir plus de ressources à exploiter, ainsi que la construction d'un futur radieux. D’un « rêve un peu fou », on est passé à une entreprise industrialisée, où le but n’est pas simplement d’avoir des habitants pour étendre l’humanité, mais avant tout d’avoir une force de travail sur les planètes où se trouvent les nouvelles ressources.

## Conflits d’influences (2120-2152)

C’est en 2120 qu’un accident arrive dans une petite ville lunaire, *Su’en*, ce qui forcera à devoir l’abandonner. Malgré les systèmes d’auto-réparation des cités lunaire qui la rendra de nouveau habitable sans risque dès les dix ans après, elle gardera éternellement l’image d’une ville maudite, ce qui en fera une ville fantôme et une sorte de squat. Cette période est marquée par une grande baisse de la croyance en la conquête spatiale. En effet, elle ne rapportait pas énormément et coûtant assez cher, de nombreux pays commenceront à douter en le bien-fondé de ce projet. Et surtout, comme l’incident de *Su’en* rappellera longtemps une vérité : la conquête spatiale est dangereuse.

Tout cela se combinera avec une très forte instabilité dans le contrôle de l’OICSA. En effet, le financement de l'outre-terre par différents pays et organisation fit renaître des tensions, s'ajoutant à celles de l'exploitation. En effet, de véritable guerres diplomatiques se firent entre des pays et entreprises pour obtenir des visa d'exploitations des astéroïdes. Une loi anti-monopole interdit en 2124 la possession de plus de 0,01% de la ceinture d'astéroïde une seule entreprise. Cette loi provoquera un scandale en 2128, quand un entrepreneur tentera via des sociétés écran de s'approprier un pourcentage plus grand et fut *interdit à vie de tout visa d'exploitation des astéroïdes* avec retrait de TOUT les visa déjà existant.

Cela fit grand bruit, et commença à donner la réputation à la conquête spatiale d'être un immense "Far West" ou les entreprises faisaient ce qu'elles voulaient.

Sur les planètes en cours de terraformation, les expériences et travaux continueront, montrant des signes d’avancement. Cependant, cela sera souvent mis de côté dans les grands médias, et beaucoup croiront que les projets sont tout simplement morts. Cependant, il serait faux de croire qu’il n’y a pas eut d’accroissement de la population. En effet, on passe de 115 millions en 2120 à 135 millions en 2152. C’est cependant une stagnation si on compare ces chiffres à celles des périodes avant et après.

### Les autres formes de vie du système solaire (2130 – 2143)

Si une grande partie de la conquête spatiale avait été accélérée par le projet Nouvelle Frontière, la recherche d’autres formes de vie avait été fortement ralenti, en grande partie parce que le budget qui aurait dû être dédié à cela était toujours redirigé vers la colonisation spatiale de la partie interne du système solaire, où aucune trace de vie n’a été trouvé. Cela fit que malgré la présence de technologie suffisante pour faire des recherches sur la présence de vie dans les satellites des géantes gazeuses, ce ne fut pas un centre d’intérêt pour les humains.

Cependant, avec l’arrivée de la colonisation après la ceinture d’astéroïde, les budgets commencèrent à être débloqués. Et le 2 janvier 2130, la première forme de vie extra-terrestre est découverte par une sonde automatisée, sur Titan, dans un lac d’hydrocarbure. Les scientifiques découvrirent des formes de vie adaptée au froid intense de la planète, utilisant le carbone comme base de sa biochimie, un polymère génétique plus proche de l’ARN que de l’ADN et un solvant différent que l'eau (le méthane). Cette forme de vie vivait en grande partie de réaction chimique entre les hydrocarbures. Des études commenceront sur le sujet. Deux ans plus tard, des traces de vie organiques furent découvertes par radiation dans les océans souterrains de Titan, Europe et Ganymède. D’autres recherches continuèrent pour ceux d’Encelade et Triton.

Cependant, cette découverte de la vie sera à l’origine de trouble sur Terre, quand un éditorialiste célèbre, proche des thèses de l’Homo Novus (qui défend l’idée d’une justice de l’inégalité, lié à une nature intrinsèque des personnes) émettra à la télévision publique l’hypothèse d’un risque de contamination par « un virus Titaniens » et s’étonnera du « manque de communication sur le sujet ». S’il n’y avait pas de virus sur Titan découvert – et les bactéries découvertes ne pourrait survivre dans un environnement terrestre, cela sera suffisant pour provoquer des mouvements de paniques. Les mouvements pro-Terre reprendront souvent le « risque du virus extra-terrestre » à partir de ce moment-là.

Cela n’arrêtera par les recherches sur la vie, et même sur Terre il y aura une certaine excitation lors de la découverte suivante. En effet, 6 ans plus tard, des formes de vie multi-cellulaire furent découvertes par les premiers forages jusqu’aux océans d’Europe et Encelade. Elles ne furent pas découverte par radiations, parce que se trouvant au niveau de sources d’énergies géothermales. Si ces formes de vie étaient assez différente d’un point de vue chimique, les mécanismes de convergence évolutive furent remarqué : Certaines de ces espèces avaient en effet des formes extérieures proches des poissons et d’autres animaux marins, tout en étant pour certaine bien plus similaire au niveau interne aux champignons qu’aux animaux.

Même si aucune forme de vie intelligente furent trouvées dans les différents océans souterrains du système solaire, il était désormais connu que nous n’étions pas seul dans l’univers, et que s’il y avait de la vie dans quatre astres du système solaire, il y en aurait sûrement ailleurs. Pour certain, c’était l’espoir de ne pas être seul.

Mais pour de nombreuses, de très nombreuses autres personnes, c’était une peur. La peur que toutes les invasions aliens qu’on avait vus dans un film, dans un livre se réalise. La peur qu’un jour, un peuple intelligent et plus puissant viennent nous anéantir.

## L'apogée l'Outre-Terre (2152-2177)

C’est en 2152 que seront atteint les objectifs premiers de la colonisation de Mars : La création des « supraécosphères ». Ce projet était un projet de réalisation d'écosphères type Monado autonomes vivables, mais cette fois de millier de kilomètres² de superficie (environ la taille d'un département) avec des premières plantes réussissant à pousser *directement* dans le sol martien.

Dans ces bases hermétiques, on y trouvait toutes les conditions idéales pour la vie terrestre : Atmosphère vivable non polluée, cycle de l'eau et présence d'eau liquide en surface, quelques reproductions de saisons - mais cependant à l'échelle de l'année martienne. Cependant, il a fallu près de 40 ans avant de réussir à entièrement fertiliser les sols des bases martiennes, ce qui aura de nombreux effets négatifs sur les prix de l'alimentation.

Environs 20 ans plus tard, la première écosphère Venusienne fut commencé en travaux, les technologies étant différente pour faire face à une surpression externe plutôt qu'interne. À partir de ce moment-là, le nombre de bases commença à croître très vite sur les deux planètes.

La taille de ces bases fut un grand catalyseur pour l'avancée de la vie dans l'espace. Contrairement aux écosphères classiques, les habitants ne se sentaient plus autant à l'étroit dedans. Il était possible d'avoir des forets, des cours d'eau, et bien plus de vie dedans que jamais.

La planète fut alors divisée en territoire par un conseil de l’OICSA contenant des membres des nations les plus riches, des grandes entreprises d’exploitation spatiale et des pays en développement. Les débats seront houleux pour savoir qui à quoi, et finalement les planètes sont divisées de la manière suivante : 20 % seront autorisé à la possession privée, 35 % seront aux pays les plus riches et le reste des pays devront se partager les 25 % qui restent. Il faut rajouter qu’à l’intérieur même de ces portions, le partage sera très inégal.

Cette division provoquera de nombreuses critiques, mais surtout de grands changements dans la politique de population de l'espace : Là où les pays avant se fondaient surtout sur l’utilisation de nouvel espoir, ils vont rajouter à cela d’autres méthodes plus douteuses, tel que l’acceptation d’aller vivre sur Mars devient rapidement une condition sine que non aux remises de peines.

Beaucoup de clandestins, reçoivent le choix d’aller travailler dans les deux planètes où d’être renvoyé dans leur pays d’origine. Il est même très fortement conseillé aux personnes en situations de chômages de quitter la planète Terre. L’argumentation dans ces cas est toujours fondée sur l’idée que c’est dans ces planètes dynamiques qu’ils auront le plus de chance d’avoir une vie correcte. Certains pays vont même jusqu’à faire des campagnes d’émigrations massives vers ces planètes, se basant officiellement sur le volontarisme, mais souvent avec des pressions très fortes.

Cependant, une chose est sûre, c’est que ces campagnes ont été efficaces en terme d'accélération du nombre d'habitants dans le reste du système solaire. Pendant les trente-cinq ans qui ont suivi, le nombre d’habitants dans l’espace a grimpé exponentiellement, d’autant plus qu’il y a de fortes politiques natalistes sur ces planètes. On passe d'une petite centaine de millions d’habitants dans l’espace en 2152 à 850 millions d’habitants en 2177, une grande majorité se trouvant sur Mars.

Cette période fut également une grande période d’accroissement des richesses pour les pays exploitant la planète. Mais ce fut également une période parfois de conflits entre les territoires ultraterriens et la Terre, comme nous en parlerons plus tard, et cela en grande partie à cause des méthodes employée pour les peupler et d'une de ses conséquences logiques, à savoir un certain mépris qu'il y avait de la Terre pour Mars et la Lune.

Cependant, c’est dans les années 80 du vingt-deuxième siècle que s’arrêtera brusquement cette ère, avec l’indépendance de Sélénite et la naissance de la Confédération Solaire, ce qui ira jusqu’à entraîner une guerre sur la Terre.
