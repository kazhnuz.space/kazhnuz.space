---
layout: layouts/switchpages.njk
eleventySubNavigation:
  parent: Vieux trucs
  key: Univers de Space Opera
  description: Un vieil univers prévu pour du JDR
  order: 1
---

# Univers de Space Opera

Un projet d'univers de *Space Opera* que j'avais, avec différentes idées en vrac que j'ai eut et exploré. 

Le but était d'en faire un JDR avec quelques inspirations diverses (dont *Star Wars*, *Star Trek*, *Farscape*, etc). Certains éléments de cet univers ont été repris dans d'autres, que ce soit dans Erratum ou d'autres. Quelques-unes de mes nouvelles se passent *plus ou moins* dans cet univers, ou en tout cas dans des éléments que j'avais constitués pour cet univers qui n'a jamais été complètement finalisé.

Beaucoup des éléments mis ici seront plus des pages de brainstorming plutôt que d'un univers complètement construit.

Parmi les noms prévus pour cet univers se trouvaient *Meteors Sonata*, *Bulles* (pour l'incarnation sur Terre, en Antarctique) et *Nouvelles-Frontières* (pour montrer que cette conquête spatiale était une variation tout aussi problématique du mythe de la frontière).

## Liste des pages

<ul>
  {%- for post in collections.all | sort(false, true, "data.eleventySubNavigation.order") -%}
    {%- if post.data.eleventySubNavigation.parent == eleventySubNavigation.key -%}
      <li><a href="{{ post.url }}">{{ post.data.eleventySubNavigation.key }}</a></li>
    {%- endif -%}
  {%- endfor -%}
</ul>

## Antartica / Bulles

Bulles devait se trouver dans un futur "pas très lointain", au début de la conquête spatiale, lors de l'invention des bulles. Les fameuses bulles étaient des bases hermétiques contenant des écosystèmes clos, inventées à partir de l'étude des réussites et échecs des projets Biosphère II, MELiSSA et BIOS-3. Et notamment l'action d'Antartica devait être dans des [bases créés en Antarctique](https://fr.wikipedia.org/wiki/Liste_de_bases_antarctiques), qui ont été une des terres d'expérimentations des bulles (avec la base Monado dans les îles Kerguelen, mais qui est une biosphère incomplète).

L'idée était de présenter un système solaire encore en construction, avec de nombreuses guerres civiles, et des difficultés liées aux manques de capacités de terraformation, et du coup les risques liés aux villes-bulles, et la peur du "Bus Factor" (le fait que si la terre tombait, tout le système ne serait pas autonome). Et le risque que posait le fait que cela devienne un empire fasciste, notamment avec des groupes comme l'Homo-Novus (qui est un gros néofasciste/néonazi qui prône un homme nouveau, mais revenant aux traditions) ou les autres volontés capitalistes et impérialistes, notamment lors de la découverte d'aliens, qui étaient utilisé comme nouveau "risque de danger" par les groupes comme l'Homo Novus.

Cela devait être aussi le début du cybernétismes, des implants, des modifications biologiques. Beaucoup de résistance, mais en Antarctique cela prend de l'ampleur. L'idée était d'avoir une vision critique du danger que pose le capitalisme dans une telle situation, tout en montrant comme alternative une grosse scène "Do It Yourself" et d'entraide/partage.

Il devait y avoir aussi des "IA domestiques" basé sur un système nommé le système Khimera, nommée les Chimères. Les Zoomorphes, sous une forme où ils étaient plus biomécanique devaient apparaitre lors de la construction solaire, et servaient à poser la question de la vie, ainsi que de notre traitement de ce qui est différent.

Une des dernière hésitation que j'avais eut était de reprendre plus "contemporain" toute la partie "bulles", et déplacer plus le reste vers un futur hypothétique.

## Space Opéra (Meteors Sonata)

L'idée était à ce moment de dériver vers du *Space Opera*, avec différents systèmes stellaires, tout en ayant encore de nombreux conflits internes dans l'humanité. Ça pouvait tout autant être une suite d'Antartica que non, et il y avait la présence des autres civilisations (avec quelques références, genre les [Xipéhuz](https://fr.wikipedia.org/wiki/Les_Xip%C3%A9huz))

## Noos et Noosphère

Un autre concept qui devait être présent était celui du Noos / de la Noosphère. C'était une sorte de grand réseau informatique émergeant qui reliait de nombreux esprits entre eux et permettait de communiquer, qui s'accédait avec divers moyens, aussi bien implants cybernétiques (fait notamment par les grosses corporations) que par des moyens plus "hackers" fait par la scène DiY.

Ce Noos était utilisé par un certain nombre de gros "jeux en ligne" fait par des corporations peu fiable qui devaient être une base du JDR, notamment avec l'idée de forcer du code pas prévu pour dans les différents jeux. La liste que j'avais prévue était :
- Un jeu style "jeu de vie" (Sims, Second Life) très permissif
- Un jeu de guerre spatiale
- Un jeu plus type JRPG coloré et médiéval
- Un jeu survie en territoire sauvage
- Un jeu très "cartoony" émergeant avec des aspects Garry's Mod mais MMO
- Un jeu de stratégie antique

À côté de cela se trouvaient pleins de micro-serveurs gérés par des plus petites équipes, loin des règles des grosses corporations. L'idée était que tous ces jeux étaient en fait scène à des conspirations, mais était aussi des lieux pour des personnes opprimées à se retrouver, s'organiser et tout (comme nos réseaux sociaux), et posait la question de nos choix sur ce genre d'aspects, et de comment combattre l'oppression.