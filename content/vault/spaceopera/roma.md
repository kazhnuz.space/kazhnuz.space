---
layout: layouts/subpages.njk
eleventySubNavigation:
  key: Système de Roma-Byzance
  parent: Univers de Space Opera
  order: 5
---

# Système de Roma-Byzance

Le système de Roma-Byzance est un système stellaire de trois étoiles, composé de deux sous-systèmes, celui de Roma et celui de Byzance. Le premier est un système double, avec une luminosité combinée de 2.3 soleils et un éloignement de 0,13 UA. Le système de Byzance est composé de la naine rouge Byzance (aussi nommée Byzas, du nom du fondateur légendaire de la ville, ou Constantinople quand on veut rappeler l’Empire Romain d’Orient), et ses cinq planètes sont nommées suivant les Empereurs de l’Empire Romain d’Orient/Empire Byzantin. Byzance (et son système planétaire) est en orbite autour de Roma, et est situé à 15 000 Unité Astronomique.

Les étoiles la plus proche de Roma sont Albe-La-Rouge, une Naine Rouge située à 3,28 année-lumière, L’Étoile-Des-Sabins, située à 4,30 année-lumière, Ravenne, située à 5,02 année-lumière, Carthago, situé à 5,23 année-lumière, et Byzance, située à 5,97 année-lumière. Elles ont toutes été nommée selon d’autres villes importantes (ou un peuple pour L’Étoile-Des-Sabins) ayant interagi avec Rome, les trois dernières ayant été nommé suivant d’autres capitales de l’Empire Romain.

Ce système stellaire a été conquis par un groupe de vaisseau lors du projet Dandelion, visant à ensemencer une partie de l’espèce humaine afin de la faire perdurer. Elle fait partie du secteur des « citée antique », qui contient plusieurs systèmes ensemencés. Les deux sous-systèmes sont nommés suivant l’Empire Romain, le premier suivant l’Empire Romain d’Occident et le second suivant celui d’Orient. Le système de Byzance a été ensemencé quelques dizaines d’années avant celui de Roma, et les deux sous-systèmes ont eu longtemps une histoire séparée.

## Données générales

Le système de Roma contient deux étoiles principales, deux naines jaunes nommées Romulus et Rémus, séparée de 0,13 UA.

Romulus est une étoile jaune, plus vieille, avec une masse de 1.1 masse solaire et une luminosité de 1.4 luminosité solaire. Rémus est la plus jeune des deux, et présente une luminosité de 0.9 masse solaire, et 0.6 6 luminosités solaires. Combinées, elles sont une masse de 2 masses solaires et une luminosité de 2.12 luminosités solaire. Cela a pour effet une zone habitable plus éloignée que pour le soleil, située entre 1.28 et 4.37 Unités Astronomiques.

Les deux étoiles sont entourées de 11 planètes (organisées en 10 orbites) et deux ceintures d’astéroïdes, ces premières étant nommée suivant les empereurs de l’empire Romain d’Occident. Leurs noms sont, dans l’ordre : Nero, Claudius, Septimius Severus, Caesar, Tiberius, Trajanus, Augustus, Constantinus, Marcus Aurelius et Julius Nepos. (Pour les planètes ayant des noms en deux parties, n’est en gras que la partie du nom couramment utilisée). Toutes les planètes orbites autour du système Romulus-Rémus, à l’exception de Nero, qui n’orbite qu’autour de Romulus.

### Créatures locales

Ce système solaire est le premier système solaire existant ayant montré les traces d’une ancienne civilisation qui avait presque atteint le type II sur l'échelle de Kardachev (c'est-à-dire que c'était une civilisation capable d'exploiter directement l'énergie de son étoile). On remonte son apogée à il y a plusieurs millions d’années (plus précisément 70 millions d’années), mais depuis elle a chuté, suite à une catastrophe inconnue liée sans doute à l'ignum lourd. Une grande partie de ses connaissances furent perdue, notamment comment utiliser les nombreuses structures spatiales pour récolter l'énergie de l'étoile. Il existe de nombreux entrepôts spatiaux cachés qui possède des vaisseaux et des technologies de ce peuple, qui ont été préservé par le vide de l'espace.

Cependant, si la civilisation s'est effondrée, tous les êtres qui la composaient n'ont pas disparu, et elle a en cela des descendants. Ces descendants étaient à différents stades d'avancement technologique à l'arrivée des humains, mais tous en dessous du type I (exploiter toute l'énergie accessible au niveau d'une planète). Ces civilisations allaient de la civilisation protohistorique à la civilisation industrielle.

La faune est composée de deux types de créatures : des créatures rocheuses/cristalline étant la faune originelle et des créatures organiques (importées par les terriens).


## 1 - Nero

La première planète du système de Roma se remarque par son extrême proximité de son étoile. En effet, Roma n’est situé qu’à 0.03 UA de son étoile, soit environs un dixième de l’orbite de Mercure ! Elle est tellement proche de son étoile qu’elle est en effet plus proche de Romulus que son étoile double Rémus ! Cela provoque une orbte très rapide, son année ne durant que l’équivalent de 1 jour terrestre et demi.

Cette planète possède également une orbite synchrone à cause des forces de marrée, ce qui fait que c’est toujours la même face qui est montré au soleil. Cette force de marée, provoque aussi un volcanisme massif sur la face éclairée, avec la présence même de lac de magma par endroit. Sa chaleur intense et le volcanisme sur sa face éclairée sont à la source de son nom, puisque cette planète a été nommée Nero en référence à l’incendie de Rome duquel cet empereur a été accusé.

Cette petite planète (d’une taille entre celle de Mercure et Pluton) n’a également pas d’atmosphère. Cela, combiné aux écarts de températures entre les deux faces de la planète, en fait également une des planètes non vivables, qui n’a jamais eut de civilisations, et qui la rendent difficile à utiliser et impossible à terraformer. Cependant, sa face gelée possède quelques usines de productions d’énergie entièrement automatisées construites par les humains. Une rumeur raconte que cette planète aurait été utilisé pour des condamnations à mort, notamment de celles des zoomorphes se rebellant (Ce qui est absurde, compte tenu du coût que cela aurait).

Aucun satellite n’orbite autour de la planète.

| Distance étoile | Durée de l’année |  Durée du jour | Rayon | Volume | Masse | Gravité en Surface |
|:---------------:|:----------------:|:--------------:|:-----:|:------:|:-----:|:------------------:|
| 0,027 U.A. | 1,54 Jour Terrestre | 1,54 Jour Terrestre | 0.286 Rayon Terrestres | 0,023 Volume Terrestre | 0,021 masse terrestre | 0,26 Gravité Terrestre |

## 2 – Claudius

Comme la planète qui la précède, la planète Claudius est une petite planète (elle est un peu plus petite que Mars) sans atmosphère, avec des températures très chaudes le jour et très froide la nuit, ce dût à la proximité du soleil et à son absence d’atmosphère (un peu comme sur Mercure). La théorie commune pour expliquer la disparition de son atmosphère est qu’elle était déjà très ténue à l’origine, et que les vents solaires a suffi à la souffler.

Il reste de l'ignum à la surface de la planète, puisque de nombreux cristaux et minerai contaminés par de l'ignum peuplent la croûte terrestre de la planète, on suppose qu’il s’agit de roche qui ont réagi à l’événement ayant provoqué la "corruption" de Septimius. Il ne reste plus de trace d'ignum lourd, la planète s’étant purifiée depuis 11 millions d’années. Quelques lacs d’azote d'ignum, une substance liquide ayant la propriété de rester liquide dans des circonstances extrêmes (et également une des formes liquides d'ignum les plus courante) parsème la surface.

Vue de loin, la planète apparaît comme grise parsemée du bleu ciel des formes cristallines et liquide d'ignum, ce qui lui donne l’apparence d’une sorte de Super-Mercure – pas très loin de la taille de Mars – parsemé d'ignum. Sa richesse en minéraux précieux et rare, en feront une parfaite planète minière et industrielles. Quelques créatures cristallines minérales peuplent la planète, les créatures cristallines étant les seules formes de vie pouvant survivre au vide spatial et aux énormes différences de températures de la planète. Il reste quelques traces d’une exploitation de la planète à l’époque de la civilisation Romulienne.

Claudius n’a pas de satellite naturel.

| Distance étoile | Durée de l’année |  Durée du jour | Rayon | Volume | Masse | Gravité en Surface |
|:---------------:|:----------------:|:--------------:|:-----:|:------:|:-----:|:------------------:|
| 0,491 U.A. | 88,98 jours terrestres (188,72 jours locaux) | 11,3 heures terrestres | 0,512 rayon terrestre | 0,134 volume terrestre | 0,145 masse terrestre | 0,55 gravité terrestre |

## 3 – Septimius Severus

Septimius Severus, souvent appelé Septimius est la troisième planète du système de Roma. Vue de l’extérieur, cette planète ayant un rayon près d'une fois et demi celui de la terre, et un volume et une masse près de quatre fois celle de notre planète ressemble à une sphère d’un noir d’encre, d’où son visible depuis l’espace des arcs électriques violets.

Cette apparence est due à sa haute atmosphère, composé en très grande quantité d’une matière corrompu, ce qui lui donne cette apparence extrêmement sombre, et surtout l’étrange couleur violette des éclairs qui la traverse. Cette haute atmosphère à un climat et une composition assez différente de la surface, bien qu’il y ait de l'ignum lourd dans les deux zones. En effet, la portion haute de l’atmosphère Septimienne est extrêmement dense, et très agitée, par des vents violents. La température peut y atteindre les 500 degrés, comme à la surface de Vénus. Cette haute atmosphère est opaque et empêche toute lumière d’atteindre la surface.

La surface de la planète est bien plus froide et possède une pression atmosphérique bien moins forte. Ce sont des paysages désolés qui s’y trouvent, balayés par des pluies violettes d’azote d'ignum lourd et de très fréquents orages, qui ont éradiqué toute forme de vie et érodé complètement la surface. Quelques lacs d'ignum corrompu pur forment d’immenses flaques violettes, mais la planète possède très peu de liquides à la surface. Les seuls types de structures datant d’avant la grandes corruptions sont ceux qui se trouvent profondément ensevelis sous terre. La seule structure située en surface est un immense monolithe, datant de l’époque ou des civilisations existait dessus. Il est impossible à dater.

Désormais totalement invivable, seules quelques espèces de vie minérale ayant évolué pour incorporer l'ignum dans leur structure – Septime était quand même habité jadis par une forme de vie dont on n’a aucune trace sur la planète même, mais qu’on suppose être la civilisation Romulienne. On estime que la totalité des espèces de la planète ont été éradiqués lors d’une catastrophe ayant engendré la forte présence d'ignum corrompu sur la planète, voir sur d’autres.

Septimius possède un unique Satellite, Pertinax, qui a un rayon d’approximativement 420 kilomètres.

| Distance étoile | Durée de l’année |  Durée du jour | Rayon | Volume | Masse | Gravité en Surface |
|:---------------:|:----------------:|:--------------:|:-----:|:------:|:-----:|:------------------:|
| 1.15 ua | 317,73 jours terrestres (230,67 jours locaux) | 33,05 heures terrestres. | 1,53 rayon terrestre | 3,58 volumes terrestres | 4,04 masses terrestres | 1,73 gravité terrestre |

## 4 – Le système planétaire de Caesar

Le système planétaire de César est le système planétaire double qui contient les deux planètes les plus proches de la Terre niveau condition de vie. Ce système est composé de deux planète, Julius et Crassus, ainsi que d’un satellite, Pompée.

Ces deux planètes sont habitable et habité, et ce sont généralement les planètes « capitales » du système de Roma. En effet, les températures de ces deux planètes sont assez proche de la Terre, ceci parce que la distance soleil-terre et la distance soleil-système de Caesar sont très proche.

Le satellite naturel du système de Caesar se démarque par sa couleur d’un rouge sombre.

| Distance Julius-Crassius | Durées mois | Autres lunes |
|:------------------------:|:-----------:|:------------:|
| 0,00764 UA (2,97 fois la distance Terre-Lune) | 100,69 Jours Terrestres | Pompée |

### 4.1 – Julius Caesar

Julius est nommé historique la « Jumelle de la Terre » par une partie des humains vivant sur cette planète. Cette planète tellurique située dans la zone habitable de Roma, possède des caractéristiques extrêmement proches de la planète bleue. En effet, outre un climat très proche de celui de la Terre, la taille de Julius n’est pas très loin de celle de la Terre.

De plus, la faune et la flore terrestre se sont très bien acclimaté à Julius. La ressemblance de Julius et la Terre est donc une coïncidence qui fut à la base de bien des idéologies d’empires, de religions, de guerres, de massacres, de chants, de légendes, de protections et de compassions. Cela a aussi fait de Julius la ville la plus importance du système de Roma, abritant notamment la capitale de l’union interplanétaire de Roma.

Cependant, il existe bien évidemment des espèces endémiques à Julius. Les espèces endémiques de Julius sont les créatures minérales. Deux espèces intelligentes non importées par l’homme existe sur cette planète, les Eirons et les Rekts. Ces espèces, descendants de l’ancienne civilisation Romulienne, vivent ensemble en des civilisations d’une technologie entre la renaissance et l’époque moderne, tout en ayant déjà découvert des technologies comme la radio en certains lieux. Cependant, ces civilisations sont très différentes les unes des autres. La plus avancée technologiquement parlant vit sous forme de clans semi-nomade, accompagné de groupe plus sédentaires, formant des castes différentes. Ses formes politiques diffèrent de la Terre en ayant des « nations » différentes pour différents corps de métiers, et n’ont pas véritablement de notion de « nation » lié aux cultures.

Un jour Julien dure 30,13 heures terrestres, ce qui fait qu’une année julienne dure 301,097 jours, la plupart des calendriers choisissant d’avoir un jour intercalaire environs tout les dix ans.

| Distance étoile | Durée de l’année |  Durée du jour | Rayon | Volume | Masse | Gravité en Surface |
|:---------------:|:----------------:|:--------------:|:-----:|:------:|:-----:|:------------------:|
| 1,44 unité astronomique | 447,30 jours terrestres / 474,57 jours locaux | 22,62 heures terrestres | 0,97 rayon terrestre | 0,91 volume terrestre | 0,88 masse terrestre | 0,94 gravité terrestre |

### 4.2 – Crassus

Crassus est la plus petite planète du système binaire de Caesar. La différence de masse entre Julius et Crassus fait que la durée d’une journée est égale à une orbite de Crassus autour de Julius (tout comme la lune, Crassus montre toujours la même face à Julius), ce qui donne une journée d’un tout petite peu plus d’une centaine de jours terrestres, ce qui fait des nuits et des jours extrêmement long.

La flore qui y vit est unique, assez proche de fougère d’un point de vue « forme » (mais elles sont bien une forme de vie minérale souple), et ayant une couleur assez unique, puisque ses fougères endémiques y sont de couleurs violettes. Il n’y a pas de plantes similaires à l’herbe sur Julius. Quant à la faune, elle, elle est très proche de la faune Julienne, mais du fait de la bien moindre présence des espèces intelligentes, la mégafaune y est largement plus présente. Ce qui fait que si d’un point de vue du climat, Crassus est plutôt hospitalière, disons que l’omniprésence de créatures pouvant bouffer des êtres humains la rend bien plus "sauvage" et dangereuse. On y trouve également des Eirons, qui vivent de manière plus sédentaire, dans des grandes forteresses pour se défendre des monstres.

Une très grande partie de la planète est recouvert par des plaines recouvertes de fougère, les forets ayant du mal à exister avec les grandes différences entre la journée et la nuit annuelle Crassienne. Si le jour est dangereux à cause de la forte présence de créatures potentiellement agressives et mortelles, la longue nuit de Crassius est glaciale et subit de nombreuses tempêtes de neiges, ce qui la rend également à redouter. Cette planète est très peu peuplée, et la planète servira pas mal à exiler des personnes gênantes ou ayant commis des crimes.

| Distance étoile | Durée de l’année |  Durée du jour | Rayon | Volume | Masse | Gravité en Surface |
|:---------------:|:----------------:|:--------------:|:-----:|:------:|:-----:|:------------------:|
| 1,44 unité astronomique | 447,30 jours terrestres / 4,44 jours locaux | 100,69 Jours Terrestres / 80,20 Jours Juliens | 0,59 rayon terrestre | 0,21 volume terrestre | 0,20 masse terrestre | 0,57 gravité terrestre |

## 5 – Tiberius

Tiberius est la dernière planète du système interne de Roma, et la seconde planète tellurique la plus massive avec un rayon proche de deux fois celui de la terre et une masse proche de 8 fois celle de la Terre.

C’est également une planète assez inhospitalière, à cause de sa gravité faisant plus de deux fois celle de la terre en surface, mais également à cause du climat, bien plus froid et aride que sur (L’eau ne couvre que 13% de la surface de la planète) Terre, Jullius ou Crassus. De plus, si la planète à un champ magnétique fort protégeant du vent solaire, elle ne filtre que mal les ultraviolets du soleil, ce qui participe également à rendre la vie difficile sur la planète. Même la zone tempérée de la planète est très sèche et les écarts de température y sont fort. Seule quelques forets tropicale située dans des vallées spécifiques offrent des climats plus cléments et régulier.

Cela fait que la faune et la flore Tibèrienne est assez différente de celles de la Terre et de Crassus, composé en grande partie de créatures assez reptilienne, se remarquant par de fortes carapaces avec peu de diversité générique, mais tout de mêmes différentes. La faune a en effet évoluée pour survivre dans le climat bien plus rude de la planète et un climat très homogène. Étant donné la forte concentration en glace d’eau du satellite naturel de Tiberius, il est supposé que la planète a perdue son eau d’une manière ou d’une autre.

On suppose que la planète est cependant le fruit d’une terraformation par la civilisation Romulienne, puisque son atmosphère présente des traces prouvant qu’elle était toxique à l’origine, trop dense et bien trop froide. Les traces indiqueraient une terraformation vieille de 70 millions d’années et ayant duré plusieurs millions d’annés, confirmant la théorie Romulienne. Les deux espèces vivantes intelligentes sur la planète n’en ont cependant aucun souvenir ni aucune archive (il n'y a sur cette planète que des rekt et des d’kraiens), la planète ayant eut de nombreux changements de civilisation.

La principale théorie est que cette terraformation serait liée à la catastrophe qui a provoqué la fin de la civilisation Romulienne, et la destruction de presque toutes formes de vie sur Septimius. Il y a 70 millions d’années, face à la catastrophe, des groupes auraient tenté de survivre. Un groupe se serait attaqué aux planètes du système de Caesar (plus vivable, mais plus proche si la catastrophe devenait interplanétaire), et un autre à Tiberius (moins vivable, mais plus loin et plus protégé de la catastrophe). La terraformation aurait alors durée plusieurs millions d’années, les espèces ayant été en stases pendant tout ce moment, subissant des reprogrammations génétiques pour pouvoir endurer le climat qui allait être le climat Tiberiens. La planète est donc vivable - mais inhospitalière - pour des terriens, julliens ou crassiens.

Habitable et habité, la plupart des grandes villes se concentrent sur les bords des zones maritimes, plus hospitalière.

Cette planète possède un satellite, Germanicus, qui était composé en grande partie de glace d’eau jadis, et qui ironiquement est devenu désormais un satellite entièrement recouvert par un océan unique (et des calottes glaciaires aux pôles), s’étant énormément réchauffé depuis. Sur Germanicus vive une sous-espèce d’eirons, les eions aquatique. Germanicus à un rayon de 0,34 rayon terrestre.

| Distance étoile | Durée de l’année |  Durée du jour | Rayon | Volume | Masse | Gravité en Surface |
|:---------------:|:----------------:|:--------------:|:-----:|:------:|:-----:|:------------------:|
| 2,25 UA | 874,15 jours terrestres / 659,94 jours locaux | 31,79 heures | 1,93 rayon terrestre | 7,19 volumes terrestres | 7,91 masses terrestres | 2,12 gravités terrestres |

## 6 – Ceinture du Vallum Aelium

Après ça se trouve la Vallum Aelium, première ceinture d’astéroïde. Ce nom provient du nom latin du mur d’Hadrien. Il s’agit de la ceinture d’astéroïde entourant ce qui est généralement considéré comme la zone « centrale » du système de Roma. Cette ceinture a été décrite par les partisans d’un Empire Stellaire de Roma comme étant la muraille de ce qui protégerait un « Roma Interne » qui serait le centre culturel de l’empire. C’est pour cela que la plupart des volontés de colonisations de ces astéroïdes a été soit pour en extraire des ressources naturelles, soit pour en faire une véritable ligne de défense des planètes les « plus importantes ». Il y a trois planètes naines dans cette ceinture d’Astéroïdes, nommé selon trois des empereurs de l’année des quatre empereurs : Galba, Othion et Vitellius.

### 6.1 – Galba

Galba est quasiment totalement déserte, sans atmosphère, à l’exception de quelques créatures minérales dangereuses, introduites par des colons pendant l’époque de la colonisation par les humains, et infestant les grottes et les anciennes mines de la planète. Elle est mise en quarantaines, parce que vue comme trop dangereuses. Il semblerait en plus qu’une partie de ces créatures aient atteintes des zones contenant de l'ignum lourd, parce que quelques formes « corrompues » de ces créatures ont été vues.

### 6.2 – Othion

La seconde de ces planètes, Othion, n’est pas totalement sphérique, ayant plus la forme d’un immense ovale. Anciennement désertique et sans atmosphère, a été entièrement mécanisé par des impérialistes, et est devenu leur forteresse, les résidents vivant entièrement à l’intérieur de la planète, qui est sous la coupe d’un empire unique, hyper-militarisé et discipliné. De nombreuses tours de contrôles disposant de missile à portée inter-astéroïdales y sont disposée, ce officiellement pour des mesures de protections. Cependant, l'Empire Othion reste un empire très discret, qui vie sa vie sans véritablement se disputer avec les autres civilisations.

### 6.3 – Vitellius

La dernière, Vitellius est devenue une planète colonisée, composé petites colonies de types rurale dans des dômes étanches, avec très peu d’habitant. Cette planète est une planète très rurale à l’intérieur de ces dômes de hautes technologies, avec un côté assez traditionaliste, mais avec de nombreux néoruraux venus s’y installer. Cette planète vit presque entièrement en autarcie, et à tendance à ne pas apprécier les nouveaux venus. Sa forme de gouvernement est une démocratie représentative proche de la démocratie directe, puisque dans les petites communautés tout le monde connait tout le monde.

## 7 – Trajan

Trajan est la première des planètes gazeuse, et la deuxième planète la plus massive de Roma (elle n'est pas très loin d’être deux fois moins volumineuse que Saturne tout en étant proche d’être une fois et demi plus lourde que cette même planète) et vue de l’extérieur, elle ressemble fortement à Jupiter niveau couleurs, quoique plus orangée. Cette planète jovienne (c’est-à-dire ayant une atmosphère pas très loin de celle de Jupiter) possède deux satellites massifs (et une vingtaine d’autres plus anecdotiques), nommés suivant des villes antiques du Moyen-Orient, en référence aux guerres parthiques de Trajan. Les deux plus importantes se nomment Babylone et Suse.

| Distance étoile | Durée de l’année | Rayon | Volume | Masse |
|:---------------:|:----------------:|:-----:|:------:|:-----:|
| 8,72 UA | 18,22 années terrestres | 7,15 rayons terrestres | 365,53 volumes terrestres | 142,56 masses terrestres |


### 7.1 – Babylone

Babylone est un satellite extrêmement froid de roches sèches, à l’atmosphère ténue. Babylone est un satellite d’une taille respectable (le plus gros de Trajan), mais pas très exploitable pour être habité, tout en étant moins intéressant que Latium sur ce plan-là, ce qui fait que la colonisation de Babylone n'est pas du tout vu comme une priorité. Il est par contre intéressant par la présence de minéraux utiles industriellement, ce qui fait que ce satellite contient surtout des bases industrielles, souvent automatisé et des bases de ravitaillement pour l’extérieur du système solaire de Roma.

### 7.2 – Suse

Suse, elle, est un satellite dont la surface aux couleurs ocres et rouges est balayé par des pluies d’acides, ayant une atmosphère ténue et irrespirable la réchauffant un peu – mais elle reste très froide. Il y existe un réseau de grottes artificielles et extrêmement protégée vivable servant de colonies. La surface, elle, est périodiquement attaqué par des créatures minérales, des sortes de créatures dangereuses considérées comme « démoniaque », s’attaquant généralement aux êtres humains et menant parfois des raids contre les bases. Cela provoque chez Suse une grande peur de l’extérieur et une superstition très forte.

### 7.3 – Zone vivable intra-trajane

Cette planète a la particularité d’avoir une zone découverte comme potentiellement vivable dans son atmosphère : La pression, la température y serait relativement proche de celle de la Terre et de Julius, cependant, son atmosphère y est toxique.

Cette zone a été colonisée à l’aide de nombreuses immenses colonies flottantes, reliées par des ponts servant à faire traverser un réseau de trains à vapeur (moins dangereuse que l’électricité dans une telle atmosphère), servant de moyen de locomotion principal, avec des bateaux volants et autres vaisseaux utilisant diverses méthodes pour flotter, ainsi que des ballons dirigeables. La plupart des intérieurs sont étanches et oxygénés, grâce à des pompes allant puiser dans des poches d’oxygènes situées plusieurs kilomètres en dessous des colonies. L’existence de ces poches d’oxygènes font que certains veulent qualifier cette planète de planète Trajane, se différenciant des planètes Joviennes, où la distinction serait justement la présence de ces poches d’oxygènes.

Les habitants sont en grande partie des zoomorphes aviaires, même si d’autres espèces y vivent et portent des masques à oxygènes à l’extérieur, à cause de la composition de l’air. Les technologies y sont des améliorations des technologies industrielles, en effet, électroniques et les technologies ont du mal à fonctionner dans un espace aussi magnétisé. Des rekts qui vivent aussi, sur des îles éparses, éloignées des humains.

## 8 – Augustus

Augustus est la seconde planète gazeuse du système de Roma, et la plus volumineuse et massive des planètes du système de Roma. Augustus possède quelques anneaux visibles. Sa couleur n’est pas uniforme, puisqu’il y a des bandes jaunes et quelques bandes un peu plus vertes, formant des volutes dans son atmosphère.

Son nom vient du premier empereur de l’Empire romain, Auguste. Grâce à son status de plus grande des géantes gazeuses, et sa position de géante gazeuse « centrale », cette planète est souvent considérée comme la plus importante des géantes gazeuse. Ceci fait que c’est la planète dont la colonisation des satellites a le plus été défini comme une priorité.

Augustus possède de très nombreux satellites, tous étant nommés avec des noms de régions de provinces de l’Empire Romain. Les satellites principaux sont basés sur celles de la province d’Italie, et sont nommés (dans l’ordre de proximité avec la planète) : Campania, Lucania, Calabria, Latium (qui est les plus gros satellites naturels de tout le système de Roma), Apulia, Umbria et Etruria.

Si la plupart de ces satellites sont des satellites de glaces assez classiques, quelques-uns se distinguent cependant.

| Distance étoile | Durée de l’année | Rayon | Volume | Masse |
|:---------------:|:----------------:|:-----:|:------:|:-----:|
| 17,71 UA | 52,71 années terrestres | 9,83 rayons terrestres | 949,87 volumes terrestre | 218,47 masses terrestres |

### 8.1 – Lucania et Calabria

Lucania et Calabria furent l’objet d’une expérience de terraformations, qui visait à créer des températures vivables sur des satellites situés loin de l’étoile centrale. Une seule des deux expériences fonctionna et créa une planète vivable.

Lucania réussi à créer une température et une pression proche de la Terre, mais cela causa un emballement toxique et corrosive. Dépourvue de la moindre forme de vie, et avec une atmosphère étrangement calme, ce est à l’origine de son surnom : « La Lune du Silence ».

Calabria, elle, fut mise sous surveillance constante par une station spatiale orbitant autour de la planète et contrôlant tout ce qui se passe, pour que cela ne se passe pas. Cependant, c’est cette fois l’actions d’individus qui menèrent une attaque sur la planète qui provoqua des problèmes. En effet, depuis cette attaque, il n’y a plus aucun contact avec la planète, qui semble être totalement coupé du reste du monde.

### 8.2 – Latium

Latium est le premier des satellites des géantes gazeuses à être colonisé, même si son atmosphère et ses températures bien trop froides fait que la vie ne peut se trouver que dans d’immenses villes-bulles proches de celles utilisées sur la Lune, s’étendant sur des kilomètres.

C’est pourquoi lorsqu’on atterrit sur la planète pendant l’époque de la colonisation du système de Roma, on voit une planète froide, pleines de cryovolcanisme, mais où d’immense villes métalliques y sont visibles, avec des dômes et d’immenses tours reposant sur un socle d’acier de plusieurs étages.

Conçues pour conserver la chaleur, ces bases en font un satellite industrialisé, et dont la plus grande base a fini par devenir une véritable capitale interplanétaire.

## 9 – Constantinus

D’un vert sombre émeraude, Constantinus est la huitième planète du système de Roma, et la dernière géante Gazeuse. Sa composition est jovienne, mais les quelques écarts de composition sont la cause de sa couleur particulière, qui est à l’origine du surnom de Constantinus comme « la plus belle planète de Roma », ou alors de « joyaux de Roma ». Constantinus n’a que de faibles anneaux peu marqués et une petite dizaine de satellites, dont seuls deux sont véritablement massifs. Les noms de ces satellites viennent de nombreuses créatures bénéfiques de la bible, notamment les archanges (ou des anges), les plus massifs se nommant Gabriel et Lucifer.

Sa beauté fait que de très nombreuses stations spatiales ont été construites dans le but d’être des stations balnéaires, ce pour des prix souvent assez haut. Rares sont ceux qui peuvent se permettre d’y avoir des résidences secondaires – ou alors ce sont des employés des stations, et ils vivent plus dans des sortes d’appartements cachés dans les zones des employés. Ces stations sont souvent fréquentées par les personnes riches et/ou importantes des autres planètes, et y sert également parfois de lieux de rencontre entre dirigeants, de grandes salles de réunions. Une des particularités du mode de fonctionnement de ces stations spatiales balnéaires font que techniquement, elles se sont régis par aucune forme étatique que ce soit, n’étant dirigé que par la direction de l’entreprise « Les Vues Constantines », qui possède ces stations. Des rumeurs font d’ailleurs souvent surface sur les mauvais traitements des employés, et sur divers trafics qui se produiraient dans certaines des stations balnéaires.

| Distance étoile | Durée de l’année | Rayon | Volume | Masse |
|:---------------:|:----------------:|:-----:|:------:|:-----:|
| 29,59 UA | 113,79 années terrestres | 5,23 rayons terrestres | 143,06 volumes terrestres | 38,63 masses terrestres |

### 9.1 – Gabriel

Le satellite Gabriel est à première vue un classique satellite rocheux sans atmosphère, et n’a pas eu de programme officiel de colonisations pendant les grandes périodes de colonisations du système stellaires.

Cependant, il a été colonisé par un vaisseau dirigé par une communauté de membres d’un culte, le Grand-Culte de Faustus, un syncrétisme du catholicisme romain, du manichéisme et du gnosticisme s’étant formé sur pendant le déplacement de la colonie

Et c’est la ou les versions diffèrent : Selon les membres du culte eux-mêmes, ils auraient trouvé sur cette planète à l’emplacement exact du pôle nord du satellite un lac d'ignum pur, et ils y auraient construit leur nouveau Grand-Temple, fasciné par ce miracle. Cependant, ce lac semble pour beaucoup de spécialistes artificiel… Mais est-ce vraiment les membres du Grand-Culte qui l’aurait construit ? Personne n’a la réponse, un tel chantier semblait bien trop demandant pour une organisation aussi petite à l’époque.

Ce satellite est tout de même devenu la terre sacrée de ce culte, et est devenu une théocratie, ayant alterné entre des phases rudes ou la morale religieuse y était extrêmement présente, et l’autorité du culte fort, et des phases plus ouvertes. De nombreuses bases hermétiques y ont été construit, et cette planète est devenue au fil du temps extrêmement peuplé, que ce soit par des migrations ou par des lois qui encourageait une forte natalité. D’un point de vue économique et vie quotidienne, on peut dire que ce satellite n’a pas vraiment de particularité autre que son statut de théocratie : En effet, elle est dans la moyenne du mode de vie des bases closes, avec des rations souvent basées de nourriture séché, peu de produit frais, et ce n’est pas une planète ou l’économie est spécialement fleurissante, mais elle n’a pas non plus de gros problèmes.

Cependant, une grande partie de la production des mines de matériaux précieux sert à construire de grandioses temples religieux – pour le plus grand bonheur des prêtres, mais surtout des touristes.

### 9.2 – Lucifer

Lucifer, lui, est un satellite à la couleur sombre, et à l’atmosphère épaisse. Lucifer est une planète replis d’un certains nombres de créatures qualifié de « Créatures Démoniaque ». Ces créatures minérales étant par l’évolution devenue de terribles prédateurs dont les propriétés électrogènes peuvent paralyser leur proie à distance.

Ce satellite est donc extrêmement dangereux et maintenu sous un blocus.

## 10 – Marcus Aurelius

L’avant-dernière planète du système de Roma est une planète tellurique gelée est également la plus massive planète tellurique du système de Roma. Vue de l’extérieur, c’est une planète qui semble surtout noire et blanche, dût à la composition de la glace présente en surface de la planète. Marcus Aurelius possède également la particularité d’avoir de grands anneaux, nommés les anneaux auréliens, ce qui en fait une des rares planètes telluriques à en posséder. Une des particularités de la planète est son climat : Si c’est en effet une géante de glace, sa surface possède comme le satellite Titan une grande présence d’hydrocarbure, qui fonctionnent comme le cycle de l’eau sur terre, s’évaporant, retombant en pluie et formant divers lacs à la surface de la planète, ce qui fait que le système météorologique n’est pas sans ressembler à celui de la terre – mais en bien plus froids. En plus de ce cycle d’hydrocarbure, un océan liquide existe sous l’épaisse couche de glace, dont les courants internes dus à la chaleur interne de la planète et à la pression sont parfois la cause de formation de véritables volcans de glaces – des cryovolcans. Une des particularités de cette planète est la stabilité de ce climat : En effet, Marcus Aurelius aurait le même climat depuis quasiment 2 milliards d’années. Il y a très peu de variation de température et cette planète ne semble pas avoir de climats. Son atmosphère est irrespirable, étant composé en majeure partie d’azote et de méthane. De plus, sa rotation est très lente, une journée Aurélienne étant plus longue qu’une année terrestre.

Marcus Aurélius est également connu pour ses « Forêts de Glace », de grandes structures de formes de vies minérales non-mobiles (que l’on compare souvent à des plantes) et qui ont colonisé une très grande partie de la surface. Ces formes de vies minérales ont l’apparence d’immenses champignons de glaces qui auraient des centaines de petits cristaux de glaces suspendus sous leur chapeau. Cette planète est également habitée par des formes de vies minérales capables de se mouvoir, dont les parties inertes sont d’ailleurs souvent composées de glaces. Parmi ces créatures, la plus caractéristique sont les "Géants de Glace", une sous espèce de d’kraiens, visiblement des descendants de membres de la civilisation Romulienne ayant été fortement modifiés pour vivre sur cette planète. Ces créatures à l’espérance de vie très longue (ils peuvent vivre jusqu’à plusieurs milliers d’années !) existe depuis très longtemps et a créé des formes de société souvent difficile à comprendre pour les créatures intelligentes organiques. Leur mode de communication principal est par un langage complexe composé de signes, les Géants de Glace ne pouvant pas parler de manière orale.

Cette planète ne possède pas de satellite.

| Distance étoile | Durée de l’année | Rayon | Volume | Masse |
|:---------------:|:----------------:|:-----:|:------:|:-----:|
| 65,13 UA | 371,60 années terrestres | 3,57 rayons terrestres | 45,54 volumes terrestres | 59,20 masses terrestres |

## 11 – Julius Nepos

Julius Nepos, souvent nommé Nepos, est la dernière planète du système Solaire. Elle est considérée souvent comme une petite planète, bien qu’étant plus massive que Nero, Claudius et Crassius. Cependant, elle est souvent considérée comme une planète « ridicule » parce qu’« après les géantes gazeuses qui étaient nommés comme certains des plus grands empereurs de Rome, la dernière planète du Système Solaire fait office d’une figure pitoyable, et presque inutile. », Julius Nepos étant un des derniers empereurs de Rome, son nom fut donné à la planète. Cependant, cette planète se démarque parce qu’au lieu d’être composé en grande partie de silice, elle est composée en grande partie de carbone. L’effet que cela fait et la présence potentielle de diamants – et d’une atmosphère composée en grande partie de monoxyde et dioxyde de carbone, ce qui la rend toxique en plus du froid extrême de cette planète et de la pression trop faible de son atmosphère.

Cela ne l’empêchera pas d’être source de moult convoitises de chercheurs de diamant, croyant y voir une potentielle source d’une fortune facile. Une très grande partie de ces prospecteurs finiront ruiné à vivre péniblement dans les quelques bases vivables de la planète, ou succomberont aux dangers de cette planète, tels que les nombreux terrains instables et fragiles.

Quelques traces indiquent une exploitation de la planète, il y a plusieurs millions d’années.

| Distance étoile | Durée de l’année | Rayon | Volume | Masse |
|:---------------:|:----------------:|:-----:|:------:|:-----:|
| 101,42 UA | 722,16 années terrestres | 0,50 rayon terrestre | 0,13 volume terrestre | 0,12 masse terrestre |

## 12 – Le limes

Le Limes est la dernière ceinture d’astéroïde et englobe entièrement le système stellaire. Cette ceinture d’astéroïde est considéré comme la frontière extérieure de « l’Empire de Roma ». Étrangement, on y trouve peu de planète naine et la planète de Nepos est trop éloignée de cette ceinture d’astéroïde pour en faire partie.
