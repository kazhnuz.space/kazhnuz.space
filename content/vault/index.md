---
layout: layouts/switchpages.njk
eleventySubNavigation:
  parent: Mes projets
  key: Vieux trucs
  description: Mes anciens projets
  order: 1
---

# Vieux trucs

Bienvenue dans mon petit grenier où je mets tous mes vieux trucs !

Cette page est une petite page pour organiser un peu tout mon bazar et rendre plus simple à lire les vieux trucs que je voudrais poster. Vous y trouverez à la fois des vieux projets, des vieux concepts d'histoire, et d'autres petits trucs que j'ai fait pour le fun.

Tous ces contenus sont utilisables sous les termes CC BY-SA. (Creative Common Attribution - Partage à l'Identique). Certains de ces trucs reprennent cependant des éléments privateurs (venant de licences de JV, par exemple) en tant que fanfictions. Les aspects opensources ne sont que ceux que j'ai inventés. Tout n'est pas forcément utilisable (certains vieux trucs j'ai dû utiliser une vieille VM avec un GM8 cracké pour pouvoir les retester), mais peut-être que quelques trucs ici vous seront utiles :3

## Ce que vous pourrez trouver ici

- Des vieux concepts de projets
- Peut-être du vieux code
- Des vieux textes et constructions d'univers que j'ai mis de côté.
- En vrai pas grand-chose d'intéressant, c'est surtout pour garder mes vieux trucs quelque part si je les reprends.

## Pages dans cette section

<ul>
  {%- for post in collections.all | sort(false, true, "data.eleventySubNavigation.order") -%}
    {%- if post.data.eleventySubNavigation.parent == eleventySubNavigation.key -%}
      <li><a href="{{ post.url }}">{{ post.data.eleventySubNavigation.key }}</a> - {{ post.data.eleventySubNavigation.description }}</li>
    {%- endif -%}
  {%- endfor -%}
</ul>

<div class="align-center">

![Des cochons dans une ambiance glitché qui regarde l'écran](/img/articles/vault/incoming.gif)

</div>