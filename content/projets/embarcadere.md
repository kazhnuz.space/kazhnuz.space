---
layout: layouts/subpages.njk
eleventySubNavigation:
  parent: Mes projets
  key: Embarcadère
  description: Un petit portail de site
  order: 1
---


# Embarcadère

[Embarcadère](https://git.kobold.cafe/kazhnuz/embarcadere/) est le code de [Kobold Start](https://start.kobold.cafe/), un petit portail que j'ai créé pour Kobold Cafe (qui me sert de page d'accueil sur mes navigateur web).

{% image "../../public/img/articles/projets/koboldstart.webp", "Une capture d'écran d'Embarcadère, montrant un site avec un fond typé falaise montrant des liens dans des boutons et d'autres sous formes de plusieurs listes. Un petit kobold dans une tasse est en bas à droite" %}

Son principe est d'être un simple "embarcadère" vers les différents sites auquel j'ai besoin d'accéder. Elle ne contient pas de fonctionnalités fancy comme afficher l'heure (mon OS fait ça), la météo (mon OS fait ça) ou lire des flux RSS (j'ai un lecteur de flux pour ça).

Le dépôt contient le code tel qu'il est utilisé pour Kobold Cafe, et donc des éléments associés, mais vous pouvez le modifier pour créer votre propre portail.

## Fonctionnalités

- Une barre de recherche vers DuckDuckGo
- Affichez une liste de site qui vous intéresse, aisément modifiable via le fichier `_data/config.json`
- Différenciation entre des liens mis en avant et par catégorie + un lien "plus de site".
- Un style simple et neutre, mais où vous pouvez
  - Modifier la couleur d'accent dans la configuration
  - Modifier le fond et le favicon
  - Afficher une mascotte pour plus de fun !
- Utilisation d'émoji afin de rajouter un peu de vie à la liste des sites

## Customisation

Le site peut être customisé par trois moyens principaux : modifier l'image de fond, le favicon modifier la mascotte (et son texte/alt) et la couleur d'accents.

- Le fond peut être modifié en modifiant /public/img/background.jpg
- La couleur d'accent peut être modifié en modifiant le contenu de `accentColor` dans le fichier `_data/config.json`
- La mascotte peut être modifiée en modifiant `/public/img/mascotte.png`
  - Son alt-text se trouve dans le fichier `_data/config.json` dans `mascotte.alt`
  - Une description supplémentaire peut être trouvée dans le fichier `_data/config.json` dans `mascotte.message` et s'affichera au survol de la mascotte
- Le favicon peut être modifié en modifiant `/public/favicon.png`

## Modifié les sites

Les sites sont inclus dans les variables `featuredSites` ou dans des blocs dans la variable `siteBlocks`. Pour modifier les sites "mis en avant" (les boutons colorés), il vous suffit de modifier les sites qui sont géré de la manière suivante.

```json
  "featuredSites":[
    {"nom":"<nom qui sera affiché sur le bouton>", "url":"<url du site>", "emoji":"<Un emoji à utiliser>"},
  ]
```

Pour modifier les sites dans les blocs, vous pouvez le faire de la manière suivante

```json
  "siteBlocks":[
    {
      "nom":"<nom du block>",
      "sites":[
        {"nom":"<nom qui sera affiché sur le lien>", "url":"<url du site>", "emoji":"<Un emoji à utiliser>"},
      ]
    },
  ]
```

Un lien supplémentaire `moreLink` peut également être modifié

```json
  "moreLink": {
    "nom": "Plus de liens sur mon shaarli !",
    "url": "https://shaarli.kazhnuz.space",
    "emoji": "🔗"
  },
```

## Tester & compiler

Pour tester le site, il vous faut npm et npx sur votre installation, et utiliser la commande

```
npx @11ty/eleventy --serve
```

Vous pouvez build le site avec

```
npm build
```

## Crédits

- Fond d'écran venant d'Elementary OS, fait par Ashim DSilva
- La mascotte de Kobold Café par [Withelias](https://withelias.kobold.cafe/)