---
layout: layouts/subpages.njk
eleventySubNavigation:
  parent: Mes projets
  key: Boutons 88x31
  description: Mes petits boutons en 88x31
  order: 2
---

# Boutons 88x31

J'aime beaucoup les boutons au format 88x31, je trouve que ce sont des boutons très funs et sympa à mettre en place dans un site et tout. Voici du coup ici tout ceux que j'ai créé, ceux que j'aime et qui me représentent auront leur propre page plus tard.

## Mes sites

J'ai fait pas mal de petits boutons pour mes sites ! Le premier, et pas des moindres, est tout simplement celui de ce blog ! La police d'écriture utilisée est boxy, et le kazhnuz en pixel art a été fait par Withelias. Il existe en deux version, une version ou je dépasse du cadre, et une qui respecte plus strictement le format.

![Un bouton avec un kobold à oreille de chèvre en pixel-art, gris et violet avec un manteau noir. Le kobold dépasse du cadre](/img/buttons/sites/kspace.png) ![Un bouton avec un kobold à oreille de chèvre en pixel-art, gris et violet avec un manteau noir. Le kobold reste bien sagement dans le cadre](/img/buttons/sites/kspace-small.png)

### Kobold Café

Ensuite, ceux pour Kobold.cafe ! Kobold.cafe est le site qui héberge tout ce que je fais pour moi ou mes potos. Il possède deux boutons, basé sur ses deux thèmes. Il y a trois variante : la variante "café", la variante "jardin", et une café avec un petit Kazhnuz dessus.

![Un fond de bar en style RPG 16-bit, avec écrit par dessus "Kobold Cafe"](/img/buttons/sites/koboldcafe.gif) ![Un fond de bar en style RPG 16-bit, avec écrit par dessus "Kobold Cafe", avec aussi un petit Kazhnuz disant "bjr"](/img/buttons/sites/koboldcafekazh.gif)  ![Un fond naturel en style RPG 16-bit, avec écrit par dessus "Kobold Cafe"](/img/buttons/sites/koboldcafe-garden.gif)

Le dernier est une variation de l'ancien bouton de fanstuff-garden, domaine que j'ai re-fusionné récemment avec kobold.cafe.

![Un fond naturel en style RPG 16-bit, avec écrit par dessus "Fanstuff Garden"](/img/buttons/sites/fanstuffgarden.gif)

### Mes autres sites

Mais j'ai d'autres sites ! Voici en vrac trois boutons pour trois sites hébergé par Kobold Café : Quarante-Douze, Sonic Garden et Missing Number.

![Un bouton avec un fond rouge pixelisé avec écrit dessus "Dimension Quarante-Douze"](/img/buttons/sites/qdouze.gif) ![Un bouton avec un fond de ville en bleu, avec un missing-no et un fossile de Pokémon Rouge](/img/buttons/sites/missingnumber.png) ![Un bouton avec Sonic courrant devant une ville, avec écrit "Sonic Garden"](/img/buttons/sites/sonicgarden.png)


## Boutons anti-ia

J'ai fais quelques variantes du petit bouton "No Ai!" que j'ai sur ma page d'accueil, qui servent à indiqué la non-utilisation de l'IA dans son site web. Une variante "kobold" est faite spécialement pour mes sites (et les potos kobold), et une variante human plus "générique".

![Bouton web avec écrit "No Ai! Human Made"](/img/buttons/noai/noai-kobold.gif) ![Bouton web avec écrit "No Ai! Human Made"](/img/buttons/noai/noai.gif)

J'ai aussi fait quelques variantes avec des noms de divers animaux pour tout les potos furry/therians/etc ^^ Le but était de faire le message tout en faisant des boutons qui soient utilisablent par celleux qui préfèrent autre chose que "human" écrit dessus.

![Bouton web avec écrit "No Ai! Cat Made"](/img/buttons/noai/noai-cat.gif) ![Bouton web avec écrit "No Ai! Fox Made"](/img/buttons/noai/noai-fox.gif) ![Bouton web avec écrit "No Ai! Dog Made"](/img/buttons/noai/noai-dog.gif) ![Bouton web avec écrit "No Ai! Wolf Made"](/img/buttons/noai/noai-wolf.gif) ![Bouton web avec écrit "No Ai! Dragon Made"](/img/buttons/noai/noai-derg.gif) ![Bouton web avec écrit "No Ai! Animal Made"](/img/buttons/noai/noai-animal.gif)

## Boutons prides

Pleins de boutons LGBT ! J'ai d'abord créé les fonds, puis j'ai fait un programme pour générer pleins de variante textuelles.

### Version avec textes

{% for prideflag in pridebuttons %}
    {{ prideflag.title }}

    {% for button in prideflag.list %}<img src="/img/buttons/pride/{{ button.file }}" alt="{{ button.localizedAlt.fr }}" /> {% endfor %}
{% endfor %}