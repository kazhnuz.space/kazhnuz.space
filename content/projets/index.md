---
layout: layouts/sommaire.njk
eleventyNavigation:
  key: Mes projets
  order: 2
---

# Mes projets

Une petite page où je liste les différents projets sur lesquels je participe ou travaille 😀 Cela peut servir un peu à vous donner un ordre d’idée de trucs que je fais ou qui sont en cours, et moi ça me sert à flex un peu */PAF. Plus sérieusement, j’avais envie d’une page ou je liste un peu des projets qui sont importants pour moi et que j’aime bien ^^

Cette page remplace de manière plus simple l’ancienne page d’accueil que j’avais sur Kobold City (qui sera remplacée par un projet plus ambitieux)

## Sites webs

La majorité de ces sites sont hébergé sur [Kobold Cafe](https://kobold.cafe), à l'exception de Woltar et Planète-Sonic.

- *Kazhnuz Space* : Ce site 🙂 Mon site personnel, un peu de tout suivant mes humeurs
- [Quarante-Douze](https://quarante-douze.net) - Mon site/blogs de technologie et politique
- [Fanstuff Garden](https://fanstuff.garden/) - Ensemble de petit fansites et espaces de partages (Contient [Sonic Garden](https://sonic.fanstuff.garden/), [Press Garden](https://press.fanstuff.garden/) et le site Pokémon [Missing Number](https://missing-number.fanstuff.garden/))
-  [Portefolio de Withelias](https://withelias.kobold.cafe/)  et [Le site de Distant-Flare](https://distantflare.kobold.cafe/) : Développement du theme wordpress (design fait par Seren_Moonflower), hosting. N’hésitez pas à aller voir, Withe est un super artiste 🙂
-  Je participe aussi aux sites [Planète-Sonic](https://planete-sonic.com/) (tech, rédacteur, invité de podcast et de stream) et [Woltar](https://woltar.net/) (co-développeur).

## Écriture et JDR

- [Erratum](https://erratum.kazhnuz.space) - Univers de JDR et d’histoires sur lesquels je bosse depuis un moment. Vous pouvez trouvez les histoires écrites dans cet univers dans le tag suivant.

## Développement et code

- [romme.php](https://git.kobold.cafe/kazhnuz/romme.php) (2018) est un petit code pour convertir une date dans le [systeme Romme du calendrier révolutionnaire](https://fr.wikipedia.org/wiki/Calendrier_r%C3%A9publicain#Projet_de_r%C3%A9forme_pr%C3%A9par%C3%A9_par_Romme)
- [Épervier](https://git.kobold.cafe/epervier/epervier-old) (2019) est un petit framework de jeu pour love2D
- [Roleplay](https://git.kobold.cafe/jdr-et-univers/roleplay.css) (2019) est un theme bootstrap inspiré RPG
- [Blue Sky](https://git.kobold.cafe/kazhnuz/bluesky-bootstrap-theme) (2019) est un theme bootstrap. Rien à voir avec le site.
- [Embarcadère](embarcadere/) (2024) est un portail de site utilisant eleventy.
- [Moineau](https://moineau.kazhnuz.space) (2024) est une feuille de style minimaliste pour du HTML sémantique.

## Divers

- [Boutons 88x31](/projets/88x31/) - Tout les boutons au format 88x31 que j'ai fait pour ce site ou d'autres.
  - [Pride buttons](/projets/88x31/#boutons-prides) (2025) est un set de boutons 88x31 basé sur les différents drapeaux de la pride.

## Projets futurs

- **Forum** : Un espace communautaire français un peu "retrotech", fandom-oriented et orienté NA/LGBT, étant aussi antiraciste et anti-oppression. Il se rajouterait à Fanstuff Garden et Kobold Cafe pour former la galaxie de petits services que je fournis aux gens pour s'aider.
- **Kobold City** : Site RPG-esque de fantasy ou vous jouez des kobolds dans un monde dirigé jadis par les dragons. Un mélange entre un clone du site de mon enfance « woltar » et un « RPG de gestion » avec une intrigue qui sera construite sur le long terme. J’y fait le dev, le game design et le web design. Aide de Withelias pour les graphismes.
- **Imperium Porcorum** : RPG tactique au tour par tour, avec une inspiration de Pokémon. Vous y dirigez un "maître du mal" qui veut conquérir le monde avec des animaux de fermes.
- **Sonic Radiance** : Un fan-RPG Sonic
- **Goetian Racing** : Un jeu hybride de course/shooter se passant en enfer, avec des changements de gameplay suivant les niveaux (projet de jeu cours). On y joue une âme damné tentant de sortir des enfers via son véhicule.
- **Aestrus** : Univers de light SF anthro. Site en cours de création.
  - *Quatorze* : Un mini-roman de science-fiction ou un adolescent élevé pour être le « clone mental » d’un ancien empereur apprend à avoir une identité et des buts dans la vie.
- **Ailleurs** : Série de romans courts ou deux adolescentes vont découvrir un gamin capable de voyager entre les mondes et des y entrainer. L’idée est de présenter le multivers sous un angle nouveau, celui du voyage dans des histoires différentes. C’est une reprise d’un vieux projets que j’avais eut pour un inktober, mais l’idée ne convenait pas trop au format que j’avais, on passait de monde en monde trop vite. Forte inspiration des *Conquérants de l'Impossible* de Philippe Ébly.

## Vieux projets

Vous pouvez accéder à mes vieux projets abandonnés sur [cette page](/vault)