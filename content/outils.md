---
layout: layouts/sommaire.njk
eleventyNavigation:
  key: Outils et guides
  order: 4
---

# Outils et guides

Voici une petite liste de quelques guides et ressources que j'ai postées sur mes différents sites web.

## Alternatives libres

- [Des moteurs de recherches alternatifs](https://quarante-douze.net/quelques-moteurs-de-recherches-respecteux-de-votre-vie-privee)
- [Les interfaces alternatives pour accéder aux silos numériques](https://quarante-douze.net/les-interfaces-alternatives-pour-acceder-aux-silos-numeriques)
- [Guide de passage sous Linux](https://quarante-douze.net/tag/guide-linux) (en cours)
- **Hors de Twitter** (informations sur comment quitter twitter et les gros réseaux "GAFAMs") 
  - [1 – Les réseaux alternatifs](https://quarante-douze.net/decentraliser-son-web) 
  - [2 – Ode aux sites personnels](https://quarante-douze.net/hors-de-twitter-2-ode-aux-sites-personnels)

## Listes de contenus

- [Liste d'applications et d'extensions pour GNOME](https://github.com/Kazhnuz/awesome-gnome)
- [Liste de fan-créations Sonic](https://sonic.fanstuff.garden/fan-area/)
- [Ressources utiles pour l’enseignement](https://quarante-douze.net/resources-utiles-pour-lenseignement)
- [Liens et guides pour le dev web](https://quarante-douze.net/liens-et-guides-pour-le-dev-web)
- [Liens et guides sur l’accessibilité](https://quarante-douze.net/liens-et-guides-sur-laccessibilite)

## Tuto et divers

- [Les jeux flash et JavaME en 2023](https://quarante-douze.net/les-jeux-flash-et-javame-en-2023)
- [Jouer aux jeux Sonic](https://sonic.fanstuff.garden/discover/games/)