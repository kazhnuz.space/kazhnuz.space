---
layout: layouts/subpages.njk
permalink: /now/
eleventySubNavigation:
  parent: À propos
  key: /now
  description: Ce que je fais en ce moment
  order: 1
---

# En ce moment…

Voici une petite page où je raconte ce que je fais dans différents domaines.

## Personnel

- 📵 J'essaie d'avoir une moins grande addiction à mon smartphone.
- 🥗 J'essaie de manger plus équilibré.
- 🛌 J'essaie de gagner un meilleur équilibre entre travail et détente.

## Développement et création

- 🧑‍💻 Je développe une nouvelle version *d'épervier*, mon framework de jeu (généraliste 2D, mais avec des trucs utiles pour RPG).
- 🧑‍💻 Je développe une première version de *mouette*, mon framework CSS.
- 🧑‍💻 Je maintiens les sites du webcomic [Distant-Flare](https://distantflare.kobold.cafe) (lisez-le il est cool), de son artiste [Withelias](https://withelias.kobold.cafe) et participe à la maintenance de [Planète-Sonic](https://planete-sonic.com) et [Woltar](https://woltar.com).
- ✏️ J'écris (difficilement) mon roman *Une petite histoire de démon*.
- ✏️ J'écris sur mes blogs et sur Planète-Sonic.

## Musique

- 💿 J'écoute *Hyperthoughness* de *Fear and Loathing in Las Vegas* (gros gros banger).
- 💿 J'écoute la bande son de *Sonic Frontiers* et les musiques du *Concert Symphonique des 30 ans de Sonic*.
- 💿 J'écoute la bande son de *Promare*.
- 💿 J'écoute comme toujours la bande son de *Homestuck*.

##  Séries, films et livres

- 📚 Je lis la série des *Royaumes de Feu* (Wings of Fire en anglais).
- 📚 Je lis la série des *Traquemort* (Deathstalker en anglais).
- 📺 Je regarde la série d'animation *Arcane*.
- 📺 Je regarde la série d'animation *Helluva Boss*.
- 🗨️ Je lis le webcomic [Slightly Damned](https://www.sdamned.com/).
- 🗨️ Je lis le webcomic [Tamberlane](https://www.tamberlanecomic.com/).
- 🗨️ Je lis le webcomic [Homestuck : Beyond Canon](https://beyondecanon.com/).
- 🗨️ Je lis évidemment le webcomic Distant-Flare mis plus haut :D.
- 🗨️ Je lis la BD de détournement *L'Intégrale de l'Effondrement*.

## Jeu vidéo

- 🎮 J'ai joué à *Sonic x Shadow Generations*.
- 🎮 J'ai joué à *Here comes Niko*.
- 🎮 Je joue à *Pseudoregalia*.
- 🎮 Je joue à *Valheim* avec des amis.
- 🎮 J'ai commencé *Terraria*.
