---
layout: layouts/old.njk
oldVersion:
  key: Design Solarizé
  yearStart: 2019
  img: kspace2019.webp
  alt: "Un design de blog solarizé, avec une esthétique affiche. La couleur d'accent est rose, et les titres utilisé des biseau. Le design est en deux colonne, la principale à le contenu et des preview en sorte de boite, et la sidebar à les catégories et trucs du genre. Les couleurs ont moins de contraste que celui du site présent. Le header à un ciel intégré, comme celui-là"
  engine: Hexo puis Wordpress
---

La première version du design qui deviendra celui-présent. À l'époque, il était bien moins lisible je trouve. Par contre j'avais jamais remarqué qu'à l'époque, je changeais de design quasiment tous les ans wow.