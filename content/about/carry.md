---
layout: layouts/subpages.njk
permalink: /carry/
eleventySubNavigation:
  parent: À propos
  key: /carry
  description: Ce que je porte sur moi
  order: 1
---

# Ce que j'ai sur moi

J'ai souvent quelques trucs pour moi, et voici la page pour décrire ce que j'ai souvent dans mon sac

## Sur moi

J'ai un petit sac qui contient sur moi le plus important. J'ai aussi quelques trucs qui vont 

- Mon téléphone, un smartphone Nokia 7.1
- Un petit portefeuille avec toutes mes cartes et tout.
- Un mini portefeuille gay-pride qui contient ma carte de bus/tram et me sert de porte-clef.
- Un baladeur MP3 tout con, sans écran tactile (mais avec un petit écran quand même), qui a l'avantage de tenir longtemps avec sa batterie
- Mes lunettes solaire.
- Mon casque, qui est un casque à fil, mais dont le fil n'est PAS intégré, pour pouvoir le remplacer facilement.

## Dans mon sac à dos

Je me suis également pris un sac à dos pour pouvoir transporter plus d'affaire quand je suis en trajet ou que j'ai besoin de trucs comme mon ordinateur. Dedans, j'y inclus des éléments comme :

- Une liseuse - j'aime bien avoir ma liseuse sur moi, cela me permet de pouvoir me poser et lire tranquillou avec ma musique
- Une console Ambernic RG353P - Un bon moyen d'avoir accès à pleins de petits jeux, en plus elle est en plastique noir transparent
- Une trousse avec un crayon bille à cartouche, des crayons de bois et des feutres de couleurs
- Un *bullet journal* et un carnet
- Mon laptop
- Une bouteille d'eau
- Des sachets de thé à infusion froide quand je veux être fancy