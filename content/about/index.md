---
layout: layouts/about.njk
eleventyNavigation:
  key: À propos
  order: 0
---

# À propos

Hello et bienvenue sur mon blog personnel ! Le but de cette page est de présenter un peu plus en détail qui je suis, ce que je fais, et quel est ce blog.

Je suis Kazhnuz, une sorte de bestiole queer qui fait du dev pour pouvoir nourrir ses chats, et le reste du temps des trucs divers et variés pour le fun ! J’aime particulièrement écrire, le webdesign et différents trucs qui y sont lié. J’aime bien m’essayer à des trucs nouveaux, tester des choses, avoir une obsession et faire qu’un seul truc pendant trois semaines puis pas en refaire pendant 10 mois… Et parfois je poste les résultats ici. Je suis également politiquement engagé, je suis un sale gauchiste. Désolé, ce site n’est pas un espace « apolitique » (déjà parce que ce n’est pas possible).


<div class="d-flex-sm">
  <div style="width:100%;">
    <div class="card mr-half c-success">
        <h2 class="card-header">J'aime</h2>
        <ul>
          <li>Écrire et créer des trucs</li>
          <li>Le webdesign et le design</li>
          <li>Les vieux jeux et techno retro</li>
          <li>La philosophie et l'éthique</li>
          <li>Me balader dans des parcs</li>
          <li>Les animaux et trucs mignons</li>
          <li>L'esthétique glitch et creepy</li>
          <li>Les trucs colorés</li>
        </ul>
    </div>
  </div>
  <div style="width:100%;">
    <div class="card ml-half c-danger">
        <h2 class="card-header">J'aime pas</h2>
        <ul>
          <li>Les situations de conflit</li>
          <li>Bloquer sur une idée</li>
          <li>Quand y'a trop de gens</li>
          <li>Être seul trop longtemps</li>
          <li>Être face à du vide</li>
          <li>L'eau trop profonde</li>
          <li>Ne pas pouvoir aider</li>
          <li>Les chips natures</li>
        </ul>
    </div>
  </div>
</div>

Je suis féru de ce qu’on appelle le web indépendant et suis contre l’unification d’une grande partie de notre vie numérique dans des services controlées par des multinationales de plus en plus puissantes. Je défends du coup les mouvements du logiciel libre, de l’indé, de la création amateur, et du web décentralisé.

Ce site est un peu le résultat de cela : c’est mon jardin personnel, mon espace sur lequel je peux un peu m’éclater et faire en sorte que tout ce que j’y poste m’appartiennent toujours. Il remplace à la fois mon ancien compte deviantART et une partie de ce que je faisais sur mes comptes de réseaux sociaux (ou dans des forums) sous forme de thread.

J’ai pour but de faire pas mal d’autres petits sites suivant mes intérêts et envie, qui seront accessible depuis le haut de ce site. J’aimerais beaucoup réussir à relancer des forums aussi.

(J’aime bien les kobolds et autres lézards aussi)

## Anciennes versions

<div class="preview-grid">
  {%- for post in collections.all | sort(false, true, "data.oldVersion.yearStart") -%}
    {%- if post.data.oldVersion -%}
      <article class="card card-preview head-primary">
          <a href="{{ post.url }}" class="preview-link">
            <h1 class="card-header"> {{ post.data.oldVersion.key }} </h1>
            <div class="preview-content">
              <div class="preview-exerpt" aria-hidden="true"><p class="p-img">
                {%- set thumb = "../../public/img/articles/old/" + post.data.oldVersion.img -%}
                {%- thumbnail thumb, "" -%}
              </p></div>
              <div class="preview-overlay">
                <div class="preview-metadata">
                  <div class="metadata-pills">
                    <div><span class='badge c-info small-text'>Ancienne version</span></div>
                    <div><time><span class="badge c-info small-text">{{ post.data.oldVersion.yearStart }}</span></time></div>
                  </div>
                  <div class="comment-text">
                    {{ post.data.oldVersion.engine }}
                  </div>
                </div>
              </div>
            </div>
          </a>
      </article>
    {%- endif -%}
  {%- endfor -%}
</div>

## Pages slashes

Des [pages slashes](https://slashpages.net/#uses) sont des petites pages descriptives souvent utilisées dans le web indépendant pour se décrire ou donner plus d'information sur soi. J'en ai quelques-uns que je trouve fun et les vois.

<ul>
  {%- for post in collections.all | sort(false, true, "data.eleventySubNavigation.order") -%}
    {%- if post.data.eleventySubNavigation.parent == eleventyNavigation.key -%}
      <li><a href="{{ post.url }}">{{ post.data.eleventySubNavigation.key }}</a> - {{ post.data.eleventySubNavigation.description }}</li>
    {%- endif -%}
  {%- endfor -%}
</ul>

## Crédits

- Site propulsé par [Eleventy](11ty.org)
- Mon avatar et sa version pixel art ont été imaginé et réalisé par [Withelias](https://withelias.kobold.cafe/).
- Ce site utilise une version modifiée de la palette [solarized](https://ethanschoonover.com/solarized/)