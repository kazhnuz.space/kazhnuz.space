---
layout: layouts/subpages.njk
permalink: /todo/
eleventySubNavigation:
  parent: À propos
  key: /todo
  description: Ma todolist
  order: 1
---

# Todolist

Voici ma liste de "trucs que je vais faire" :3 Les projets ne seront pas forcément faits dans un ordre précis, mais c'est les trucs que je suis en train de faire en ce moment. Certains des plus important sont dans ma page [/now](/now/). Je n'y met pas tout mes projets et trucs à venir, juste ce qui me semble qui va arriver un de ces quatre.

## Sites actuels

- Ajouter le support des webmentions et autres protocoles IndieWeb à blog.kazhnuz.space et quarante-douze.
- Réactiver des commentaires aussi sur ces blogs.
- Ajouter dans la vault d'autres de mes vieux projets.
- Ajouter dans Missing Number des infos sur les vieilles creepypasta et tout.

## Développement et création

- Nouvelle version de souklemanteau, mon vieux site de partage de créations disponibles sur internet.
- Brocéliande, un "fansite" d'univers magique, permettant d'en découvrir (notamment des alternatives à une certaine saga littéraire). Peut-être que j'en présenterais certains avant sur mon blog.
- Créer un site et une page de JDR pour Radiant Skie, mon univers de light-sf.
- Tux Café, une collection de petits jeux casual libre, pour tester mon moteur *Épervier*.

## Séries, films et livres

- 📚 Je compte lire le recueil de nouvelle des Utopiales 2024.
- 📚 Je compte lire *C'était mieux demain*, un recueil de nouvelle Solarpunk.
- 📚 Je compte lire les recueils de nouvelles Utopien⋅nes.
- 📚 Je compte lire la série des Chrestomanci.
- 📺 Je compte regarder *Jentry Chau vs the Underworld*.
- 📺 Je compte regarder les nouvelles saisons de la série *Le Prince Dragon*.

## Jeux vidéos

- 🎮 Je compte jouer à *The Stanley Parable*.
- 🎮 Je compte jouer à *Sable*.
- 🎮 Je compte jouer à *Arzette : The Jewel of Faramore*.
- 🎮 Je compte jouer à *Pyschonauts 2*.
- 🎮 Je compte jouer à *Digimon Survive*.
- 🎮 Je compte jouer à *Digimon Story Cyber Sleuth : Complete Edition*.
- 🎮 Je compte jouer à *Cassete Beast*.
- 🎮 Je compte jouer à *Coromon*.
- 🎮 Je compte jouer à *Dungeons of Eather*.
- 🎮 Je compte jouer à *Owlboy*.
- 🎮 Je compte jouer à pleins de vieux point and click.