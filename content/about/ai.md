---
layout: layouts/sommaire.njk
permalink: /ai/
---

# Ce site n'utilise pas l'IA

Ce site n'utilise l'IA d'aucune manière que ce soit. Ni pour les recherches (j'utilise des moteurs de recherche à l'ancienne), ni pour la rédaction qui est fait à l'ancienne, avec mon style foireux.

![Bouton web avec écrit "No Ai! Kobold Made"](/img/buttons/noai/noai-kobold.gif)

Cela vaut pour tous les types de créations (code, articles, textes narratifs, réalisations graphiques). Cela vaut également pour mes autres sites et les autres sites host par [kobold.cafe](https://kobold.cafe/).

## Pourquoi ?

Je pense que les blogs générés par IA sont un fort problème pour internet, polluant les résultats de moteurs de recherches et produisant du spam qui a des effets néfastes sur le réseau dans son ensemble. De plus, leur consommation énergétique fait que je ne peux pas me dire que je vais l'utiliser pour mon blog, même si cela améliorait la qualité (ce que je ne pense pas, encore plus dans le cas d'un blog PERSONNEL). Ce blog vise à contenir mes opinions, mes points de vue et mes propres erreurs.

Pour reprendre la vieille citation « *If you can't be bothered to write it, why should I read it ?* ». Je préfère que mon blog et mes sites [soient d'un internet vivant, humain, que d'un internet automatisé mort-vivant](https://quarante-douze.net/l-internet-vivant-et-l-internet-mort).

## Boutons

J'ai fais quelques variantes du petit bouton "No Ai!" que j'ai sur ma page d'accueil et plus haut.

![Bouton web avec écrit "No Ai! Human Made"](/img/buttons/noai/noai.gif)

Dont quelques variantes avec des noms de divers animaux pour tout les potos furry/therians/etc ^^

![Bouton web avec écrit "No Ai! Cat Made"](/img/buttons/noai/noai-cat.gif) ![Bouton web avec écrit "No Ai! Fox Made"](/img/buttons/noai/noai-fox.gif) ![Bouton web avec écrit "No Ai! Dog Made"](/img/buttons/noai/noai-dog.gif) ![Bouton web avec écrit "No Ai! Wolf Made"](/img/buttons/noai/noai-wolf.gif) ![Bouton web avec écrit "No Ai! Dragon Made"](/img/buttons/noai/noai-derg.gif) ![Bouton web avec écrit "No Ai! Animal Made"](/img/buttons/noai/noai-animal.gif)

Pas besoin de créditer le bouton, c'est juste un truc viteuf !

## Resources

- [A.I is BS](https://www.youtube.com/watch?v=ro130m-f_yk)
- [Niveau des émissions énergétique de l'IA](https://www.theguardian.com/technology/article/2024/jul/02/google-ai-emissions)
- [SlashAI.page](https://slashai.page/)