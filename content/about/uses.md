---
layout: layouts/subpages.njk
permalink: /uses/
eleventySubNavigation:
  parent: À propos
  key: /uses
  description: Ce que j'utilise au quotidien
  order: 1
---

# Ce que j'utilise au quotidien

Voici une petite page pour décrire les logiciels que j'utilise au quotidien. Une seconde page existe pour les objets que j'ai sur moi au quotidien.

## Ordinateurs

J'utilise principalement (hors taff) deux ordinateurs : une tour et un laptop.

Voici les spécifications de ma tour. Je l'ai construite il y a 3~4 ans je crois, pour me faire un cadeau après mes premières années de taff.

- Processeur : AMD Ryzen 7 3700X
- Carte graphique : AMD Radeon™ RX 5700 XT
- RAM : 32 Go
- Un disque dur de 8.5 To (contient ma partition /home)
- Un SSD de 500 Go (contient mon système)
- OS: Ubuntu sous la dernière version
- Interface : GNOME Vanilla avec comme extensions Caféine et GSConnect (pour relier à mon téléphone portable)

C'est un bon PC qui fait bien son taff et qui marche bien sous Linux, et c'est ce que je lui demande. Avec je peux faire du gaming, travailler sur mes projets, écrire, et le garder encore un moment.

Mon Laptop est un Lenovo Ideapad 3

- Processeur : AMD Ryzen 5 3500U
- Puce graphique : AMD Radeon Vega 8
- RAM : 8 Go
- Un disque dur de 1 To (contient ma partition /home)
- Un SSD de 250 Go (contient mon système)
- OS: Fedora Silverblue
- Interface : GNOME Vanilla avec comme extensions Caféine et GSConnect (pour relier à mon téléphone portable)

C'est mon PC que j'utilise quand je vais faire du JDR avec des potes, ou quand je suis en voyage. Malheureusement, il commence à avoir des soucis de reboot intempestif, je compte tenter une réinstallation pour voir si ça résout le souci.

## Applications

Voici les applications que j'utilise au quotidien et que je trouve cool. J'ai une préférence pour les applications GNOME, dont j'aime la simplicité et la philosophie du "do one thing and do it well".

J'ai aussi une préférence pour utiliser autant que possible des applications natives quand je peux au lieu d'utiliser des sites webs.

Voici les applications que j'utilise pour la navigation / connections :

- Navigateur web : Firefox avec pas mal d'extensions (article à venir sur quarante-douze)
- Mail : Thunderbird (j'attends avec impatience un plus minimal pour GNOME XD)
- Lecteur de flux RSS : [Newsflash](https://apps.gnome.org/fr/NewsFlash/)
- Mastodon & Fedivers : [Tuba](https://tuba.geopjr.dev/)
- Messagerie instantanée : Discord (personne n'est parfait), [Fractal](https://flathub.org/apps/org.gnome.Fractal) pour Matrix, [Dino](https://flathub.org/apps/im.dino.Dino) pour XMPP. J'hésite à utiliser Signal également, mais j'ai peu de gens autour de moi qui l'utilise.

Sinon, voici quelques autres applis que j'utilise au quotidien :

- Écriture : [Apostrophe](https://apps.gnome.org/fr/Apostrophe/) est un éditeur de fichier markdown que j'utilise pour écrire mes articles de blogs. Pour les projets de romans, j'ai plus tendance à utiliser LibreOffice.
- Mot de Passe / MFA : [Secrets](https://apps.gnome.org/fr/Secrets/) est mon gestionnaire de mot de passe et [Authenticator](https://apps.gnome.org/fr/Authenticator/) mon gestionnaire d'authentification à deux facteurs.
- Musique : [Gapless](https://flathub.org/apps/com.github.neithern.g4music)
- Développement : Eclipse (pour Woltar & projet kobold) et [VSCodium](https://flathub.org/apps/com.vscodium.codium) (pour le reste)

## Web

Voici les serveurs que j'utilise et les moteurs utilisés par différents de mes sites :

- Un serveur sous Ubuntu comporte tous les sites statiques (sous Eleventy) + mes deux blogs persos (qui sont sous Bludit)
- Un serveur sous Yunohost comporte le reste de mes services et les blogs encore sous wordpress

Sous Yunohost, j'utilise les services suivants :

- Cloud : Nextcloud
- RSS : Freshrss
- Read-It-Later : Wallabag
- Forge git : Forgejo
- CI : Drone (sera remplacé peut-être par Forgejo Actions)
- Fedivers : Gotosocial