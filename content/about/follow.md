---
layout: layouts/sommaire.njk
permalink: /follow/
---

# Mes liens

Voici divers liens pour me contacter, réseaux sociaux ou retrouver ce que je fais. D'autres seront rajouté au fur et à mesures, peut-être, sans doute, qui sait. Certains sont en communs avec d'autres pages, le but étant de regrouper un peu tout ici !

## Réseaux sociaux

Je suis présent sur les réseaux sociaux :

<ul>
{%- for link in social -%}
    <li>{{ link.emoji }} <a href="https://{{link.url}}" rel="me">{{ link.name }}</a> - {{ link.title }}</li>
{%- endfor -%}
</ul>

## Mes sites

<ul>
{%- for link in sites -%}
    <li>{{ link.emoji }} <a href="https://{{link.url}}" rel="me">{{ link.name }}</a> - {{ link.title }}</li>
{%- endfor -%}
</ul>

## Flux RSS

Vous pouvez me suivre via les flux RSS suivants :

<ul>
{%- for link in rss -%}
    <li>{{ link.emoji }} <a href="https://{{link.url}}">{{ link.name }}</a> - {{ link.title }}</li>
{%- endfor -%}
</ul>