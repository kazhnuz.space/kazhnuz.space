---
layout: layouts/toppages.njk
eleventyNavigation:
  key: Fandoms et shrines
  order: 3
---

# Fandoms et shrines

Je suis depuis des années dans pas mal de fandoms différents. Sur cette page, je vais un peu présenter quelques séries que j’aime bien, pourquoi je les aime bien, et d’autres petits anecdotes dessus. J'ai pour but de transformer certaines de ces sections en "shrines", des petites pages dédiées avec leur propre design à terme.

## Mes shrines

Voici les petits shrines que j'ai fait pour des séries :

<ul>
  {%- for post in collections.all | sort(false, true, "data.eleventySubNavigation.order") -%}
    {%- if post.data.eleventySubNavigation.parent == eleventyNavigation.key -%}
      <li><a href="{{ post.url }}">{{ post.data.eleventySubNavigation.key }}</a> - {{ post.data.eleventySubNavigation.description }}</li>
    {%- endif -%}
  {%- endfor -%}
</ul>

## Percy Jackson

Une série que j’ai découverte bien plus tard que pas mal de monde ! En effet, je l’ai découvert en cherchant à lire quelque chose après avoir été déçu de ce que devenait une série de romans de mon enfance, à la fois en terme créatif et surtout en termes de comportement de son autrice (vous avez deviné de qui je parle, je pense mdr), et j’ai décidé de tenter de voir ce que donnait cette série qu’on me conseillait justement en « truc à lire si on aime bien les ados face à des univers magiques ».

Et j’ai énormément apprécié cette série. J’aime tout particulièrement ses enjeux, son humour, et son utilisation de la mythologie pour construire un monde cohérent, et ses réflexions sur certains aspects de la mythologie. La série est cool. Mon livre préféré est l’un des plus récemment sortit : Le Soleil et l’Étoile, centré sur Nico, sa relation avec son copain et sa relation avec lui-même.

J’ai aussi bien aimé les autres séries mythologies se passant dans le même univers, et je commence à m’intéresser pas mal à la collection des « Riordan Presents », une collection d’autres séries basés sur les mêmes concepts de fusionner mythologie et monde contemporain.

## Doctor Who

Une série que j’ai découverte à la fac, et dont je n’ai plus décroché depuis ! J’ai toujours aimé particulièrement son univers (encore une fois très riche, c’est souvent ce qui me plait dans une série), et son ambiance pouvant êtes parfois burlesques, épiques, etc. Le traitement du personnage du docteur, toujours à mis chemin entre « une divinité » et « juste un⋅e idiot⋅e » est très cool, et participe beaucoup à rendre le personnage sympa.

J’ai beaucoup apprécié les ères Russel T. Davies et Moffat, celle Chibnail j’ai adoré l’actrice qui jouait le Docteur (notamment dans ses moments lumineux, elle apportait une énergie très cool au Docteur) mais j’ai trouvé certaines intrigues assez mal amenées (mais j’ai rien contre le concept du Timeless Child, en vrai).

La nouvelle ère RTD est cool aussi, et j’ai été tout particulièrement content de le voir reprendre et faire avoir des conséquences à ce qui se passait avant (notamment le Flux et le Timeless Child). Je trouve que c’est plus intéressant que de juste oublier des trucs qui ont autant d’impact. J’ai surtout vu pour l’instant les séries, j’ai pas encore commencé les audiobooks Big Finish, et j’ai lu qu’une des nombreuses compilations de romans.

## Le Disque Monde

Une de mes séries de romans préférées (en même temps vous vous en doutez sinon ce ne serait pas ici). J’ai découvert cette série au lycée, quand je cherchais des moyens d’occuper mes recréations et heures de permanences : je les ai passé à lire Pratchett. J’aime particulièrement la manière dont il fait une série principalement humoristique, mais y insère de vraies intrigues et des personnages qui ont des enjeux.

Mes deux sous-séries préférées y sont celles que beaucoup adorent je pense : celle du Guet (notamment pour le personnage de Carotte et celui d’Angua) – ce qui est paradoxal vu mes opinions politiques – et celle des sorcières.