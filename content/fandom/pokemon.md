---
layout: layouts/shrine.njk
eleventySubNavigation:
  key: Pokémon
  parent: Fandoms et shrines
  description: La série avec pleins de bestioles qui se balancent des patates.
  order: 2
customcss: pokemon
title: Pokémon
---

<section>

La série qui a en grande partie lancé mon amour des RPG et JRPG. J'ai découvert la série avec Pokémon Rouge et Bleu, et je l'avais un peu lâchée à l'époque de Soleil et Lune (j'avais aussi loupé la troisième génération à l'époque, mais je l'ai rattrapée depuis).

J’aime la série pour la diversité des créatures qu’on y trouve et leurs caractères haut en couleur. Pour moi, le fun de Pokémon vient vraiment de constituer son équipe et la faire évoluer face aux challenges présents dans le monde (ce qui fait que j’adore les challenges pokémon). Je suis moins fan de toute la dimension stratégique, même si elle est très intéressantes. Ce que j'aime aussi, c'est que c'est un de mes jeux pour chill. Si je fais parfois des défis genre Nuzlocks et tout, c'est aussi un des jeux auquel j'aime jouer quand j'attends que mes draps se lavent à la laverie, quand je suis dans le train, etc.

J'ai créé un petit site pokémon "Missing Number" qui se concentré sur les "secrets" autour des deux premières générations.

**Crédits de la page** : 

- Fond et bordure venant de pokémon RFVF upscale via l'algorithme xBR
- Images d'évolition en train de dormir venant de Pokémon Sleep récupéré sur Bulbapedia
- Images de fond du titre étant un fond d'écran de Pokémon Home récupéré sur Bulbapedia

</section>

<div class="align-center mb-1">

<a class="btn btn-primary" href="https://missing-number.fanstuff.garden/">Missing Number</a>

</div>

<section>

## Mes jeux préférés

J'ai joué à pas mal des titres de la série pokémon, et mes jeux préférés sont : Pokémon Or/Argent, Pokémon Rouge/Bleu, Pokémon Blanc/Noir et Pokémon Legend Arceus (qui a la boucle de gameplay la plus cool de la série). J'aime aussi beaucoup Rubis/Saphir/Émeraude et Diamant/Perle/Platine, mais j'ai moins de "nostalgie" pour, comme je n'y ai pas joué à l'époque, donc y'a moins la petite vibe émotionnelle. Mais ça reste je trouve des titres excellents !

Je n’ai pas joué aux jeux depuis Soleil et Lune à l’exception d’Arceus pour l’instant, mais j’ai vu un peu du scénario d’Écarlate/Violet et j’ai bien aimé ce que j’ai vu. J'ai joué un peu au premiers Donjons Mystère que j'avais bien aimé, un jour il faudra que je fasse le légendaire Explorateur du Ciel, parce qu'on m'en a toujours dit énormément de bien.

</section>

<div class="align-center mb-1">

<img src="/img/shrines/pokemon/eevee.png" alt="" class="shrine-illu" />

</div>

<section>

## Mes pokémons préférés

Comme beaucoup de personnes, j'ai un gros faible pour les évolitions (comme le montre subtilement cette page), mais en fait y'a pas mal d'autres pokémon pour lesquels j'ai beaucoup d'affections !

Là, en pokémon qui me viennent de tête (en plus des évolitions citées plus haut) que j'aime beaucoup il y a Fouinard, Elecsprint, Junko, Bulbizard, Lucario (oui), Goupix & Feunard, Dracofeu. En légendaire, j'ai tendance a préféré ceux "simple" qui ont moins une dimension cosmique, notamment les trois oiseaux légendaire, les trois bêtes légendaires ou les Swords of Justice de B/N.

J'ai tendance à préférer les designs plus "animalesque" que les autres, notamment les pokémon quadripèdes. J'ai aussi un gros faible pour les lézards et les dragons.

</section>

<div class="align-center mb-1">

<img src="/img/shrines/pokemon/umbreon.png" alt="" class="shrine-illu" />

</div>

<section>

## L'univers émergent

Un des points que j'aime le plus dans la série, c'est qu'il y à un univers énorme que j'appellerais "l'univers émergeant". Cet univers émergeant, c'est toute la partie de pokémon qui existent dû aux interprétations, à des éléments de la fanbase, ou à des trucs qui n'étaient pas prévu par les créateurs. La série Pokémon dépasse le simple contenu "prévu", des jeux, et je trouve ça assez cool.

Dedans on retrouve :

- Tout ce qui est né des "glitch" et mécaniques imprévues, qui est étudié en détail par les fans. Ça, c'est quelque chose qu'on retrouve surtout dans Rouge et Bleu, et c'est super fun comment par exemple les glitch (genre MissingNo.) ont participé à créer des éléments dans l'univers collectif.
- Tout ce qui est "rumeurs", "creepypasta", etc. Si certaines sont un peu cringe et edgydark avec le recul (par exemple j'ai du mal avec celles qui parlent de l'unité 731 maintenant que j'ai grandis, parce que bah c'est des vrai crimes de guerres), j'aime bien tout cet univers de cartouche hanté et de trucs trop sombre pour le jeu. Certaines de mes préférées sont les classiques comme Pokémon Black (à ne pas confondre avec le jeu), Lost Silver ou Abandon Lonliness. Mais sinon j'aime bien aussi l'univers des rumeurs genre Pokégods, jardins de Léo, etc.
- Et un dernier que j'aime tout particulièrement est la quantité de défis et de façons de jours alternatives qui ont été trouvées dans Pokémon. Les gens ont beaucoup d'imagination pour se mettre des limitations et c'est cool !

Je parle un peu de tous ces aspects dans mon site Missing Number /pub éhonté.

</section>

<div class="align-center mb-1">

<img src="/img/shrines/pokemon/volteon.png" alt="" class="shrine-illu" />

</div>

<section>

## Les hackroms

Voici quelques hackroms que j'aime beaucoup et que je recommande :3 Attentions, ce sont surtout des hacks 2G, parce que je me suis pas encore penché sur l'univers des hacks 3G/4G, mais ça ne saurait tarder !

- La série des **Pokémon Legacy** qui contiennent des améliorations fidèles de Pokémon Jaune et Crystal. L'idée est de rester au plus proche de ce qui était prévu dans le jeu, tout en améliorant le jeu grâce au pouvoir d'avoir du recul dessus.
- Si vous préférez une modernisation de l'expérience, je conseille : **Pokémon Red++** pour la G1, **Pokémon Polished Crystal** pour la G2, **Pokémon Delta Emeraude** pour la G3 et **Pokémon Mega Blue** pour les remakes de la G1.
- Dans les jeux qui modifient tout le jeu, j'ai surtout joué à **Pokémon Bronze**, **Bronze 2**, **Pokémon Orange** (la version basée sur Crystal) et **Pokémon Blanc et Noir 3 Genesis**. Y'en a pleins aussi pour la 3G que je vais devoir tenter !
- Des remakes des versions bétas de Or et Argent existent avec **Pokémon Super 97** et **Pokémon 97 Reforged**
- Et en vrac : **Pokémon Red Unova et Kalos Crystal** changent les pokémon des G1 et G2 par ceux des G5 et G6, **Pokémon Black** est la creepypasta véritablement jouable et **Pokémon Kanto Expansion Pack** et une extension de la G1 !

Vous pourrez en trouver plus sur Missin- okay on a compris.

</section>

<div class="align-center mb-1">

<img src="/img/shrines/pokemon/flareon.png" alt="" class="shrine-illu" />

</div>

<section>

## Sites sympa

Voici quelques liens que je trouve sympa ou intéressant si vous êtes fans de Pokémon !

- [Blue Moon Fall](https://bluemoonfalls.com/) est un fansite qui parle des Gen 1 et 2 de pokémon (une de mes inspirations pour mon site missing-number), mais centré sur les conseils pour y jouer et aller plus loin dessus.
- [Buried relic](https://buriedrelic.neocities.org/) est un fansite qui propose des guides et outil pour le jeu normal et le shiny hunting dans Pokémon
- [Poképrint](https://pokeprint.kimbachu.com/site/) est un fansite qui propose des ressources pokémon à imprimer.
- [Pamtree Berry](https://pamtre-berry.neocities.org/) est un fansite qui recréer les mini-jeux des ères GBA-DS
- Même s'il est plus connu, je ne peux pas ne pas mettre [Bulbapedia](https://bulbapedia.bulbagarden.net/wiki/Main_Page) qui est un wiki pokémon *indépendant de fandom.com*.
- Page The Cutting Room Floor pour [Pokémon](https://tcrf.net/Category:Pok%C3%A9mon_series) qui contient des TONNES d'informations, c'est incroyable.

</section>

<div class="align-center mb-1">

<img src="/img/shrines/pokemon/sylveon.png" alt="" class="shrine-illu" />

</div>
