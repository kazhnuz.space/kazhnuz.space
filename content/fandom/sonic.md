---
layout: layouts/shrine.njk
eleventySubNavigation:
  key: Sonic the Hedgehog
  parent: Fandoms et shrines
  description: Juste un hérisson bleu qui court un peu vite.
  order: 1
customcss: sonic
title: Sonic the Hedgehog
---

<section>

Ma série de jeux vidéo préféré, et un univers que j’affectionne tout particulièrement. 

J’aime la série pour son gameplay dynamique, mais aussi pour son côté éclectique et diversifié : dans Sonic on va avoir à la suite des jeux humoristiques, sérieux, dramatique, etc. l’univers mélange aussi de nombreuses inspirations, pouvant être coloré, orienté science-fiction comme fantasy, etc. Ce qui fait la force de la série est, je pense, qu’elle part de base simple (un hérisson bleu qui court vite et hait l’oppression contre un savant voulant devenir dictateur mondial), et itère à travers de nombreux concepts, et a créé avec le temps un cast haut en couleur, dynamique et juste fun en fait.

C'est complètement série B (voir Z parfois), ça pars dans tout les sens, ça mélange du cartoon, de l'anime… bref je kiffe.

Je suis rédacteur sur le site planète-sonic, et j'ai dump pleins de vieilles pensées sur le site Sonic Garden.

**Crédits de la page** : 

- Images des perso de Sonic par le Sonic Channel, récupéré sur Sonic Wiki Zone
- Images de fond du titre venant de Sonic adventure

</section>

<div class="align-center mb-1">

<a class="btn btn-primary" href="https://planete-sonic.com/">Planète-Sonic</a> <a class="btn btn-primary" href="https://sonic.fanstuff.garden/">Sonic Garden</a>

</div>

<section>

## Mes personnages préférés

La série Sonic est connue pour ces personnages hauts en couleurs qui ont été, à une époque, pas mal qualifiés de "shitty friends" par des gens qui n'aimaient pas qu'il y ait pleins de personnages dans une série ou le design appelle à faire pleins de personnage.

J'ai une affection particulière pour certains de ces personnages :

- **Silver the Hedgehog**, parce qu'il est le best boy. J'aime bien son côté un peu plus "innocent" que les autres personnages, ayant moins une maitrise des codes sociaux, tout en pouvant avoir un côté "intense" dans les moments. J'aime bien son interprétation dans IDWSonic ou il peut passer du "cinnamon roll" ultime à un perso super classe.
- **Shadow the Hedgehog**, parce que j'adore les perso edgy */PAN. Plus sérieusement, j'aime bien son design, une version plus "edgy" et "sombre" de Sonic, sans aller dans le "too much", et qui garde un aspect iconique.
- **Miles Tails Prower**, parce que j'aime bien le fait d'avoir un personnage (un peu) moins combattant, et qu'il créer pleins de machine. Je fais partie de celleux qui aiment bien son gameplay dans SA2, et le perso est choupi.
- **Blaze the Cat**, parce qu'elle a des pouvoirs de feu et qu'elle est cool. Si son tempérament à des points communs avec Silver, Shadow et Knuckles, elle a un côté royal qui est cool, et une histoire qui ajoute un aspect "pression sociale" intéressant. Et puis elle crame tout donc c'est cool.
- **Surge the Tenrec**, personnage exclusif à IDWSonic, que j'aime particulièrement parce qu'elle à une énergie de délinquante qui est fun, et que son histoire montre qu'elle est plus complexe que juste une ennemie de Sonic. Elle a des désirs et envies contradictoires, et ça la travaille et lui pose soucis. 

</section>

<div class="align-center mb-1">

<img src="/img/shrines/sonic/sonic-illu-2.webp" alt="" class="shrine-illu" />

</div>

<section>


## Mes jeux préférés

Je n'ai pas totalement de préférence entre les trois grandes ères de Sonic (Classique, Adventure, Boost), mais quelques jeux Sonic sortent du lot en tant que mes préférés :

- **Sonic Frontiers**, dont j'adore le gameplay openzone et le côté "bordélique, mais giga créatif".
- **Sonic Adventure 1 et 2**, jeux de mon enfance sur lesquels j'ai passé tant de temps ! J'aime leur système des upgrades, et surtout les CHAO.
- **Sonic 3&K**, qui est pour moi le meilleur jeu Classique, parce que j'adore la manière dont on traverse Angel Island pour aller sauver le monde.
- **Sonic Generations**, qui est l'un des jeux Sonic les plus fun à rejouer et dont j'adore refaire des niveaux de temps en temps. Plus y'a pleins de mods :D
- **Tails Adventure**, dont j'adore le gameplay un peu plus "metroidvania mais avec des niveaux", et que je trouve vraiment cool. Son seul soucis est qu'il utilise des mots de passe, mais y'a moyen de rendre l'expérience bien plus fun en émulation.

</section>

<div class="align-center mb-1">

<img src="/img/shrines/sonic/sonic-illu-3.webp" alt="" class="shrine-illu" />

</div>

<section>


## Les fangames et les hacks

Un des points que j'aime le plus dans la fanbase Sonic est sa *créativité*. La série contient des tonnes de fangames et de hacks de qualités, au points ou j'ai pu en faire un [listing](https://sonic.fanstuff.garden/fan-area/games/) assez riche. Mon fangame préféré est l'incroyable Sonic Robo Blast 2, dont j'ai [refait la fiche sur planète-sonic](https://planete-sonic.com/communaute/fan-games/article/sonic-robo-blast-2) et dont j'ai consacré une partie entière de mon listing [à ses mods](https://sonic.fanstuff.garden/fan-area/srb2/).

Mais outre [SRB2](https://srb2.org) qui est fun et pleins de mods, il y a toute une richesse de jeux dans des genres divers et tout. Et ce qui est impressionnant, c'est qu'on est arrivé à un nombre très correct de jeux finis. Parmi les autres que je conseillerais il y a :

- **Sonic Chrono Adventure**, une expérimentation fusionnant Sonic et Metroidvania.
- **Sonic Triple Trouble 16 bits**, qui est un remake de Triple Trouble qui le transforme en une véritable suite de Sonic 3 & Knuckles.
- **Sonic GT** qui mélange vitesse et exploration en 3D
- **Project P-06** qui transforme Sonic 2006 en un jeu qui mérite véritablement d'être vu comme un Sonic Adventure 3
- **Sonic Blasting Adventure** qui n'est pas le plus incroyable des fangames mais dont le concept est giga cool : un remake d'un jeu bootleg sorti sur gameboy et gameboy color.

Et je vous invite aussi à explorer la quantité de mods et de hacks !

</section>

<div class="align-center mb-1">

<img src="/img/shrines/sonic/sonic-illu-1.webp" alt="" class="shrine-illu" />

</div>

<section>

## La communauté

On se demande souvent si la communauté Sonic est "la pire qui soit" ou des trucs du genre. Mon avis, pour y être depuis plus de 20 ans, et pour être dans pas mal d'autre communauté internet est que… elle n'est pas spécialement pire qu'une autre. 

Je ne dis pas que y'a pas de soucis. Genre si vous allez sur le Twitter Sonic, ouais vous allez voir des tonnes de discours à la con… mais c'est Twitter quoi. La commu Sonic est comme une majorité de fandom un endroit ou des tas de gens vont faire diffuser des avis très (trop) vite, ou des rumeurs peuvent circuler et tout. Pour moi, on rentre dans les soucis spécifiques au fandoms, dont j'ai parlé en long et en large [dans cet article](https://blog.kazhnuz.space/construire-de-meilleurs-fandoms).

Mais du coup, quels sont les soucis principaux pour moi ? Je vais exclure ici les trucs carrément horrible genre l'existence de grooming et tout, ou là la seule solution c'est d'exclure définitivement toute personne qui s'en prend à des mineurs. Je veux parler ici des soucis plus "en tant que commu"

- Les guerres de shipping. Trop de gens décident de mettre la défense de tel ou tel ship au-delà du respect de base qu'on doit aux autres être humains.
- La diffusion de rumeurs, en partie par les Sonictubeur. C'est pour moi un gros soucis, des tas de chose fausse sont crue parce que diffusé par des "grands noms"
- L'élitisme. S'il s'est calmé, il y a eut historiquement des soucis d'élitisme dans le fandom Sonic.
- Et comme la plupart des fandoms (même si les comportements précédant aussi sont communs dans les fandoms) : la tendance à se fight sur des avis à la con.

Comment résoudre tout ça ? Pour moi, ce qu'il faut faire c'est [construire de meilleurs espaces communautaires](https://blog.kazhnuz.space/construire-de-meilleurs-fandoms-2) où ce genre de comportement seront proscris.

</section>

<div class="align-center mb-1">

<img src="/img/shrines/sonic/sonic-illu-4.webp" alt="" class="shrine-illu" />

</div>

<section>

## Quelques sites cools

Pour finir, voici quelques sites que je trouve sympa sur Sonic. Je ne vais mettre ni Planète-Sonic dedans, ni Sonic Garden, parce que je serais trop biaisé parce qu'y participant (mais allez les voir quand même lol).

- [Secrets of Sonic Team](https://sostarchive.neocities.org/) - Un vieux fansite parlant des secrets de la Sonic Team
- [Sonic Xtreme Compendium](http://www.geocities.ws/sonicxtreme/home.html) - Pleins de chose sur Sonic Xtreme
- [Sonic Fangames HQ](https://sonicfangameshq.com) - Comme son nom l'indique, tout sur les fangames
- [Sonic Hacking Contest](https://shc.zone/) - Un concours de hackroms régulier
- [Sonic Robo Blast 2](https://srb2.org) - Qui contient notamment un forum avec des tas de création de la commus

</section>
