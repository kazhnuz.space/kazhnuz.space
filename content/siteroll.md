---
layout: layouts/sommaire.njk
eleventyNavigation:
  key: Siteroll
  order: 5
---


# Sites que j'aime bien

Voici une petite page ou je site quelques liens cool/que je conseille pour les gens qui veulent découvrir des sites que j’aime bien. Cette liste est continuellement incomplète, je la fait au fur et à mesure de ce à quoi je pense.

Il y aura un peu de tout en vrac : site, blogs, chaines de vidéastes, etc.

## Mes autres siterolls

- Les sujets lié au **numérique** dans son ensemble sont sur le siteroll de [Quarante-Douze](https://quarante-douze.net).

## Gens cool et potos

- [Withelias](https://withelias.kobold.cafe/) – Artiste numérique qui fait pas mal de trucs (notamment des grosses bébètes)
  - [Distant Flare](https://distantflare.kobold.cafe/) : le webcomic de Withelias
- [OniriCorpe](https://oniricorpe.eu/) – Émy est une bidouilleuse qui fait pleins de trucs cool et des réflexions intéressantes sur le handicap et d’autres sujets.
- [Chachoukie](https://linktr.ee/chachoukie.tsune) – Artiste, streameuse et tatoueuse en devenir.
- [Planete-Sonic](https://planete-sonic.com/) – Site de référence FR pour Sonic (j’y aide owo)
- [Woltar](https://woltar.net/) – Site d’élevage gratuit (j’y aide owo)

## Sites d’artistes et blog BD

- [Lizzie Crowdagger](https://crowdagger.fr/) – Autrice de littérature queer et punk
- [Chez Kek](https://blog.zanorg.com/) – Blog BD de Kek
- [Bouletcorp](https://bouletcorp.com/) – Blog BD de Boulet
- [Slightly Damned](https://www.sdamned.com/) – Un webcomic d’aventure parlant d’anges et de démons [EN]
- [Tamberlane](https://www.tamberlanecomic.com/) – Un webcomic avec une humaine apparue mystérieusement dans une ville d’animaux anthropomorphiques

## Vidéastes

- [Hacking Social](https://www.hacking-social.com/) – Un blog qui parle de la notion du hacking social, et de psychologie sociale. Produit des vidéos sur la chaine youtube du même nom
- [Folding Ideas](https://www.youtube.com/channel/UCyNtlmLB73-7gtlBz00XOQQ) – Chaine youtube étudiant des œuvres et phénomène d’un point de vue culturel et contextuel, se penchant sur comment certaines cultures se forment. [EN]
- [Meeea](https://www.youtube.com/@Meeea) – Une vidéaste parlant de divers sujets de popculture, notamment plus anciens (années 80~90).
- [Big Blue Studio](https://www.youtube.com/@BigBlueArms) – Chansons gauchistes et autres créations.

## Divertissement et fansites

*Note : dans cette catégorie, il y en a tellement que je mets surtout quelques-uns que j’affectionne tout particulièrement. Elle est encore en construction le temps que j’y pense tous.*

- [Blue Moon Falls](https://bluemoonfalls.com/) – Un petit site Pokémon sur les vieilles générations (1 et 2).
- [Dragonfly Cave](https://www.dragonflycave.com/) – Un autre petit site Pokémon, assez complet.
- [Secrets of Sonic Team](http://sost.emulationzone.org/) – Un ancien site web Sonic centré sur les mystères et secrets autour des jeux

## Lifestyle

- [Thee burger dude](https://theeburgerdude.com/) – Un créateur de nourriture de fast-food/confort food vegan, si vous voulez baver devant des recettes de burger végan. [EN]
- [Nantes-Vegetal](https://nantes-vegetal.fr/) – Où trouvez des restaurants végans/végétarien à Nantes
- [Patate et Cornichon](https://patateetcornichon.com/) – Un blog d’une cuisinière végan faisant des recettes pour les flemmards comme moi

Note : Pour plus de liens, vous pouvez voir mon shaarli