---
layout: layouts/sommaire.njk
eleventyNavigation:
  key: Mes univers
  order: 1
---

# Mes univers de fictions

J’ai commencé à écrire il y a un moment, et même si je produis plus beaucoup de texte ces derniers temps, j’ai toute une petite série d’univers qui sont plus ou moins développés, dans des contextes que j'ai imaginés pour le fun.

Le but de cette petite page est de les présenter, si cela vous intéresse.

## Erratum

[Erratum](https://erratum.kazhnuz.space) est un univers de JDR de fantasy urbaine se passant dans l’époque contemporaine, se déroulant dans une terre parallèle ou la magie existe, notamment sous la forme de 12 types de pouvoirs appelés les « signes », basé sur les signes du zodiaque. L’univers est la continuation de plusieurs de mes anciens univers. Parmi les lieux important qu’on y retrouve, il y a l’école DanseRonce, un pensionnat et sanctuaire pour « enfants bizarres/étrange » visant à leur permettre de se développer et de se protéger d’un monde parfois cruel et oppressif.

Cet univers a un aspect "chorale" où on retrouve pêle-mêle de la magie, des techno-magies avancées, des dieux, des superhéros… et contient des composantes humoristiques par moments.

### Univers dérivés

- [Dragons from Outer Space](https://erratum.kazhnuz.space/extensions/dfos/) est un dérivé de l’univers d’Erratum avec une approche « space fantasy ». Il ajoute à Erratum des planètes, systèmes planétaires, espèces de SF, etc. Il existe pour permettre quelques autres types de campagnes dans le système d’Erratum.

- **Antichton** (pas encore de page) est une variation de l’univers d’Erratum ou le monde magique a été extrait de force du nôtre lors de la Grande Guerre des Vertus (l’évènement ayant provoqué la découverte de la magie dans Erratum), et du coup se trouve sur une terre parallèle nommée Antichton ou la Contre-Terre, située à l’exact opposé de notre planète du Soleil. Cette contre-terre contient très peu de technologie et est un peu plus bloquée dans une sorte d’époque semi-préindustrielle.

## Ailleurs

**Ailleurs** est le nom de mon « multivers » (dont font techniquement partie mes autres histoires, mais ça n’a pas énormément d’importance). Ailleurs est un multivers basé sur la notion de « Paradoxe » : chaque univers à ses lois quelque peu « imparfaite ». Constamment, des minicontradictions se produisent à l’échelle atomique. De ces contradictions, suivant la règle de l’ex falso, tout devient possible. Ces contradictions produisent une énergie permettant de changer les possibles, les réalités.

Il existe alors un nombre infini de monde, tout est possible. Chaque variation des lois de la physique, chaque univers qu’on peut imaginer. Il est quelque part.

De cela, deux espèces sont apparues. Les Phosphoriens, visant à porter la « lumière de la science », visant à contrôler le paradoxe et les Stokasthès qui ont embrassé ces puissances. Les deux peuples ont grandi pour découvrir de nombreux mondes, et ont fini par se faire une guerre terrible, et s’entre-annihiler. Cependant, leurs héritages semblent toujours causer de nombreux problèmes à travers les mondes.

C’est également le nom de projets de miniromans que j’aimerais écrire dedans où deux adolescentes découvrent un étrange être nommé Alior qui a le pouvoir de voyager à travers les mondes.

### Histoires dans ce multivers


- La première version de Ailleurs est disponible dans [mon ancien Deviantart](https://www.deviantart.com/kazhnuz/gallery) (mais sera remplacée, et cette version peut-être archivée dans la Vault)
- Les histoires de mon tag [petites histoires anomiques](https://blog.kazhnuz.space/tag/petites-histoires-anomiques/).

## Radiant Skies

**Radiant Skies** est un univers de soft science-fiction « furry » lumineux et coloré avec des animaux anthropomorphiques amusant dans un monde principalement technologique avec quelques touches de mystiques, inspiré de franchise telles que Sonic, Starfox et Megaman. Le monde y est également inspiré de l’antiquité, avec deux principales nations, une inspirée de la Rome Antique et une autre inspiré des différents Empires Perses.

Ce monde est composé d’une planète éponyme ou vive les Aestriens, des animaux anthropomorphiques diverses et variés, utilisant des technologies avancée et généralement plus ou moins lié à l’Ignum, un matériel étrange et source d’énergie puissante. Certains êtres ont même la capacité de l’utiliser directement, tel que les anciens et redoutés *Adonaï*. Les terres sont en grande parties sauvages, coupées de quelques villes protégées, parce que de nombreux êtres anciens, les Souverains, de gigantesques armes biotechnologiques se trouve dans les grands territoires sauvages. Seuls les aventuriers se risquent dans ce genre d’endroit, devenant essentiels pour la transmission des informations entre les villes. Le monde est parsemé de ruines et restes de civilisations antiques très avancées, aux nombreux mystères non élucidés.

À côté de cela, dans les cieux se trouvent un archipel flottant mystérieux et convoité, au fond de la tempête qui ne s’arrête jamais, ou se trouverait les secrets de ce monde.