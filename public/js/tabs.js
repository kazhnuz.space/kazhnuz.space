document.addEventListener("DOMContentLoaded", () => {
    const tabsContainer = document.querySelector(".tabs");

    function initializeTabs(tabsContainer) {
        const tabList = tabsContainer.querySelector(".tabs-list");
        const tabItems = Array.from(tabList.children);
        const tabContent = tabsContainer.querySelector(".tabs-content");
        const tabContentItems = Array.from(tabContent.children);
        let activeIndex = 0;

        function setActiveTab(index) {
            tabItems.forEach(item => item.classList.remove("active"));
            tabItems.forEach(content => content.ariaSelected = false);
            tabContentItems.forEach(content => content.classList.remove("visible"));
            tabItems[index].classList.add("active");
            tabItems[index].ariaSelected = true;
            tabContentItems[index].classList.add("visible");
        }

        tabItems.forEach((item, index) => {
            item.addEventListener("click", () => {
                activeIndex = index;
                setActiveTab(index);
            });

            item.addEventListener('keydown', function(event) {
                if (event.key === 'Tab') {
                    event.preventDefault();

                    const iframe = tabContentItems[index].querySelectorAll('iframe')[0].contentWindow;
                    iframe.document.querySelectorAll('a')[0].focus(); // On focus le premier lien de l'iframe
                }
                if (event.key === 'ArrowRight') {
                    event.preventDefault();

                    tabItems[(index + 1) % tabItems.length].focus();
                }
                if (event.key === 'ArrowLeft') {
                    event.preventDefault();

                    let newIndex = index - 1;
                    if (newIndex < 0) {
                        newIndex = tabItems.length - 1;
                    }

                    tabItems[newIndex].focus();
                }
            });
        });

        

        setActiveTab(activeIndex);
    }

    // Initialize the tabs
    initializeTabs(tabsContainer);
});