<?php
  header('Content-Type: text/html; charset=utf-8');

  if (!ini_get('date.timezone')) {
	  date_default_timezone_set('Europe/Paris');
  }

  require_once 'feed.php';
  $error = false;
  
	$title = "Derniers posts";
	if (!is_null($_GET["name"])) {
		$title = $_GET["name"];
	}

  try {
		  $rssBlog = Feed::loadRss('https://' . $_GET["url"]);
	} catch (Exception $e) {
		  $error = true;
	}
?>

<!DOCTYPE html>
<html lang="fr" class="m-1 p-0">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kazhnuz's Feed</title>
    <link rel="stylesheet" href="/css/index.css">
    <link rel="icon" href="/img/favicon.png">
  </head>
  <body class="m-0 p-0">
    <div class="card h-feed hfeed">
      <h2 class="card-header p-name site-title"><?php echo $title ?></h2>
      <div class="menu fg-dark">
      <?php if ($error == false) { ?>
        <ul>
          <?php $i = 1; ?>
          <?php foreach ($rssBlog->item as $item): ?>
            <?php if ($i <= 6) { ?>
              <li class="h-entry hentry">
                <a href="<?php echo htmlspecialchars($item->url) ?>" class="d-flex f-row u-url" target="_blank">
                  <div class="ellipsis p-name entry-title"><?php echo htmlspecialchars($item->title) ?></div>
                  <time class="small-text" datetime="<?php echo date(DATE_ATOM, (int) $item->timestamp) ?>">(<?php echo date('j/n/Y', (int) $item->timestamp) ?>)</time>
                </a>
              </li>
            <?php $i++; ?>
            <?php } ?>
          <?php endforeach ?>
        </ul>
      <?php } else { ?>
      	<p class="text-center align-center">Erreur : L'URL du flux n'est pas accessible.</p>
      <?php } ?>
      </div>
    </div>
  </body>
</html>
