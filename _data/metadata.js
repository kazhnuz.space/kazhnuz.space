module.exports = {
	title: "Kazhnuz' Space",
	url: "https://kazhnuz.space/",
	language: "fr",
	description: "Le site personnel de Kazhnuz",
	author: {
		name: "Kazhnuz",
		email: "kazhnuz@kobold.cafe",
		url: "https://kazhnuz.space/"
	}
}
